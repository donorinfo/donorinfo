package com.lexadev.donorinfo.parse;

import com.parse.ParseUser;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Alexey Rogovoy (lexapublic@gmail.com) on 11.01.2015.
 */
public class AppUser extends ParseUser {
    private static final String TAG = "AppUser";
    public static final String F_FIRST_NAME = "First_Name";
    public static final String F_LAST_NAME = "Last_Name";
    public static final String F_SEX = "Sex";
    public static final String F_BLOOD = "Blood";
    public static final String F_BLOOD_COUNT = "Blood_Count";
    public static final String F_PHONE = "Phone";
    public static final String F_REGION = "Region";
    public static final String F_ADMIN = "Admin";
    public static final String YEARS = "Years";

    public static AppUser getCurrentUser(){
        return (AppUser)ParseUser.getCurrentUser();
    }
    public AppUser() {
    }

    public void setFirstName(String s) {
        put(F_FIRST_NAME,s);
    }
    public String getFirstName(){
        return getString(F_FIRST_NAME);
    }

    public void setLastName(String s) {
        put(F_LAST_NAME,s);
    }
    public String getLastName(){
        return getString(F_LAST_NAME);
    }
    public void setBlood(String blood){
        put(F_BLOOD,blood);
    }
    public String getBlood(){
        return getString(F_BLOOD);
    }

    public String getCountBlood() {
        return getString(F_BLOOD_COUNT);
    }
    public void setCountBlood(String countBlood){
        put(F_BLOOD_COUNT,countBlood);
    }

    public String getPhone() {
        return getString(F_PHONE);
    }
    public void setPhone(String phone){
        put(F_PHONE,phone);
    }

    public String getRegion() {
        return getString(F_REGION);
    }
    public void setRegion(String region){
        put(F_REGION,region);
    }

    public void setSex(int sex) {
        put(F_SEX,sex);
    }
    public int getSex(){
        return getInt(F_SEX);
    }
    public boolean isAdmin(){
        return getBoolean(F_ADMIN);
    }
    public boolean addYear(int newYear){
        List<Integer> years = getList(YEARS);
        if (years==null)
            years = new ArrayList<Integer>();
        for (Integer year:years){
            if (year.intValue()==newYear){
                return false;
            }
        }
        years.add(new Integer(newYear));
        put(YEARS,years);
        return true;
    }
    public List<Integer> getYears(){
        return getList(YEARS);
    }
    public void setYears(List<Integer> years){
        put(YEARS,years);
    }
}
