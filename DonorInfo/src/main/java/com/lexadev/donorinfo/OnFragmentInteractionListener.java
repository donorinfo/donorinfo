package com.lexadev.donorinfo;

import android.content.SharedPreferences;
import android.view.View;

import com.lexadev.donorinfo.helper.InfoFragments;
import com.lexadev.donorinfo.helper.LoadingProgressDialog;
import com.lexadev.donorinfo.model.ParseEvent;
import com.lexadev.donorinfo.model.ParseRequest;
import com.lexadev.donorinfo.ui.RegionsFragment;

/**
 * Created by Alexey Rogovoy (lexapublic@gmail.com) on 12.02.2015.
 */
public interface OnFragmentInteractionListener {
    public void toSelected(int id);
    //public void update
    public SharedPreferences getSettings();
    public void setTitle(int resTitle);
    public void back();
    public void addNewEvent(long date);
    public void startWaiting(int resId, LoadingProgressDialog.OnCancelListener listener);
    public void stopWaiting();
    public void showToast(int resId);
    public void showToast(int resId, String msg);
    public void toProfile(boolean addToBackStack);
    public void toRegistration(boolean addToBackStack);
    public void toLogin(boolean addToBackStack);
    public void editEvent(ParseEvent event);
    public void setSelectedRegion(String region);
    public String getSelectedRegion();

    public void regionSelected(String region_id, String city_id,RegionsFragment.SelectionMode selectionMode );

    public void toRegionSelect(RegionsFragment.SelectionMode selectionMode ,boolean addToBackStack);

    public void selectPlace(String mStationId);

    public void selectStation(String stationId);
    public String getSelectedStationId();
    public void selectRegions(String regionId);
    public String getSelectedRegionId();

    public void viewRequest(ParseRequest request);

    public void setUseAdmin(boolean checked);

    public boolean isUseAdmin();
    public void updateRemindTimer();
    public void setupUI(View view);
    public void toInfo(InfoFragments fragment);

    public void showLegend();

    public void viewRegionEvent(ParseEvent event);

    public void openList();
}
