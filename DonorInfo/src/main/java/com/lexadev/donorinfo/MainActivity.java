package com.lexadev.donorinfo;

import android.app.Activity;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.res.Configuration;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarActivity;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.lexadev.donorinfo.helper.DbAdapter;
import com.lexadev.donorinfo.helper.DrawerAdapter;
import com.lexadev.donorinfo.helper.DrawerItem;
import com.lexadev.donorinfo.helper.FontsHelper;
import com.lexadev.donorinfo.helper.InfoFragments;
import com.lexadev.donorinfo.helper.LoadingProgressDialog;
import com.lexadev.donorinfo.model.ParseCities;
import com.lexadev.donorinfo.model.ParseEvent;
import com.lexadev.donorinfo.model.ParseRegions;
import com.lexadev.donorinfo.model.ParseRequest;
import com.lexadev.donorinfo.model.ParseStation;
import com.lexadev.donorinfo.model.ParseVersions;
import com.lexadev.donorinfo.model.TableVersions;
import com.lexadev.donorinfo.parse.AppUser;
import com.lexadev.donorinfo.ui.AboutDonationFragment;
import com.lexadev.donorinfo.ui.AboutFragment;
import com.lexadev.donorinfo.ui.BeforeDonationFragment;
import com.lexadev.donorinfo.ui.CalendarFragment;
import com.lexadev.donorinfo.ui.DeniedFragment;
import com.lexadev.donorinfo.ui.ForeverDeniedFragment;
import com.lexadev.donorinfo.ui.InfoFragment;
import com.lexadev.donorinfo.ui.LegendFragment;
import com.lexadev.donorinfo.ui.ListEventsFragment;
import com.lexadev.donorinfo.ui.LoginFragment;
import com.lexadev.donorinfo.ui.LoginSelectionFragment;
import com.lexadev.donorinfo.ui.MainFragment;
import com.lexadev.donorinfo.ui.NewEventFragment;
import com.lexadev.donorinfo.ui.NewRegionEventFragment;
import com.lexadev.donorinfo.ui.ProfileFragment;
import com.lexadev.donorinfo.ui.RegionNewsFragment;
import com.lexadev.donorinfo.ui.RegionsFragment;
import com.lexadev.donorinfo.ui.RegistrationFragment;
import com.lexadev.donorinfo.ui.RequestFragment;
import com.lexadev.donorinfo.ui.RequestsFragment;
import com.lexadev.donorinfo.ui.StationFragment;
import com.lexadev.donorinfo.ui.TemporaryDeniedFragment;
import com.parse.FindCallback;
import com.parse.GetCallback;
import com.parse.ParseException;
import com.parse.ParsePush;
import com.parse.ParseQuery;

import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.List;


public class MainActivity extends ActionBarActivity implements OnFragmentInteractionListener,
    OnPossibleEventUpdates{
    private static final String TAG = "MainActivity";
    private FragmentManager mFm;
    private CustomActionBarDrawerToggle toggle;
    private ArrayList<DrawerItem> mDrawerItems;
    private Toolbar mToolbar;
    private DrawerLayout mDrawerLayout;
    private TextView mToolbarTitleView;
    private String mSelectedRegion = null;
    private LoadingProgressDialog mLoadingDialog;
    private SharedPreferences mSettings;
    private AppUser mCurrentUser = null;
    private DrawerAdapter mDrawerAdapter = null;
    private DbAdapter mDbInstance;
    private String mSelectedStationId = null;
    private String mSelectedRegionId=null;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        mSettings = PreferenceManager.getDefaultSharedPreferences(this);
        super.onCreate(savedInstanceState);
        EventRemindService.startUpdateServiceRemind(this);
        mCurrentUser = AppUser.getCurrentUser();
        mDbInstance = DbAdapter.getInstance(getApplicationContext());
        setContentView(R.layout.activity_main);
        mLoadingDialog = new LoadingProgressDialog(this);

        mToolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(mToolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        //getSupportActionBar().setHomeButtonEnabled(true);
        checkStationDatabase();
        restoreSettings();

        mToolbar.setVisibility(View.GONE);
        mToolbar.setLogo(R.drawable.logo_toolbar);

        mToolbarTitleView = getActionBarTextView();
        if (mToolbarTitleView!=null){
            mToolbarTitleView.setTypeface(FontsHelper.getFont(FontsHelper.FontName.COMFORTAA, FontsHelper.FontsStyle.BOLD));
            mToolbarTitleView.setTextColor(getResources().getColor(android.R.color.white));
        }
        mFm = getSupportFragmentManager();

        mFm.addOnBackStackChangedListener(new FragmentManager.OnBackStackChangedListener() {
            @Override
            public void onBackStackChanged() {
                if (toggle!=null) {
                    toggle.setDrawerIndicatorEnabled(mFm.getBackStackEntryCount() == 0);
                    enableDisableMenu(mFm.getBackStackEntryCount() == 0);
                    //getSupportActionBar().setDisplayHomeAsUpEnabled(mFm.getBackStackEntryCount() > 0);
                    toggle.syncState();
                }
            }
        });

        initNavigationDrawer();
        Intent intent = getIntent();
        if (intent!=null){
            String action = intent.getAction();
            if (action!=null && AppConstants.ACTION_REMIND.equals(action)){
                toSelected(R.id.action_calendar);
            }
            else
                toMainScreen();
        }
        else
            toMainScreen();
    }

    private void restoreSettings() {
        mSelectedRegionId = mSettings.getString(AppConstants.PREF_SELECTED_REGION_REQUESTS,null);
    }

    private int mCountUpdate = 0;
    private ParseVersions mVersions = null;

    private List<ParseRegions> mRegions = new ArrayList<>();
    private void updateRegions(final int count){
        ParseRegions.getRegionsQuery(count).findInBackground(new FindCallback<ParseRegions>() {
            @Override
            public void done(List<ParseRegions> parseRegions, ParseException e) {
                int newItemsCount = 0;
                if (e == null && parseRegions != null) {
                    newItemsCount = parseRegions.size();
                    if (newItemsCount>0) {
                        mRegions.addAll(parseRegions);
                        updateRegions(count + newItemsCount);
                        return;
                    }
                }
                mDbInstance.updateTableRegions(mRegions, mVersions.getRegionsVer());
                mCountUpdate--;
                if (mCountUpdate == 0) {
                    mCountUpdate++;
                    updateStations(0);
                }
            }
        });

    }

    private List<ParseCities> mCities = new ArrayList<>();
    private void updateCities(final int count){
        ParseCities.getCitiesQuery(count).findInBackground(new FindCallback<ParseCities>() {
            @Override
            public void done(List<ParseCities> parseCities, ParseException e) {
                int newItemsCount = 0;
                if (e == null && parseCities != null) {
                    newItemsCount = parseCities.size();
                    if (newItemsCount>0) {
                        mCities.addAll(parseCities);
                        updateCities(count+newItemsCount);
                        return;
                    }
                }
                mDbInstance.updateTableCities(mCities, mVersions.getRegionsVer());
                mCountUpdate--;
                if (mCountUpdate == 0) {
                    mCountUpdate++;
                    updateStations(0);
                }
            }
        });

    }

    private void checkStationDatabase() {
        ParseQuery<ParseVersions> queryVersion = ParseVersions.queryVersions();
        queryVersion.getFirstInBackground(new GetCallback<ParseVersions>() {
            @Override
            public void done(final ParseVersions versions, ParseException e) {
                if (e == null && versions!=null) {
                    mVersions = versions;
                    boolean mayUpdateStations = true;
                    if (!mDbInstance.checkVersion(TableVersions.TABLES.REGIONS, versions.getRegionsVer())) {
                        mCountUpdate++;
                        mayUpdateStations = false;
                        updateRegions(0);
                    }
                    if (!mDbInstance.checkVersion(TableVersions.TABLES.CITIES, versions.getCitiesVer())) {
                        mayUpdateStations = false;
                        mCountUpdate++;
                        updateCities(0);
                    }
                    if (mayUpdateStations&&!mDbInstance.checkVersion(TableVersions.TABLES.STATIONS, versions.getStationsVer())) {
                        mCountUpdate++;
                        updateStations(0);
                    }
                    if (mCountUpdate > 0) {
                        startWaiting(R.string.progress_updating_stations, null);
                    }
                }
            }
        });
    }
    private List<ParseStation> mStations = new ArrayList<>();
    private void updateStations(final int count){
        ParseStation.getStationsQuery(count).findInBackground(new FindCallback<ParseStation>() {
            @Override
            public void done(List<ParseStation> parseStations, ParseException e) {
                int newItemsCount = 0;
                if (e == null && parseStations != null) {
                    newItemsCount = parseStations.size();
                    if (newItemsCount > 0) {
                        mStations.addAll(parseStations);
                        updateStations(count + newItemsCount);
                        return;
                    }
                }
                mCountUpdate--;
                mDbInstance.updateTableStations(mStations, mVersions.getStationsVer());
                stopWaiting();
            }
        });

    }

    private void enableDisableMenu(boolean enableMenu) {
        if (enableMenu) {
            mDrawerLayout.setDrawerLockMode(DrawerLayout.LOCK_MODE_UNLOCKED);
            toggle.onDrawerStateChanged(DrawerLayout.LOCK_MODE_UNLOCKED);
            toggle.syncState();

        } else {
            mDrawerLayout.setDrawerLockMode(DrawerLayout.LOCK_MODE_LOCKED_CLOSED);
            toggle.onDrawerStateChanged(DrawerLayout.LOCK_MODE_LOCKED_CLOSED);
            toggle.syncState();
        }
    }

    private ParseEvent mBloodEvent = null;
    private ParseEvent mPlazmaEvent = null;
    private ParseEvent mTromboEvent = null;
    @Override
    public void updateEvent(int eventType, ParseEvent event) {
        switch (eventType){
            case ParseEvent.BLOOD_EVENT_BLOOD:
                mBloodEvent = event;
                break;
            case ParseEvent.BLOOD_EVENT_PLAZMA:
                mPlazmaEvent = event;
                break;
            case ParseEvent.BLOOD_EVENT_TROMBO:
                mTromboEvent = event;
                break;
        }
    }

    @Override
    public ParseEvent getEvent(int eventType) {
        switch (eventType){
            case ParseEvent.BLOOD_EVENT_BLOOD:
                return mBloodEvent;
            case ParseEvent.BLOOD_EVENT_PLAZMA:
                return mPlazmaEvent;
            case ParseEvent.BLOOD_EVENT_TROMBO:
                return mTromboEvent;
        }
        return null;
    }

    @Override
    public void invalidate() {
        mBloodEvent = null;
        mPlazmaEvent = null;
        mTromboEvent = null;
    }

    private class CustomActionBarDrawerToggle extends ActionBarDrawerToggle{

        public CustomActionBarDrawerToggle(Activity activity, DrawerLayout drawerLayout, int openDrawerContentDescRes, int closeDrawerContentDescRes) {
            super(activity, drawerLayout, openDrawerContentDescRes, closeDrawerContentDescRes);
        }
        private String mTitle;

        @Override
        public void onDrawerOpened(View drawerView) {

            super.onDrawerOpened(drawerView);
            mTitle = mToolbar.getTitle().toString();
            mToolbar.setTitle(R.string.app_name);
        }

        @Override
        public void onDrawerClosed(View drawerView) {
            super.onDrawerClosed(drawerView);
            mToolbar.setTitle(mTitle);
        }
        public void setTitle(String title){
            mTitle = title;
        }

    }

    private void initNavigationDrawer() {
        mDrawerLayout = (DrawerLayout) findViewById(R.id.drawer_layout);
        toggle = new CustomActionBarDrawerToggle(
                this,
                mDrawerLayout,
                R.string.navigation_drawer_open,
                R.string.navigation_drawer_close);
        toggle.setDrawerIndicatorEnabled(true);

        mDrawerLayout.setDrawerListener(toggle);
        updateDrawerAdapter();
        ListView lv_navigation_drawer = (ListView) findViewById(R.id.lv_navigation_drawer);
        lv_navigation_drawer.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                view.setSelected(true);
                mDrawerLayout.closeDrawers();
                toSelected((int)id);
            }
        });
    }

    private void updateDrawerAdapter() {
        mDrawerItems = new ArrayList<>();
        mDrawerItems.add(new DrawerItem(R.id.action_home,R.drawable.image_home,R.string.title_home,true));
        mDrawerItems.add(new DrawerItem(R.id.action_calendar,R.drawable.image_calendar,R.string.title_calendar,true));
        mDrawerItems.add(new DrawerItem(R.id.action_profile,R.drawable.image_profile,R.string.title_profile,true));
        mDrawerItems.add(new DrawerItem(R.id.action_info,R.drawable.image_info,R.string.title_info,true));
        mDrawerItems.add(new DrawerItem(R.id.action_station,R.drawable.image_stations,R.string.title_stations,true));
        mDrawerItems.add(new DrawerItem(R.id.action_request,R.drawable.image_request,R.string.title_request,true));
        mDrawerItems.add(new DrawerItem(R.id.action_needed,R.drawable.image_needed,R.string.title_needed,true));
        if (mCurrentUser!=null && mCurrentUser.isAdmin()&&isUseAdmin()){
            mDrawerItems.add(new DrawerItem(R.id.action_region_events,R.drawable.image_region_events,R.string.title_region_events,true));
        }
        ListView lv_navigation_drawer = (ListView) findViewById(R.id.lv_navigation_drawer);
        mDrawerAdapter = new DrawerAdapter(this, R.layout.drawer_item, mDrawerItems);
        int checkedItem = lv_navigation_drawer.getCheckedItemPosition();
        lv_navigation_drawer.setAdapter(mDrawerAdapter);
        if (checkedItem!=ListView.INVALID_POSITION){
            lv_navigation_drawer.setItemChecked(checkedItem,true);
        }
    }

    @Override
    public void toSelected(int id) {
        switch(id){
            case R.id.action_home:
                toMainScreen();
                break;
            case R.id.action_calendar:
                toCalendar(false);
                break;
            case R.id.action_profile:
                toProfile(false);
                break;
            case R.id.action_info:
                toInfo(false);
                break;
            case R.id.action_station:
                toRegionSelect(RegionsFragment.SelectionMode.NONE,false);
                break;
            case R.id.action_request:
                toRequest(false);
                break;
            case R.id.action_needed:
                toRequests(false);
                break;
            case R.id.action_about_app:
                toAbout(false);
                break;
            case R.id.action_region_events:
                toRegionEvents(false);
        }
        mToolbar.setVisibility(View.VISIBLE);
    }

    private void toRegionEvents(boolean addToBackStack) {
        RegionNewsFragment fragment = new RegionNewsFragment();
        switchFragment(fragment,addToBackStack);
    }

    private void toAbout(boolean addToBackStack) {
        AboutFragment fragment = new AboutFragment();
        switchFragment(fragment,addToBackStack);
    }

    private void toInfo(boolean addToBackStack) {
        InfoFragment fragment = new InfoFragment();
        switchFragment(fragment,addToBackStack);
    }

    private void toRequests(boolean addToBackStack) {
        if (mCurrentUser==null){
            LoginSelectionFragment fragment = new LoginSelectionFragment();
            switchFragment(fragment,addToBackStack);
        }
        else {
            RequestsFragment fragment = new RequestsFragment();
            switchFragment(fragment,false);
        }
    }

    private void toRequest(boolean addToBackStack) {
        if (mCurrentUser==null){
            LoginSelectionFragment fragment = new LoginSelectionFragment();
            switchFragment(fragment,addToBackStack);
        }
        else {
            RequestFragment fragment = RequestFragment.newInstance(null);
            switchFragment(fragment, addToBackStack);
        }
    }


    private void toCalendar(boolean addToBackStack) {
        CalendarFragment fragment = new CalendarFragment();
        switchFragment(fragment,addToBackStack);
    }

    @Override
    public SharedPreferences getSettings() {
        return mSettings;
    }

    @Override
    public void setTitle(int resTitle) {
        toggle.setTitle(getString(resTitle));
        mToolbar.setTitle(resTitle);
    }

    @Override
    public void back() {
        if (mFm.getBackStackEntryCount() == 0)
            toMainScreen();
        else
            onBackPressed();
    }

    @Override
    public void addNewEvent(long date) {
        NewEventFragment fragment = NewEventFragment.newInstance(date);
        switchFragment(fragment,true);
    }


    @Override
    public void toProfile(boolean addToBackStack) {
        mCurrentUser = AppUser.getCurrentUser();
        mDrawerAdapter.notifyDataSetChanged();
        if (mCurrentUser==null){
            LoginSelectionFragment fragment = new LoginSelectionFragment();
            switchFragment(fragment,addToBackStack);
        }
        else {
            ProfileFragment fragment = new ProfileFragment();
            switchFragment(fragment, addToBackStack);
        }
    }

    @Override
    public void toRegistration(boolean addToBackStack) {
        RegistrationFragment fragment = new RegistrationFragment();
        switchFragment(fragment,addToBackStack);
    }

    @Override
    public void toLogin(boolean addToBackStack) {
        LoginFragment fragment = new LoginFragment();
        switchFragment(fragment,addToBackStack);
    }

    @Override
    public void editEvent(ParseEvent event) {
        NewEventFragment fragment = NewEventFragment.newInstance(event);
        switchFragment(fragment,true);
    }

    @Override
    public void setSelectedRegion(String region) {
        mSelectedRegion = region;
        if (region!=null)
            mSettings.edit().putString(AppConstants.PREF_SELECTED_REGION,region).commit();
        else
            mSettings.edit().remove(AppConstants.PREF_SELECTED_REGION).commit();
    }

    @Override
    public String getSelectedRegion() {
        if (mSelectedRegion==null&&mSettings!=null)
            mSelectedRegion = mSettings.getString(AppConstants.PREF_SELECTED_REGION,null);
        return mSelectedRegion;
    }

    @Override
    public void regionSelected(String region_id, String city_id,RegionsFragment.SelectionMode selectionMode) {
        if (selectionMode== RegionsFragment.SelectionMode.NONE||selectionMode== RegionsFragment.SelectionMode.STATION) {
            StationFragment fragment = StationFragment.newInstance(city_id != null ? city_id : region_id, selectionMode== RegionsFragment.SelectionMode.STATION);
            switchFragment(fragment, selectionMode== RegionsFragment.SelectionMode.STATION||selectionMode== RegionsFragment.SelectionMode.NONE);
        }
        else{
            selectRegion(city_id!=null?city_id:region_id);
        }
    }

    @Override
    public void toRegionSelect(RegionsFragment.SelectionMode selectionMode,boolean addToBackStack) {
        RegionsFragment fragment = RegionsFragment.newInstance(selectionMode);
        switchFragment(fragment,addToBackStack);
    }

    @Override
    public void selectPlace(String mStationId) {
        mSelectedStationId = null;
        if (mStationId!=null && mStationId.trim().length()>0) {
            StationFragment fragment = StationFragment.newInstance(mStationId, true);
            switchFragment(fragment, true);
        }
        else{
            RegionsFragment fragment = RegionsFragment.newInstance(RegionsFragment.SelectionMode.STATION);
            switchFragment(fragment,true);
        }
    }

    @Override
    public void selectRegions(String regionId) {
        mSelectedRegionId = regionId;
        RegionsFragment fragment = RegionsFragment.newInstance(RegionsFragment.SelectionMode.REGION);
        switchFragment(fragment,true);
    }

    @Override
    public String getSelectedRegionId() {
        return mSelectedRegionId;
    }

    @Override
    public void selectStation(String stationId) {
        mSelectedStationId = stationId;
        mFm.popBackStackImmediate(NewEventFragment.TAG,0);
    }

    public void selectRegion(String regionId) {
        mSelectedRegionId = regionId;
        mSettings.edit().putString(AppConstants.PREF_SELECTED_REGION_REQUESTS,mSelectedRegionId).commit();
        mFm.popBackStackImmediate();
    }

    @Override
    public String getSelectedStationId() {
        return mSelectedStationId;
    }


    private void toMainScreen() {
        mFm.popBackStackImmediate();
        MainFragment fragment = new MainFragment();
        switchFragment(fragment,false);
    }

    private void switchFragment(Fragment fragment, boolean addToBackStack){
        FragmentTransaction ft = mFm.beginTransaction();
        ft.replace(R.id.container,fragment);
        if (addToBackStack) {
            if (fragment instanceof NewEventFragment)
                ft.addToBackStack(NewEventFragment.TAG);
            else if (fragment instanceof RequestsFragment)
                ft.addToBackStack(RequestsFragment.TAG);
            else
                ft.addToBackStack(null);
        }
        ft.commit();
    }

    @Override
    public void onBackPressed() {
        if (mDrawerLayout.isDrawerOpen(GravityCompat.START)){
            mDrawerLayout.closeDrawers();
            toggle.syncState();
            return;
        }

        int count = mFm.getBackStackEntryCount();
        List<Fragment> fragemnts = mFm.getFragments();
        boolean isMainOnTop = false;
        if (fragemnts.size()>0 && fragemnts.get(0) instanceof MainFragment)
            isMainOnTop = true;
        if (count == 0 && !isMainOnTop)
            toMainScreen();
        else
            super.onBackPressed();
    }
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == android.R.id.home) {
            if (mDrawerLayout.getDrawerLockMode(GravityCompat.START) == DrawerLayout.LOCK_MODE_UNLOCKED) {
                toggle.onOptionsItemSelected(item);
                return true;
            } else {
                onBackPressed();
                return true;
            }
        }

        if (toggle.onOptionsItemSelected(item))
            return true;

        return super.onOptionsItemSelected(item);
    }

    @Override
    protected void onPostCreate(Bundle savedInstanceState) {
        super.onPostCreate(savedInstanceState);
        toggle.syncState();
    }

    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);
        toggle.onConfigurationChanged(newConfig);
    }
    private TextView getActionBarTextView() {
        TextView titleTextView = null;

        try {
            Field f = mToolbar.getClass().getDeclaredField("mTitleTextView");
            f.setAccessible(true);
            titleTextView = (TextView) f.get(mToolbar);
        } catch (NoSuchFieldException e) {
        } catch (IllegalAccessException e) {
        }
        return titleTextView;
    }

    @Override
    public void startWaiting(int resId, LoadingProgressDialog.OnCancelListener listener) {
        mLoadingDialog.setOnCancelListener(listener);
        mLoadingDialog.showProgressDialog(resId);
    }

    @Override
    public void stopWaiting() {
        mLoadingDialog.hideProgressDialog();
    }

    @Override
    public void showToast(int resId) {
        Toast.makeText(this, resId, Toast.LENGTH_SHORT).show();
    }

    @Override
    public void showToast(int resId, String msg) {
        Toast.makeText(this, getString(resId, msg), Toast.LENGTH_LONG).show();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        if (mDbInstance!=null)
            mDbInstance.close();
        mDbInstance = null;
    }

    @Override
    public void viewRequest(ParseRequest request) {
        if (request!=null){
            RequestFragment fragment = RequestFragment.newInstance(request.getObjectId());
            switchFragment(fragment,true);
        }
    }

    @Override
    public void setUseAdmin(boolean checked) {
        mSettings.edit().putBoolean(AppConstants.PREF_USER_ADMIN,checked).commit();
        boolean subscribed = false;
        if (AppUser.getCurrentUser()!=null){
            if (AppUser.getCurrentUser().isAdmin()){
                if (checked){
                    ParsePush.subscribeInBackground(AppConstants.REQUESTS_CHANNEL);
                    subscribed = true;
                }
            }
        }
        if (!subscribed)
            ParsePush.unsubscribeInBackground(AppConstants.REQUESTS_CHANNEL);
        updateDrawerAdapter();
    }

    @Override
    public boolean isUseAdmin() {
        return mSettings.getBoolean(AppConstants.PREF_USER_ADMIN,false);
    }

    @Override
    public void updateRemindTimer() {
        EventRemindService.startUpdateRemind(this);
    }
    public static void hideSoftKeyboard(Activity activity) {
        InputMethodManager inputMethodManager = (InputMethodManager)  activity.getSystemService(Activity.INPUT_METHOD_SERVICE);
        inputMethodManager.hideSoftInputFromWindow(activity.getCurrentFocus().getWindowToken(), 0);
    }
    @Override
    public void setupUI(View view) {

        //Set up touch listener for non-text box views to hide keyboard.
        if(!(view instanceof EditText)) {

            view.setOnTouchListener(new View.OnTouchListener() {

                public boolean onTouch(View v, MotionEvent event) {
                    hideSoftKeyboard(MainActivity.this);
                    return false;
                }

            });
        }

        //If a layout container, iterate over children and seed recursion.
        if (view instanceof ViewGroup) {

            for (int i = 0; i < ((ViewGroup) view).getChildCount(); i++) {

                View innerView = ((ViewGroup) view).getChildAt(i);

                setupUI(innerView);
            }
        }
    }

    @Override
    public void toInfo(InfoFragments fragment) {
        Fragment infoFragemnt = null;
        switch (fragment){
            case DENIED:
                infoFragemnt = new DeniedFragment();
                break;
            case ABOUT:
                infoFragemnt = new AboutDonationFragment();
                break;
            case BEFORE:
                infoFragemnt = new BeforeDonationFragment();
                break;
            case DENIED_FOREVER:
                infoFragemnt = new ForeverDeniedFragment();
                break;
            case DENIED_TEMPORARY:
                infoFragemnt = new TemporaryDeniedFragment();
                break;

        }
        if (infoFragemnt!=null)
            switchFragment(infoFragemnt,true);
    }

    @Override
    public void showLegend() {
        LegendFragment fragment = new LegendFragment();
        switchFragment(fragment,true);
    }

    @Override
    public void viewRegionEvent(ParseEvent event) {
        NewRegionEventFragment fragment = NewRegionEventFragment.newInstance(event);
        switchFragment(fragment,true);
    }

    @Override
    public void openList() {
        ListEventsFragment fragment = new ListEventsFragment();
        switchFragment(fragment,true);
    }
}
