package com.lexadev.donorinfo;

import android.app.Application;

import com.lexadev.donorinfo.helper.DebugDetect;
import com.lexadev.donorinfo.helper.FontsHelper;
import com.lexadev.donorinfo.model.ParseCities;
import com.lexadev.donorinfo.model.ParseEvent;
import com.lexadev.donorinfo.model.ParseRegions;
import com.lexadev.donorinfo.model.ParseRequest;
import com.lexadev.donorinfo.model.ParseStation;
import com.lexadev.donorinfo.model.ParseVersions;
import com.lexadev.donorinfo.parse.AppUser;
import com.parse.Parse;
import com.parse.ParseInstallation;
import com.parse.ParseObject;
import com.parse.PushService;

import org.acra.ACRA;
import org.acra.ReportField;
import org.acra.ReportingInteractionMode;
import org.acra.annotation.ReportsCrashes;

//Settings for the ACRA
@ReportsCrashes(formKey = "", // will not be used
        mailTo = "lexapublic@gmail.com", logcatArguments = { "-t", "1000", "-v", "long",
        "FileLogHelper:D",
        "MainActivity:D",
        "Retrofit:D",
        "ApiService:D",
        "*:S" }, customReportContent = {
        ReportField.APP_VERSION_CODE, ReportField.APP_VERSION_NAME,
        ReportField.ANDROID_VERSION, ReportField.PHONE_MODEL,
        ReportField.CUSTOM_DATA, ReportField.STACK_TRACE, ReportField.LOGCAT }, mode = ReportingInteractionMode.TOAST, resToastText = R.string.crash_toast_text
)

/**
 * Created by Alexey Rogovoy (lexapublic@gmail.com) on 12.02.2015.
 */
public class DonorInfoApp extends Application {
    @Override
    public void onCreate() {
        super.onCreate();
        if (!DebugDetect.isDebug(getApplicationContext()))
            ACRA.init(this); //init ACRA
        ParseObject.registerSubclass(AppUser.class);
        ParseObject.registerSubclass(ParseEvent.class);
        ParseObject.registerSubclass(ParseStation.class);
        ParseObject.registerSubclass(ParseCities.class);
        ParseObject.registerSubclass(ParseRegions.class);
        ParseObject.registerSubclass(ParseVersions.class);
        ParseObject.registerSubclass(ParseRequest.class);

        Parse.initialize(this
                , getString(R.string.parse_app_id)
                , getString(R.string.parse_client_key));
        Parse.setLogLevel(Parse.LOG_LEVEL_DEBUG);
        ParseInstallation.getCurrentInstallation().saveInBackground();
        /*
        String  android_id = Settings.Secure.getString(getApplicationContext().getContentResolver(), Settings.Secure.ANDROID_ID);
        ParseInstallation installation = ParseInstallation.getCurrentInstallation();
        installation.put("UniqueId",android_id);
        installation.saveInBackground();
        */
        PushService.startServiceIfRequired(getApplicationContext());
        FontsHelper.createInstance(getAssets());
    }

}
