package com.lexadev.donorinfo.model;

import android.provider.BaseColumns;

/**
 * Created by Alexey Rogovoy (lexapublic@gmail.com) on 24.02.2015.
 */
public class TableVersions implements BaseColumns {
    public static enum TABLES{
        REGIONS,
        CITIES,
        STATIONS
    }
    public static final String TABLE_NAME = "Versions";
    public static final String TABLE ="table_name";
    public static final String VERSION = "version";
    public static final String CREATE_TABLE =
            "CREATE TABLE "+TABLE_NAME+" ("
                    +_ID+" INTEGER PRIMARY KEY AUTOINCREMENT,"
                    +TABLE+" STRING,"
                    +VERSION+" INTEGER)";


}
