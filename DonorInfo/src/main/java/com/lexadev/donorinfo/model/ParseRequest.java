package com.lexadev.donorinfo.model;

import com.lexadev.donorinfo.AppConstants;
import com.lexadev.donorinfo.parse.AppUser;
import com.parse.ParseClassName;
import com.parse.ParseObject;
import com.parse.ParseQuery;

/**
 * Created by Alexey Rogovoy (lexapublic@gmail.com) on 05.03.2015.
 */
@ParseClassName(AppConstants.PARSE_TABLE_REQUEST)
public class ParseRequest extends ParseObject {

    public enum Status{
        NEW,
        APPROVED,
        REJECTED
    }
    public static final String REGION = "Region";
    public static final String CITY = "City";
    public static final String PHONE = "Phone";
    public static final String NAME = "Name";
    public static final String HOSPITAL="Hospital";
    public static final String ROOM="Room";
    public static final String BLOOD="Blood";
    public static final String NEEDED="Needed";
    public static final String NOTE="Note";
    public static final String STATUS = "Status";
    public static final String USER = "User";
    public static final String SHOW_NUMBER = "Show_Number";
    public String getRegion(){
        return getString(REGION);
    }
    public void setRegion(String region){
        put(REGION,region);
    }
    public String getCity() {
        return getString(CITY);
    }
    public void setCity(String city){
        put(CITY,city);
    }
    public String getPhone() {
        return getString(PHONE);
    }
    public void setPhone(String phone){
        put(PHONE,phone);
    }
    public String getName() {
        return getString(NAME);
    }

    public void setName(String name){
        put(NAME,name);
    }
    public String getHospital() {
        return getString(HOSPITAL);
    }
    public void setHospital(String hospital){
        put(HOSPITAL,hospital);
    }

    public String getRoom() {
        return getString(ROOM);
    }
    public void setRoom(String room){
        put(ROOM,room);
    }
    public String getBlood() {
        return getString(BLOOD);
    }
    public void setBlood(String blood){
        put(BLOOD,blood);
    }
    public String getNeeded() {
        return getString(NEEDED);
    }
    public void setNeeded(String needed){
        put(NEEDED,needed);
    }

    public String getNote() {
        return getString(NOTE);
    }

    public void setNote(String note){
        put(NOTE,note);
    }
    public Status getStatus() {
        String status = getString(STATUS);
        if (status==null)
            return Status.NEW;
        else
            return Status.valueOf(status);
    }
    public void setStatus(Status status){
        put(STATUS,status.name());
    }

    public boolean getShowNumber(){
        return getBoolean(SHOW_NUMBER);
    }
    public void setShowNumber(boolean showNumber){
        put(SHOW_NUMBER,showNumber);
    }


    public static final ParseQuery<ParseRequest> getRequest(){
        ParseQuery<ParseRequest> query = ParseQuery.getQuery(ParseRequest.class);
        return query;
    }

    public void setUser(AppUser user){
        put(USER,user);
    }
    public AppUser getUser(){
        return (AppUser)getParseUser(USER);
    }
    public static ParseQuery<ParseRequest> getRequests(String regionId, boolean isAdmin) {
        ParseQuery<ParseRequest> query = ParseQuery.getQuery(ParseRequest.class);
        if (regionId!=null) {
            if (regionId.length()>2){
                query.whereEqualTo(CITY,regionId);
            }
            else{
                query.whereEqualTo(REGION,regionId);
            }
        }
        if (!isAdmin){
            query.whereEqualTo(STATUS,Status.APPROVED.name());
        }

        query.orderByDescending("createdAt");
        return query;
    }

}

