package com.lexadev.donorinfo.model;

import android.provider.BaseColumns;

/**
 * Created by Alexey Rogovoy (lexapublic@gmail.com) on 24.02.2015.
 */
public class TableRegions implements BaseColumns {
    public static final String TABLE_NAME = "Regions";
    public static final String ID="region_id";
    public static final String NAME="region_name";
    public static final String SMALL_NAME="region_small_name";
    public static final String HAS_STATIONS="has_stations";
    public static final String CREATE_TABLE =
            "CREATE TABLE "+TABLE_NAME+" ("
                    +_ID+" INTEGER PRIMARY KEY AUTOINCREMENT,"
                    +NAME+" STRING,"
                    +ID+" STRING NOT NULL,"
                    +HAS_STATIONS+" INTEGER NOT NULL,"
                    +SMALL_NAME+" STRING)";

}
