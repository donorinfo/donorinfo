package com.lexadev.donorinfo.model;

import android.provider.BaseColumns;

/**
 * Created by Alexey Rogovoy (lexapublic@gmail.com) on 24.02.2015.
 */
public class TableStations implements BaseColumns{
    public static final String TABLE_NAME = "Stations";
    public static final String ID="station_id";
    public static final String CITY_ID="city_id";
    public static final String REGION_ID="region_id";
    public static final String NAME="name";
    public static final String ORGANISATION_NAME="organisation_name";
    public static final String ADDRESS="address";
    public static final String PHONE="phone";
    public static final String DAY_WORK="day_work";
    public static final String TIME_WORK="time_work";
    public static final String LATITUDE="c_lat";
    public static final String LONGITUDE="c_long";
    public static final String CREATE_TABLE =
            "CREATE TABLE "+TABLE_NAME+" ("
                    +_ID+" INTEGER PRIMARY KEY AUTOINCREMENT,"
                    +NAME+" STRING,"
                    +ID+" STRING NOT NULL,"
                    +CITY_ID+" STRING,"
                    +REGION_ID+" STRING,"
                    +ORGANISATION_NAME+" STRING,"
                    +ADDRESS+" STRING,"
                    +PHONE+" STRING,"
                    +DAY_WORK+" STRING,"
                    +TIME_WORK+" STRING,"
                    +LATITUDE+" REAL,"
                    +LONGITUDE+" REAL)";
}
