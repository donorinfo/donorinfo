package com.lexadev.donorinfo.model;

/**
 * Created by Alexey Rogovoy (lexapublic@gmail.com) on 25.02.2015.
 */
public class Station {
    private String id;
    private String cityId;
    private String regionId;
    private String name;
    private String nameOranisation;
    private String phone;
    private String dayWork;
    private String timeWork;
    private String address;
    private Float latitude;
    private Float longitude;

    public void setId(String id) {
        this.id = id;
    }

    public String getId() {
        return id;
    }

    public void setCityId(String cityId) {
        this.cityId = cityId;
    }

    public String getCityId() {
        return cityId;
    }

    public void setRegionId(String regionId) {
        this.regionId = regionId;
    }

    public String getRegionId() {
        return regionId;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getAddress() {
        return address;
    }

    public void setLatitude(Float latitude) {
        this.latitude = latitude;
    }

    public Float getLatitude() {
        return latitude;
    }

    public void setLongitude(Float longitude) {
        this.longitude = longitude;
    }

    public Float getLongitude() {
        return longitude;
    }

    public void setNameOranisation(String name) {
        this.nameOranisation = name;
    }

    public String getNameOranisation() {
        return nameOranisation;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getPhone() {
        return phone;
    }

    public void setDayWork(String dayWork) {
        this.dayWork = dayWork;
    }

    public String getDayWork() {
        return dayWork;
    }

    public void setTimeWork(String timeWork) {
        this.timeWork = timeWork;
    }

    public String getTimeWork() {
        return timeWork;
    }

}
