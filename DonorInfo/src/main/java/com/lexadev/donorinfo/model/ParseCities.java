package com.lexadev.donorinfo.model;

import com.lexadev.donorinfo.AppConstants;
import com.parse.ParseClassName;
import com.parse.ParseObject;
import com.parse.ParseQuery;

/**
 * Created by Alexey Rogovoy (lexapublic@gmail.com) on 24.02.2015.
 */
@ParseClassName(AppConstants.PARSE_TABLE_CITIES)
public class ParseCities extends ParseObject{
    public static final String ID="city_id";
    public static final String REGION_ID="region_id";
    public static final String NAME="city_name";
    public String getId(){
        return getString(ID);
    }
    public String getRegionId(){
        return getString(REGION_ID);
    }
    public String getName(){
        return getString(NAME);
    }
    public static final ParseQuery<ParseCities> getCitiesQuery(int count){
        ParseQuery<ParseCities> query = ParseQuery.getQuery(AppConstants.PARSE_TABLE_CITIES);
        query.setLimit(100);
        query.setSkip(count);
        return query;
    }

}
