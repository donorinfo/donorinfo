package com.lexadev.donorinfo.model;

import com.lexadev.donorinfo.AppConstants;
import com.parse.ParseClassName;
import com.parse.ParseObject;
import com.parse.ParseQuery;

/**
 * Created by Alexey Rogovoy (lexapublic@gmail.com) on 24.02.2015.
 */
@ParseClassName(AppConstants.PARSE_TABLE_VERSIONS)
public class ParseVersions extends ParseObject{
    public static final String REGIONS = "regions";
    public static final String CITIES = "cities";
    public static final String STATIONS = "stations";
    public int getRegionsVer(){
        return getInt(REGIONS);
    }
    public int getCitiesVer(){
        return getInt(CITIES);
    }
    public int getStationsVer(){
        return getInt(STATIONS);
    }
    public static ParseQuery<ParseVersions> queryVersions(){
        ParseQuery<ParseVersions> query = ParseQuery.getQuery(AppConstants.PARSE_TABLE_VERSIONS);
        query.setLimit(1);
        return query;
    }
}
