package com.lexadev.donorinfo.model;

import com.lexadev.donorinfo.AppConstants;
import com.parse.ParseClassName;
import com.parse.ParseObject;
import com.parse.ParseQuery;

/**
 * Created by Alexey Rogovoy (lexapublic@gmail.com) on 24.02.2015.
 */
@ParseClassName(AppConstants.PARSE_TABLE_REGIONS)
public class ParseRegions extends ParseObject {
    public static final String ID="region_id";
    public static final String NAME="region_name";
    public String getId(){
        return getString(ID);
    }
    public String getName(){
        return getString(NAME);
    }
    public static final ParseQuery<ParseRegions> getRegionsQuery(int count){
        ParseQuery<ParseRegions> query = ParseQuery.getQuery(ParseRegions.class);
        query.setLimit(100);
        query.setSkip(count);
        return query;
    }
}
