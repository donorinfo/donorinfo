package com.lexadev.donorinfo.model;

/**
 * Created by Alexey Rogovoy (lexapublic@gmail.com) on 25.02.2015.
 */
public class RegionsSearch {
    public String region;
    public String region_id;
    public String city;
    public String city_id;
    public RegionsSearch(String region,String region_id,String city,String city_id){
        this.region = region;
        this.region_id = region_id;
        this.city = city;
        this.city_id = city_id;
    }
}
