package com.lexadev.donorinfo.model;

import com.lexadev.donorinfo.AppConstants;
import com.parse.ParseClassName;
import com.parse.ParseObject;
import com.parse.ParseQuery;

/**
 * Created by Alexey Rogovoy (lexapublic@gmail.com) on 19.02.2015.
 */
@ParseClassName(AppConstants.PARSE_TABLE_STATIONS)
public class ParseStation extends ParseObject {
    public static final String ID="station_id";
    public static final String CITY_ID="city_id";
    public static final String REGION_ID="region_id";
    public static final String ORGANISATION_NAME="organisation_name";
    public static final String NAME="station_name";
    public static final String ADDRESS="address";
    public static final String PHONE="phone";
    public static final String DAY_WORK="day_work";
    public static final String TIME_WORK="time_work";
    public static final String LATITUDE="latitude";
    public static final String LONGITUDE="longitude";
    public String getId(){
        return getString(ID);
    }
    public String getCityId(){
        return getString(CITY_ID);
    }
    public String getRegionId(){
        return getString(REGION_ID);
    }
    public String getName(){
        return getString(NAME);
    }
    public String getOrganisationName(){
        return getString(ORGANISATION_NAME);
    }
    public String getPhone(){
        return getString(PHONE);
    }
    public String getDayWork(){
        return getString(DAY_WORK);
    }
    public String getTimeWork(){
        return getString(TIME_WORK);
    }
    public String getAddress(){
        return getString(ADDRESS);
    }
    public float getLatitude(){
        return (float)getDouble(LATITUDE);
    }
    public float getLongitude(){
        return (float)getDouble(LONGITUDE);
    }
    public static final ParseQuery<ParseStation> getStationsQuery(int count){
        ParseQuery<ParseStation> query = ParseQuery.getQuery(ParseStation.class);
        query.setLimit(100);
        query.setSkip(count);
        return query;
    }

}
