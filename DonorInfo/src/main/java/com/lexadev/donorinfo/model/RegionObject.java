package com.lexadev.donorinfo.model;

/**
 * Created by Alexey Rogovoy (lexapublic@gmail.com) on 05.03.2015.
 */
public class RegionObject {
    public String id;
    public String name;
    public RegionObject(String id,String name){
        this.id = id;
        this.name = name;
    }
}
