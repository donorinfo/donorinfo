package com.lexadev.donorinfo.model;

import android.content.Context;
import android.preference.PreferenceManager;

import com.lexadev.donorinfo.AppConstants;
import com.lexadev.donorinfo.helper.Utils;
import com.lexadev.donorinfo.parse.AppUser;
import com.parse.ParseClassName;
import com.parse.ParseObject;
import com.parse.ParseQuery;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.List;

/**
 * Created by Alexey Rogovoy (lexapublic@gmail.com) on 19.02.2015.
 */
@ParseClassName(AppConstants.PARSE_TABLE_EVENT)
public class ParseEvent extends ParseObject {
    public static final int STATUS_NONE = 0;
    public static final int STATUS_DONE = 1;


    public static final int BLOOD_EVENT = 0;
    public static final int DENIED_EVENT = 1;
    public static final int ANALIZE_EVENT = 2;
    public static final int REGION_EVENT = 3;
    public static final int EMPTY_EVENT = -1;
    public static final int BLOOD_EVENT_BLOOD = 0;
    public static final int BLOOD_EVENT_PLAZMA = 1;
    public static final int BLOOD_EVENT_TROMBO = 2;


    public static final String USER = "user";
    public static final String REGION = "region";
    public static final String IS_PERSONAL_EVENT = "personal_event";
//    public static final String DAY = "day";
    public static final String YEAR = "year";
    public static final String DATE = "date";
    public static final String EVENT = "event";
    public static final String EVENT_TYPE = "type";
    public static final String PLACE = "place";
    public static final String REMIND = "remind";
    public static final String NOTE = "note";
    public static final String STATUS = "status";
    public static final String DENIED_FINISHED = "denied_finished";
    public static final String DATE_REMIND = "date_remind";
    public static final String MANUAL_SAVE = "manual_save";
    public static final String AUTO_EVENTS = "auto_events";

    public void setDate(Date date) {
        put(DATE, date);
        if (date != null) {
            Calendar calendar = GregorianCalendar.getInstance();
            calendar.setTime(date);
            put(YEAR, calendar.get(Calendar.YEAR));
        } else {
            remove(YEAR);
        }
    }

    public Date getDate() {
        return getDate(DATE);
    }

    public int getYear() {
        return getInt(YEAR);
    }

    public void setEvent(int eventId) {
        put(EVENT, eventId);
        if (eventId != DENIED_EVENT) {
            remove(DENIED_FINISHED);
        }
    }

    public int getEvent() {
        return getInt(EVENT);
    }

    public void setEventType(int eventType) {
        put(EVENT_TYPE, eventType);
    }

    public int getEventType() {
        return getInt(EVENT_TYPE);
    }

    public void setPlace(String placeId) {
        if (placeId != null)
            put(PLACE, placeId);
        else
            remove(PLACE);
    }

    public String getPlace() {
        return getString(PLACE);
    }

    public void setRemind(int remind) {
        put(REMIND, remind);
    }

    public int getRemind() {
        return getInt(REMIND);
    }

    public void setNote(String note) {
        put(NOTE, note);
    }

    public String getNote() {
        return getString(NOTE);
    }

    public void setStatus(int status) {
        put(STATUS, status);
    }

    public int getStatus() {
        return getInt(STATUS);
    }

    public void setUser(AppUser user) {
        put(USER, user);
        put(IS_PERSONAL_EVENT, true);
        remove(REGION);
    }

    public AppUser getUser() {
        return (AppUser) getParseObject(USER);
    }

    public void setRegion(String region) {
        put(REGION, region);
        put(IS_PERSONAL_EVENT, false);
        remove(USER);
    }

    public String getRegion() {
        return getString(REGION);
    }

    public boolean isPersonal() {
        return getBoolean(IS_PERSONAL_EVENT);
    }

    public void setDeniedFinished(Date date) {
        put(DENIED_FINISHED, date);
    }

    public Date getDeniedFinished() {
        return getDate(DENIED_FINISHED);
    }

    public void setDateRemind(Date date) {
        put(DATE_REMIND, date);
    }

    public Date getDateRemind() {
        return getDate(DATE_REMIND);
    }

    public void removeDateRemind() {
        remove(DATE_REMIND);
    }

    public void setManualSave(boolean manualSave) {
        put(MANUAL_SAVE, manualSave);
    }

    public boolean getManualSave() {
        return getBoolean(MANUAL_SAVE);
    }

    public void setAutoEvents(List<ParseEvent> autoEvents) {
        if (autoEvents == null)
            remove(AUTO_EVENTS);
        else
            put(AUTO_EVENTS, autoEvents);
    }

    public List<ParseEvent> getAutoEvents() {
        return getList(AUTO_EVENTS);
    }

    public static ParseQuery<ParseEvent> getEvents(Date from, Date to, String region) {
        AppUser user = AppUser.getCurrentUser();
        List<ParseQuery<ParseEvent>> listQueries = new ArrayList<ParseQuery<ParseEvent>>();
        Date beginDate = Utils.getBeginDay(from);
        Date endDate = Utils.getEndDay(to);
        if (user != null) {
            ParseQuery<ParseEvent> personalQuery = ParseQuery.getQuery(ParseEvent.class);
            personalQuery.whereEqualTo(IS_PERSONAL_EVENT, true);
            personalQuery.whereEqualTo(USER, user);
            personalQuery.whereEqualTo(MANUAL_SAVE, true);
            personalQuery.whereGreaterThanOrEqualTo(DATE, beginDate);
            personalQuery.whereLessThanOrEqualTo(DATE, endDate);
            listQueries.add(personalQuery);
        }
        if (region != null) {
            ParseQuery<ParseEvent> regionQuery = ParseQuery.getQuery(ParseEvent.class);
            regionQuery.whereEqualTo(IS_PERSONAL_EVENT, false);
            regionQuery.whereEqualTo(REGION, region);
            regionQuery.whereGreaterThanOrEqualTo(DATE, beginDate);
            regionQuery.whereLessThanOrEqualTo(DATE, endDate);
            listQueries.add(regionQuery);
        }
        if (listQueries.size() == 0)
            return null;
        ParseQuery<ParseEvent> query = ParseQuery.or(listQueries);
        return query;
    }

    public static ParseQuery<ParseEvent> getLastBloodEvent(int eventType) {
        AppUser user = AppUser.getCurrentUser();
        if (user == null) {
            return null;
        }
        ParseQuery<ParseEvent> query = ParseQuery.getQuery(ParseEvent.class);
        query.whereEqualTo(IS_PERSONAL_EVENT, true);
        query.whereEqualTo(USER, user);
        query.whereNotEqualTo(MANUAL_SAVE, true);
        query.whereEqualTo(EVENT, BLOOD_EVENT);
        query.whereEqualTo(EVENT_TYPE, eventType);
        query.orderByDescending(DATE);
        return query;
    }

    public static ParseQuery<ParseEvent> checkExistEvent(Date day, ParseEvent event) {
        Date beginDate = Utils.getBeginDay(day);
        Date endDate = Utils.getEndDay(day);
        ParseQuery<ParseEvent> query = ParseQuery.getQuery(ParseEvent.class);
        query.whereEqualTo(IS_PERSONAL_EVENT, true);
        query.whereEqualTo(USER, AppUser.getCurrentUser());
        query.whereGreaterThanOrEqualTo(DATE, beginDate);
        query.whereLessThanOrEqualTo(DATE, endDate);
        query.whereEqualTo(EVENT, event.getEvent());
        if (event.getEvent() == BLOOD_EVENT)
            query.whereEqualTo(EVENT_TYPE, event.getEventType());
        if (event.getObjectId() != null)
            query.whereNotEqualTo("objectId", event.getObjectId());
        return query;
    }

    public static ParseQuery<ParseEvent> getEventQuery() {
        ParseQuery<ParseEvent> query = ParseQuery.getQuery(ParseEvent.class);
        query.include(AUTO_EVENTS);
        return query;
    }
    public static ParseQuery<ParseEvent> getExistsDeniedEvents(Date date) {
        AppUser user = AppUser.getCurrentUser();
        if (user == null)
            return null;
        Date endDate = Utils.getEndDay(date);
        ParseQuery<ParseEvent> query = ParseQuery.getQuery(ParseEvent.class);
        query.whereEqualTo(IS_PERSONAL_EVENT, true);
        query.whereEqualTo(USER, user);
        query.whereEqualTo(STATUS, STATUS_DONE);
        query.whereNotEqualTo(EVENT, ANALIZE_EVENT);
        query.whereLessThanOrEqualTo(DATE, endDate);
        query.whereGreaterThanOrEqualTo(DENIED_FINISHED, endDate);
        query.orderByDescending(DENIED_FINISHED);
        return query;
    }

    public static ParseQuery<ParseEvent> getReminds() {
        AppUser user = AppUser.getCurrentUser();
        if (user == null)
            return null;
        Calendar calendar = GregorianCalendar.getInstance();
        calendar.set(Calendar.HOUR_OF_DAY,0);
        calendar.set(Calendar.MINUTE,0);
        calendar.set(Calendar.SECOND,0);
        calendar.set(Calendar.MILLISECOND,0);
        Date date = calendar.getTime();
        ParseQuery<ParseEvent> query = ParseQuery.getQuery(ParseEvent.class);
        query.whereEqualTo(IS_PERSONAL_EVENT, true);
        query.whereEqualTo(USER, user);
        query.whereEqualTo(MANUAL_SAVE, true);
        query.whereGreaterThanOrEqualTo(DATE_REMIND, date);
        query.orderByAscending(DATE_REMIND);
        return query;
    }

    public static ParseQuery<ParseEvent> getOldestEvents() {
        AppUser user = AppUser.getCurrentUser();
        if (user == null)
            return null;
        ParseQuery<ParseEvent> query = ParseQuery.getQuery(ParseEvent.class);
        Calendar calendar = Calendar.getInstance();
        calendar.set(Calendar.HOUR_OF_DAY,0);
        calendar.set(Calendar.MINUTE,0);
        calendar.set(Calendar.SECOND,0);
        calendar.set(Calendar.MILLISECOND, 0);
        calendar.add(Calendar.DAY_OF_YEAR, -10);
        query.whereEqualTo(IS_PERSONAL_EVENT, true);
        query.whereEqualTo(USER, user);
        query.whereNotEqualTo(STATUS, STATUS_DONE);
        query.whereEqualTo(EVENT,BLOOD_EVENT);
        query.whereNotEqualTo(MANUAL_SAVE,true);
        query.whereLessThan(DATE, calendar.getTime());
        return query;
    }
    public static ParseQuery<ParseEvent> getNotificationEvents() {
        AppUser user = AppUser.getCurrentUser();
        if (user == null)
            return null;

        Calendar calendar = GregorianCalendar.getInstance();
        calendar.add(Calendar.DAY_OF_YEAR,-1);
        Date day1b = Utils.getBeginDay(calendar.getTime());
        Date day1e = Utils.getEndDay(calendar.getTime());
        calendar.add(Calendar.DAY_OF_YEAR,-8);
        Date day9b = Utils.getBeginDay(calendar.getTime());
        Date day9e = Utils.getEndDay(calendar.getTime());
        List<ParseQuery<ParseEvent>> queries = new ArrayList<>();
        ParseQuery<ParseEvent> query1 = ParseQuery.getQuery(ParseEvent.class);
        query1.whereEqualTo(IS_PERSONAL_EVENT, true);
        query1.whereEqualTo(USER, user);
        query1.whereNotEqualTo(STATUS,STATUS_DONE);
        query1.whereEqualTo(EVENT,BLOOD_EVENT);
        query1.whereGreaterThanOrEqualTo(DATE, day1b);
        query1.whereLessThanOrEqualTo(DATE, day1e);

        ParseQuery<ParseEvent> query9 = ParseQuery.getQuery(ParseEvent.class);
        query9.whereEqualTo(IS_PERSONAL_EVENT, true);
        query9.whereEqualTo(USER, user);
        query9.whereNotEqualTo(STATUS,STATUS_DONE);
        query9.whereEqualTo(EVENT,BLOOD_EVENT);
        query9.whereGreaterThanOrEqualTo(DATE, day9b);
        query9.whereLessThanOrEqualTo(DATE, day9e);
        queries.add(query1);
        queries.add(query9);
        return ParseQuery.or(queries);
    }


    public static ParseQuery<ParseEvent> getRegionsEvents() {
        ParseQuery<ParseEvent> query = ParseQuery.getQuery(ParseEvent.class);
        query.whereEqualTo(IS_PERSONAL_EVENT, false);
        query.orderByDescending(ParseEvent.DATE);
        return query;
    }

    public static ParseQuery<ParseEvent> getEventsYear(int currentYear, int skip) {
        ParseQuery<ParseEvent> query = ParseQuery.getQuery(ParseEvent.class);
        query.whereEqualTo(IS_PERSONAL_EVENT, true);
        query.whereEqualTo(YEAR,currentYear);
        query.whereEqualTo(USER,AppUser.getCurrentUser());
        query.whereEqualTo(MANUAL_SAVE,true);
        if (skip>0){
            query.setSkip(skip);
            query.setLimit(AppConstants.MAX_QUERY_LIMIT);
        }
        query.orderByDescending(ParseEvent.DATE);
        return query;
    }

    public static ParseQuery<ParseEvent> getRegionReminds(Context context) {
        String region = null;
        region = PreferenceManager.getDefaultSharedPreferences(context).getString(AppConstants.PREF_SELECTED_REGION, null);
        if (region==null&& AppUser.getCurrentUser()!=null){
            region = AppUser.getCurrentUser().getRegion();
        }
        if (region==null)
            return null;

        Calendar calendarStart = GregorianCalendar.getInstance();
        calendarStart.add(Calendar.DAY_OF_MONTH,1);
        calendarStart.set(Calendar.HOUR_OF_DAY,0);
        calendarStart.set(Calendar.MINUTE,0);
        calendarStart.set(Calendar.SECOND,0);
        calendarStart.set(Calendar.MILLISECOND, 0);

        Calendar calendarEnd = GregorianCalendar.getInstance();
        calendarEnd.add(Calendar.DAY_OF_MONTH,1);
        calendarEnd.set(Calendar.HOUR_OF_DAY,calendarEnd.getActualMaximum(Calendar.HOUR_OF_DAY));
        calendarEnd.set(Calendar.MINUTE,calendarEnd.getActualMaximum(Calendar.MINUTE));
        calendarEnd.set(Calendar.SECOND,calendarEnd.getActualMaximum(Calendar.SECOND));
        calendarEnd.set(Calendar.MILLISECOND,calendarEnd.getActualMaximum(Calendar.MILLISECOND));

        ParseQuery<ParseEvent> query = ParseQuery.getQuery(ParseEvent.class);
        query.whereEqualTo(IS_PERSONAL_EVENT, false);
        query.whereEqualTo(REGION, region);
        query.whereGreaterThanOrEqualTo(DATE, calendarStart);
        query.whereLessThanOrEqualTo(DATE, calendarEnd);
        List<ParseQuery<ParseEvent>> list = new ArrayList<>();
        list.add(query);
        calendarStart.add(Calendar.DAY_OF_MONTH,7);
        calendarEnd.add(Calendar.DAY_OF_MONTH,7);
        query = ParseQuery.getQuery(ParseEvent.class);
        query.whereEqualTo(IS_PERSONAL_EVENT, false);
        query.whereEqualTo(REGION, region);
        query.whereGreaterThanOrEqualTo(DATE, calendarStart);
        query.whereLessThanOrEqualTo(DATE, calendarEnd);
        list.add(query);
        ParseQuery<ParseEvent> queryLast = ParseQuery.or(list);
        return queryLast;
    }
}
