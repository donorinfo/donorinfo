package com.lexadev.donorinfo;

import android.app.AlarmManager;
import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.IBinder;
import android.preference.PreferenceManager;
import android.support.v4.app.NotificationCompat;

import com.lexadev.donorinfo.model.ParseEvent;
import com.lexadev.donorinfo.parse.AppUser;
import com.parse.DeleteCallback;
import com.parse.FindCallback;
import com.parse.GetCallback;
import com.parse.ParseException;
import com.parse.ParseObject;
import com.parse.ParseQuery;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.List;

public class EventRemindService extends Service {
    private static final String ACTION_UPDATE_REMIND = "com.lexadev.donorinfo.ACTION_UPDATE_REMIND";
    private static final String ACTION_UPDATE_SERVICE_REMIND = "com.lexadev.donorinfo.ACTION_UPDATE_SERVICE_REMIND";
    private static final String ACTION_REMIND = "com.lexadev.donorinfo.ACTION_REMIND";
    private static final String ACTION_REGION_REMIND = "com.lexadev.donorinfo.ACTION_SERVICE_REMIND";
    private static final String ACTION_SERVICE_REMIND = "com.lexadev.donorinfo.ACTION_REGION_REMIND";
    private static final String ACTION_CLICK_ICON = "com.lexadev.donorinfo.ACTION_CLICK_ICON";
    private static final String PREF_LAST_SERVICE_DAY = "pref_last_service_date";
    private static final String TAG = "EventRemindService";
    private int mNotificationId = 1;
    private int mNotificationOldId = 2;
    private boolean mIsIconShowed = false;
    private boolean mIsIconOldShowed = false;
    private boolean mIsIconRegionShowed = false;
    private int mNotificationRegionId = 3;

    public EventRemindService() {
    }
    public static void startUpdateRemind(Context context) {
        Intent intent = new Intent(context, EventRemindService.class);
        intent.setAction(ACTION_UPDATE_REMIND);
        context.startService(intent);
    }
    public static void startUpdateServiceRemind(Context context) {
        Intent intent = new Intent(context, EventRemindService.class);
        intent.setAction(ACTION_UPDATE_SERVICE_REMIND);
        context.startService(intent);
    }
    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        handleCommand(intent);
        return START_STICKY;
    }

    private void handleCommand(Intent intent) {
        if (intent == null) {
            return;
        }
        String action = intent.getAction();
        if (ACTION_UPDATE_REMIND.equals(action)) {
            handleActionUpdateRemind();
        }
        else if (ACTION_REMIND.equals(action)){
            handleActionRemind();
            handleActionUpdateRemind();
        }
        else if (ACTION_SERVICE_REMIND.equals(action)){
            requestNextServiceRemind();
        }
        else if (ACTION_UPDATE_SERVICE_REMIND.equals(action)) {
            handleActionUpdateServiceRemind();
        }
        else if (ACTION_REGION_REMIND.equals(action)){
            handleActionRegionRemind();
        }
        else if (ACTION_CLICK_ICON.equals(action)){
            showActivity();
        }
        tryStopService();
    }

    private void showActivity() {
        NotificationManager mNotificationManager =
                (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
        mNotificationManager.cancelAll();
        mIsIconShowed = false;
        Intent intent = new Intent(ACTION_REMIND,null,this, MainActivity.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        startActivity(intent);
    }

    private void tryStopService() {
        if (!mIsIconShowed&&!mIsIconOldShowed&&!mIsIconRegionShowed)
            stopSelf();
    }

    private void handleActionRemind() {
        NotificationCompat.Builder mBuilder =
                new NotificationCompat.Builder(this)
                        .setSmallIcon(R.drawable.ic_launcher)
                        .setContentTitle(getString(R.string.app_name))
                        .setDefaults(Notification.DEFAULT_SOUND)
                        .setContentText(getString(R.string.text_notification));

        mBuilder.setContentIntent(getPendingIntentClick());
        NotificationManager mNotificationManager =
                (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
        mIsIconShowed = true;
        mNotificationManager.notify(mNotificationId, mBuilder.build());
    }

    private void handleActionRegionRemind() {
        NotificationCompat.Builder mBuilder =
                new NotificationCompat.Builder(this)
                        .setSmallIcon(R.drawable.ic_launcher)
                        .setContentTitle(getString(R.string.app_name))
                        .setDefaults(Notification.DEFAULT_SOUND)
                        .setContentText(getString(R.string.text_region_notification));

        mBuilder.setContentIntent(getPendingIntentClick());
        NotificationManager mNotificationManager =
                (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
        mIsIconRegionShowed = true;
        mNotificationManager.notify(mNotificationRegionId, mBuilder.build());
    }

    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }
    private PendingIntent getPendingIntentClick(){
        Intent intent = new Intent(this,EventRemindService.class);
        intent.setAction(ACTION_CLICK_ICON);
        PendingIntent pIntent = PendingIntent.getService(this, 0, intent, 0);
        return pIntent;
    }

    private PendingIntent getPendingIntent(){
        Intent intent = new Intent(this,EventRemindService.class);
        intent.setAction(ACTION_REMIND);
        PendingIntent pIntent = PendingIntent.getService(this, 0, intent, PendingIntent.FLAG_CANCEL_CURRENT);
        return pIntent;
    }
    private PendingIntent getPendingServiceIntent(){
        Intent intent = new Intent(this,EventRemindService.class);
        intent.setAction(ACTION_SERVICE_REMIND);
        PendingIntent pIntent = PendingIntent.getService(this, 0, intent, PendingIntent.FLAG_CANCEL_CURRENT);
        return pIntent;
    }

    private void createNewRemind(ParseEvent parseEvent) {
        clearPreviousRemind();
        if (parseEvent==null)
            return;
        AlarmManager am = (AlarmManager)getSystemService(ALARM_SERVICE);
        am.set(AlarmManager.RTC_WAKEUP,parseEvent.getDateRemind().getTime(),getPendingIntent());
    }

    private void clearPreviousRemind() {
        AlarmManager am = (AlarmManager)getSystemService(ALARM_SERVICE);
        am.cancel(getPendingIntent());
    }

    private void handleActionUpdateRemind() {
        AppUser user = AppUser.getCurrentUser();
        if (user==null)
            return;
        requestNextRemind();
    }

    private List<ParseEvent> mEvents = new ArrayList<>();
    private ParseEvent mBloodEvent = null;
    private ParseEvent mPlazmaEvent = null;
    private ParseEvent mTromboEvent = null;

    private boolean mEventQueried = false;
    private boolean mBloodQueried = false;
    private boolean mPlazmaQueried = false;
    private boolean mTromboQueried = false;

    private void requestNextRemind() {
        ParseQuery<ParseEvent> query = ParseEvent.getReminds();
        if (query != null) {
            query.setLimit(20);
            query.findInBackground(new FindCallback<ParseEvent>() {
                @Override
                public void done(List<ParseEvent> parseEvents, ParseException e) {
                    if (e == null) {
                        if (parseEvents != null)
                            mEvents = parseEvents;
                    }
                    mEventQueried = true;
                    createNextRemind();
                }
            });
        }
        else{
            mEventQueried = true;
            createNextRemind();
        }
        ParseQuery<ParseEvent> queryBlood = ParseEvent.getLastBloodEvent(ParseEvent.BLOOD_EVENT_BLOOD);
        if (queryBlood != null) {
            queryBlood.getFirstInBackground(new GetCallback<ParseEvent>() {
                @Override
                public void done(ParseEvent parseEvent, ParseException e) {
                    if (e == null) {
                        mBloodEvent = parseEvent;
                    }
                    mBloodQueried = true;
                    createNextRemind();
                }
            });
        } else {
            mBloodQueried = true;
            createNextRemind();

        }
        ParseQuery<ParseEvent> queryPlazma = ParseEvent.getLastBloodEvent(ParseEvent.BLOOD_EVENT_PLAZMA);
        if (queryPlazma != null) {
            queryPlazma.getFirstInBackground(new GetCallback<ParseEvent>() {
                @Override
                public void done(ParseEvent parseEvent, ParseException e) {
                    if (e == null) {
                        mPlazmaEvent = parseEvent;
                    }
                    mPlazmaQueried = true;
                    createNextRemind();
                }
            });
        } else {
            mPlazmaQueried = true;
            createNextRemind();

        }
        ParseQuery<ParseEvent> queryTrombo = ParseEvent.getLastBloodEvent(ParseEvent.BLOOD_EVENT_TROMBO);
        if (queryTrombo != null) {
            queryTrombo.getFirstInBackground(new GetCallback<ParseEvent>() {
                @Override
                public void done(ParseEvent parseEvent, ParseException e) {
                    if (e == null) {
                        mTromboEvent = parseEvent;
                    }
                    mTromboQueried = true;
                    createNextRemind();
                }
            });
        } else {
            mTromboQueried = true;
            createNextRemind();

        }
    }

    private void createNextRemind() {
        if (!mEventQueried||!mBloodQueried||!mPlazmaQueried||!mTromboQueried)
            return;
        Date today = new Date(System.currentTimeMillis());
        ParseEvent nearestRemind = null;
        for (int i=0;i<mEvents.size();i++){
            if (mEvents.get(i).getDateRemind().after(today)){
                nearestRemind = mEvents.get(i);
                break;
            }
        }
        if (checkDate(mBloodEvent))
            mBloodEvent=null;
        if (checkDate(mPlazmaEvent))
            mPlazmaEvent = null;
        if (checkDate(mTromboEvent))
            mTromboEvent = null;
        if (mBloodEvent!=null){
            Date dtRemind = mBloodEvent.getDateRemind();
            if (dtRemind!=null && dtRemind.after(today))
                if (nearestRemind==null || nearestRemind.getDateRemind().after(dtRemind))
                    nearestRemind = mBloodEvent;
        }
        if (mPlazmaEvent!=null){
            Date dtRemind = mPlazmaEvent.getDateRemind();
            if (dtRemind!=null && dtRemind.after(today))
                if (nearestRemind==null || nearestRemind.getDateRemind().after(dtRemind))
                    nearestRemind = mPlazmaEvent;
        }
        if (mTromboEvent!=null){
            Date dtRemind = mTromboEvent.getDateRemind();
            if (dtRemind!=null && dtRemind.after(today))
                if (nearestRemind==null || nearestRemind.getDateRemind().after(dtRemind))
                    nearestRemind = mTromboEvent;
        }
        createNewRemind(nearestRemind);

    }

    private boolean checkDate(ParseEvent event) {
        if (event==null)
            return true;
        Calendar calendar = GregorianCalendar.getInstance();
        calendar.setTime(event.getDate());
        for (ParseEvent eventList:mEvents){
            if (eventList.getEvent()==ParseEvent.BLOOD_EVENT && eventList.getEventType()==event.getEventType()) {
                Calendar calendarList = GregorianCalendar.getInstance();
                calendarList.setTime(eventList.getDate());
                if (calendar.get(Calendar.YEAR)==calendarList.get(Calendar.YEAR)&&
                    calendar.get(Calendar.MONTH)==calendarList.get(Calendar.MONTH)&&
                    calendar.get(Calendar.DAY_OF_MONTH)==calendarList.get(Calendar.DAY_OF_MONTH))
                    return true;
            }
        }
        return false;
    }

    private void checkRegionRemind() {
        ParseQuery<ParseEvent> query = ParseEvent.getRegionReminds(getApplicationContext());
        if (query != null) {
            query.getFirstInBackground(new GetCallback<ParseEvent>() {
                @Override
                public void done(ParseEvent parseEvent, ParseException e) {
                    if (e == null) {
                        if (parseEvent != null) {
                            handleActionRegionRemind();
                        }
                    }
                }
            });
        }
    }

    private void handleActionUpdateServiceRemind() {
        AppUser user = AppUser.getCurrentUser();
        if (user==null)
            return;
        requestNextServiceRemind();
    }

    private void requestNextServiceRemind() {
        clearPreviousServiceRemind();
        Calendar calendar = GregorianCalendar.getInstance();
        if (calendar.get(Calendar.HOUR_OF_DAY)>=10){
            if (!checkThisDayService())
                doService();
            calendar.add(Calendar.DAY_OF_YEAR,1);
        }
        calendar.set(Calendar.HOUR_OF_DAY,10);
        calendar.set(Calendar.MINUTE,0);
        calendar.set(Calendar.SECOND,0);
        calendar.set(Calendar.MILLISECOND, 0);
        createNextServiceEvent(calendar);
    }

    private void doService() {
        ParseQuery<ParseEvent> queryDelete = ParseEvent.getOldestEvents();
        if (queryDelete!=null){
            queryDelete.findInBackground(new FindCallback<ParseEvent>() {
                @Override
                public void done(List<ParseEvent> parseEvents, ParseException e) {
                    if (parseEvents!=null && parseEvents.size()>0)
                        ParseObject.deleteAllInBackground(parseEvents,new DeleteCallback() {
                            @Override
                            public void done(ParseException e) {
                                if (e==null){
                                    Intent intent = new Intent(AppConstants.ACTION_EVENT_BROADCAST);
                                    sendBroadcast(intent);
                                }
                            }
                        });
                }
            });
        }
        ParseQuery<ParseEvent> queryNotification = ParseEvent.getNotificationEvents();
        if (queryNotification!=null){
            queryNotification.findInBackground(new FindCallback<ParseEvent>() {
                @Override
                public void done(List<ParseEvent> parseEvents, ParseException e) {
                    if (parseEvents!=null&&parseEvents.size()>0){
                        showNotificationsOld(parseEvents.size());
                    }
                }
            });
        }
        checkRegionRemind();
        SharedPreferences settings = PreferenceManager.getDefaultSharedPreferences(this);
        settings.edit().putLong(PREF_LAST_SERVICE_DAY,System.currentTimeMillis()).commit();
    }

    private void showNotificationsOld(int size) {
        NotificationCompat.Builder mBuilder =
                new NotificationCompat.Builder(this)
                        .setSmallIcon(R.drawable.ic_launcher)
                        .setContentTitle(getString(R.string.app_name))
                        .setDefaults(Notification.DEFAULT_SOUND)
                        .setContentText(getString(R.string.text_old_notification));

        mBuilder.setContentIntent(getPendingIntentClick());
        NotificationManager mNotificationManager =
                (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
        mIsIconOldShowed = true;
        mNotificationManager.notify(mNotificationOldId, mBuilder.build());

    }

    private void createNextServiceEvent(Calendar calendar) {
        AlarmManager am = (AlarmManager)getSystemService(ALARM_SERVICE);
        am.set(AlarmManager.RTC_WAKEUP,calendar.getTimeInMillis(),getPendingServiceIntent());
    }

    private void clearPreviousServiceRemind() {
            AlarmManager am = (AlarmManager)getSystemService(ALARM_SERVICE);
            am.cancel(getPendingServiceIntent());
    }

    private boolean checkThisDayService(){
        SharedPreferences settings = PreferenceManager.getDefaultSharedPreferences(this);
        long lastServiceDate = settings.getLong(PREF_LAST_SERVICE_DAY,0);
        Calendar calendarLast = GregorianCalendar.getInstance();
        calendarLast.setTimeInMillis(lastServiceDate);
        Calendar calendarToday = GregorianCalendar.getInstance();
        if (calendarToday.get(Calendar.YEAR)==calendarLast.get(Calendar.YEAR)&&
            calendarToday.get(Calendar.MONTH)==calendarLast.get(Calendar.MONTH)&&
            calendarToday.get(Calendar.DAY_OF_MONTH)==calendarLast.get(Calendar.DAY_OF_MONTH))
            return true;
        return false;
    }
}
