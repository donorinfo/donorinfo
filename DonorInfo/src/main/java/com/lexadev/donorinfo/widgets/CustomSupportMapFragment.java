package com.lexadev.donorinfo.widgets;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.google.android.gms.maps.SupportMapFragment;

public class CustomSupportMapFragment extends SupportMapFragment{
	// Callback for results

	private MapViewCreatedListener itsMapViewCreatedListener = null; 
	public abstract static class MapViewCreatedListener {
	    public abstract void onMapCreated();
	}
	@Override
	public View onCreateView (LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
	    View view = super.onCreateView(inflater, container, savedInstanceState);
	    // Notify the view has been created
	    if( itsMapViewCreatedListener != null ) {
	        itsMapViewCreatedListener.onMapCreated();
	    }
	    return view;
	}
	public void setOnMapCreatedListener(MapViewCreatedListener listener){
		itsMapViewCreatedListener = listener;
	}
}
