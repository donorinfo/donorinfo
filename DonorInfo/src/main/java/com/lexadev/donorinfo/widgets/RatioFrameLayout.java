package com.lexadev.donorinfo.widgets;

import android.content.Context;
import android.content.res.TypedArray;
import android.util.AttributeSet;
import android.widget.FrameLayout;

import com.lexadev.donorinfo.R;


public class RatioFrameLayout extends FrameLayout {
	private static final float ZERO_RATIO = 0f;
	private float ratio = ZERO_RATIO;
	

	public RatioFrameLayout(Context context) {
		super(context);
		init(context,null);
	}

	public RatioFrameLayout(Context context, AttributeSet attrs) {
		super(context, attrs);
		init(context,attrs);
	}
	public RatioFrameLayout(Context context, AttributeSet attrs, int defStyle) {
		super(context, attrs, defStyle);
		init(context,attrs);
	}
	private void init(Context context,AttributeSet attrs) {
		TypedArray arr = context.obtainStyledAttributes(attrs,
				R.styleable.RatioFrameLayout);
		ratio = arr.getFloat(R.styleable.RatioFrameLayout_ratio,ZERO_RATIO);

		arr.recycle();
	}

	@Override
	protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
		super.onMeasure(widthMeasureSpec, heightMeasureSpec);
		if (ratio==ZERO_RATIO){
			return;
		}
		else{
			// get maximum view size
			int width = MeasureSpec.getSize(widthMeasureSpec);
			int height = (int) (width / ratio);
	
			if (width != 0) {
				heightMeasureSpec = MeasureSpec.makeMeasureSpec(height,
						MeasureSpec.EXACTLY);
				widthMeasureSpec = MeasureSpec.makeMeasureSpec(width,
						MeasureSpec.EXACTLY);
			}
			super.onMeasure(widthMeasureSpec, heightMeasureSpec);
		}
	}
}
