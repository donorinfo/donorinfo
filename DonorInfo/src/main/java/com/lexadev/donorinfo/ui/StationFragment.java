package com.lexadev.donorinfo.ui;

import android.app.Activity;
import android.content.Context;
import android.graphics.Paint;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;

import com.lexadev.donorinfo.OnFragmentInteractionListener;
import com.lexadev.donorinfo.R;
import com.lexadev.donorinfo.helper.DbAdapter;
import com.lexadev.donorinfo.helper.FontsHelper;
import com.lexadev.donorinfo.model.Station;

import java.util.List;

public class StationFragment extends Fragment {
    private static final String ARG_PARENT = "parent";
    private static final String ARG_SELECTION_MODE = "selection_mode";
    private static final String PARENT_ID = "parent_id";

    private String mParentId;

    private TextView mTextRegion;
    private ListView mListStations;
    private boolean mSelectionMode = false;
    private OnFragmentInteractionListener mListener;
    private List<Station> mStations;

    public static StationFragment newInstance(String parentId,boolean selectionMode) {
        StationFragment fragment = new StationFragment();
        Bundle args = new Bundle();
        args.putString(ARG_PARENT, parentId);
        args.putBoolean(ARG_SELECTION_MODE, selectionMode);
        fragment.setArguments(args);
        return fragment;
    }

    public StationFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (savedInstanceState!=null){
            mParentId = savedInstanceState.getString(PARENT_ID);
        }
        if (getArguments() != null) {
            mParentId = getArguments().getString(ARG_PARENT);
            mSelectionMode = getArguments().getBoolean(ARG_SELECTION_MODE);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_station, container, false);
        if (mListener!=null)
            mListener.setupUI(rootView);
        handleViews(rootView);
        String name = null;
        if (mParentId==null){
            name = getString(R.string.item_all_regions);
        }
        else{
            name = DbAdapter.getInstance(getActivity()).getRegionName(mParentId);
        }
        mTextRegion.setText(name);
        getStationsList();
        return rootView;
    }

    private void getStationsList() {
        mStations = DbAdapter.getInstance(getActivity()).getStationList(mParentId);
        StationsAdapter adapter = new StationsAdapter(getActivity(),mStations,getChildFragmentManager());
        mListStations.setAdapter(adapter);
    }


    private static class StationsAdapter extends ArrayAdapter<Station>{
        private List<Station> mItems;
        private LayoutInflater mInflater;
        private FragmentManager mFm;

        public StationsAdapter(Context context, List<Station> items,FragmentManager childFm) {
            super(context, 0,items);
            mFm = childFm;
            mItems = items;
            mInflater = LayoutInflater.from(context);
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {
            ViewsHolder holder = null;
            if (convertView==null){
                convertView = mInflater.inflate(R.layout.station_item,parent,false);
                holder = new ViewsHolder(convertView);
            }
            else{
                holder = (ViewsHolder)convertView.getTag();
            }
            holder.build(position);
            return convertView;
        }

        private class ViewsHolder {
            LinearLayout layoutAddress;
            LinearLayout layoutWorks;
            LinearLayout layoutPhone;
            TextView name;
            TextView address;
            TextView works;
            TextView phone;
            TextView location;
            public ViewsHolder(View rootView) {
                layoutAddress = (LinearLayout)rootView.findViewById(R.id.layout_address);
                layoutWorks = (LinearLayout)rootView.findViewById(R.id.layout_works);
                layoutPhone = (LinearLayout)rootView.findViewById(R.id.layout_phone);
                name = (TextView)rootView.findViewById(R.id.name);
                address = (TextView)rootView.findViewById(R.id.address);
                works = (TextView)rootView.findViewById(R.id.works);
                phone = (TextView)rootView.findViewById(R.id.phone);
                location = (TextView)rootView.findViewById(R.id.location);
                location.setPaintFlags(location.getPaintFlags() | Paint.UNDERLINE_TEXT_FLAG);
                name.setTypeface(FontsHelper.getFont(FontsHelper.FontName.ROBOTO, FontsHelper.FontsStyle.BOLD));
                address.setTypeface(FontsHelper.getFont(FontsHelper.FontName.ROBOTO, FontsHelper.FontsStyle.REGULAR));
                works.setTypeface(FontsHelper.getFont(FontsHelper.FontName.ROBOTO, FontsHelper.FontsStyle.REGULAR));
                phone.setTypeface(FontsHelper.getFont(FontsHelper.FontName.ROBOTO, FontsHelper.FontsStyle.REGULAR));
                TextView title = (TextView)rootView.findViewById(R.id.address_title);
                title.setTypeface(FontsHelper.getFont(FontsHelper.FontName.ROBOTO, FontsHelper.FontsStyle.MEDIUM));
                title = (TextView)rootView.findViewById(R.id.works_title);
                title.setTypeface(FontsHelper.getFont(FontsHelper.FontName.ROBOTO, FontsHelper.FontsStyle.MEDIUM));
                title = (TextView)rootView.findViewById(R.id.phone_title);
                title.setTypeface(FontsHelper.getFont(FontsHelper.FontName.ROBOTO, FontsHelper.FontsStyle.MEDIUM));
                location.setTypeface(FontsHelper.getFont(FontsHelper.FontName.ROBOTO, FontsHelper.FontsStyle.REGULAR));
                rootView.setTag(this);
            }

            public void build(int position) {
                final Station item = getItem(position);
                name.setText(item.getName());
                if (item.getAddress()!=null) {
                    layoutAddress.setVisibility(View.VISIBLE);
                    address.setText(item.getAddress());
                }
                else
                    layoutAddress.setVisibility(View.GONE);

                if (item.getDayWork()!=null||item.getTimeWork()!=null) {
                    layoutWorks.setVisibility(View.VISIBLE);
                    String worksStr="";
                    if (item.getDayWork()!=null)
                        worksStr+=item.getDayWork();
                    if (item.getTimeWork()!=null) {
                        if (worksStr.length()>0)
                            worksStr+=", ";
                        worksStr+=item.getTimeWork();
                    }
                    works.setText(worksStr);
                }
                else
                    layoutWorks.setVisibility(View.GONE);
                if (item.getPhone()!=null) {
                    layoutPhone.setVisibility(View.VISIBLE);
                    phone.setText(item.getPhone());
                }
                else
                    layoutPhone.setVisibility(View.GONE);

                if (item.getLatitude()!=0&&item.getLongitude()!=null) {
                    location.setVisibility(View.VISIBLE);
                    location.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            StationMapFragment fragment = StationMapFragment.newInstance(item);
                            fragment.show(mFm, "MAP");
                        }
                    });
                }
                else{
                    location.setVisibility(View.GONE);
                }
            }
        }
    }
    private void handleViews(View rootView) {
        mListStations = (ListView)rootView.findViewById(R.id.list_stations);
        mListStations.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                if (mSelectionMode){
                    Station station = mStations.get(position);
                    if (mListener!=null){
                        mListener.selectStation(station.getId());
                    }
                }
            }
        });
        mTextRegion= (TextView)rootView.findViewById(R.id.text_region);
        mTextRegion.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (mListener!=null)
                    mListener.toRegionSelect( mSelectionMode? RegionsFragment.SelectionMode.STATION: RegionsFragment.SelectionMode.NONE,true);
            }
        });
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        try {
            mListener = (OnFragmentInteractionListener) activity;
        } catch (ClassCastException e) {
            throw new ClassCastException(activity.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putString(PARENT_ID,mParentId);
    }
    @Override
    public void onResume() {
        super.onResume();
        if (mListener!=null)
            mListener.setTitle(R.string.title_fragment_station);

    }

}
