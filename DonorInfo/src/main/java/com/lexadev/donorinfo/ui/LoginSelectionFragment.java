package com.lexadev.donorinfo.ui;

import android.app.Activity;
import android.graphics.Paint;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.lexadev.donorinfo.OnFragmentInteractionListener;
import com.lexadev.donorinfo.R;
import com.lexadev.donorinfo.helper.FontsHelper;

public class LoginSelectionFragment extends Fragment {

    private TextView mTextLogin;
    private TextView mTextRegistration;
    private OnFragmentInteractionListener mListener;

    public LoginSelectionFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_login_selection, container, false);
        if (mListener!=null)
            mListener.setupUI(rootView);
        handleViews(rootView);
        return rootView;
    }

    private void handleViews(View rootView) {
        mTextLogin = (TextView)rootView.findViewById(R.id.text_login);
        mTextLogin.setTypeface(FontsHelper.getFont(FontsHelper.FontName.ROBOTO, FontsHelper.FontsStyle.REGULAR));
        mTextLogin.setPaintFlags(mTextLogin.getPaintFlags() |   Paint.UNDERLINE_TEXT_FLAG);
        mTextLogin.setOnClickListener(mOnLoginClick);
        mTextRegistration = (TextView)rootView.findViewById(R.id.text_registration);
        mTextRegistration.setTypeface(FontsHelper.getFont(FontsHelper.FontName.ROBOTO, FontsHelper.FontsStyle.REGULAR));
        mTextRegistration.setPaintFlags(mTextLogin.getPaintFlags() |   Paint.UNDERLINE_TEXT_FLAG);
        mTextRegistration.setOnClickListener(mOnRegistrationClick);

    }
    private View.OnClickListener mOnLoginClick = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            if (mListener!=null)
                mListener.toLogin(false);
        }
    };
    private View.OnClickListener mOnRegistrationClick = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            if (mListener!=null)
                mListener.toRegistration(false);
        }
    };


    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        try {
            mListener = (OnFragmentInteractionListener) activity;
        } catch (ClassCastException e) {
            throw new ClassCastException(activity.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
        if (mListener!=null)
            mListener.setTitle(R.string.title_enter);
    }
}
