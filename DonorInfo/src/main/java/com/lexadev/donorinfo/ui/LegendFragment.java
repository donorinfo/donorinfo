package com.lexadev.donorinfo.ui;

import android.app.Activity;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.lexadev.donorinfo.OnFragmentInteractionListener;
import com.lexadev.donorinfo.R;
import com.lexadev.donorinfo.helper.FontsHelper;

public class LegendFragment extends Fragment {

    private OnFragmentInteractionListener mListener;

    public LegendFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_legend, container, false);
        handleViews(rootView);
        return rootView;
    }

    private void handleViews(View rootView) {
        TextView text = (TextView)rootView.findViewById(R.id.text_title_1);
        text.setTypeface(FontsHelper.getFont(FontsHelper.FontName.ROBOTO, FontsHelper.FontsStyle.BOLD));
        text = (TextView)rootView.findViewById(R.id.text_title_2);
        text.setTypeface(FontsHelper.getFont(FontsHelper.FontName.ROBOTO, FontsHelper.FontsStyle.BOLD));
        text = (TextView)rootView.findViewById(R.id.text_title_3);
        text.setTypeface(FontsHelper.getFont(FontsHelper.FontName.ROBOTO, FontsHelper.FontsStyle.BOLD));
        text = (TextView)rootView.findViewById(R.id.text_planned_red);
        text.setTypeface(FontsHelper.getFont(FontsHelper.FontName.ROBOTO, FontsHelper.FontsStyle.REGULAR));
        text = (TextView)rootView.findViewById(R.id.text_planned_yellow);
        text.setTypeface(FontsHelper.getFont(FontsHelper.FontName.ROBOTO, FontsHelper.FontsStyle.REGULAR));
        text = (TextView)rootView.findViewById(R.id.text_planned_green);
        text.setTypeface(FontsHelper.getFont(FontsHelper.FontName.ROBOTO, FontsHelper.FontsStyle.REGULAR));
        text = (TextView)rootView.findViewById(R.id.text_done_red);
        text.setTypeface(FontsHelper.getFont(FontsHelper.FontName.ROBOTO, FontsHelper.FontsStyle.REGULAR));
        text = (TextView)rootView.findViewById(R.id.text_done_yellow);
        text.setTypeface(FontsHelper.getFont(FontsHelper.FontName.ROBOTO, FontsHelper.FontsStyle.REGULAR));
        text = (TextView)rootView.findViewById(R.id.text_done_green);
        text.setTypeface(FontsHelper.getFont(FontsHelper.FontName.ROBOTO, FontsHelper.FontsStyle.REGULAR));
        text = (TextView)rootView.findViewById(R.id.text_denied);
        text.setTypeface(FontsHelper.getFont(FontsHelper.FontName.ROBOTO, FontsHelper.FontsStyle.REGULAR));
        text = (TextView)rootView.findViewById(R.id.text_analize);
        text.setTypeface(FontsHelper.getFont(FontsHelper.FontName.ROBOTO, FontsHelper.FontsStyle.REGULAR));
    }


    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        try {
            mListener = (OnFragmentInteractionListener) activity;
        } catch (ClassCastException e) {
            throw new ClassCastException(activity.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

}
