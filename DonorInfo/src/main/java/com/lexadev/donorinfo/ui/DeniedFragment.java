package com.lexadev.donorinfo.ui;

import android.app.Activity;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.lexadev.donorinfo.OnFragmentInteractionListener;
import com.lexadev.donorinfo.R;
import com.lexadev.donorinfo.helper.FontsHelper;
import com.lexadev.donorinfo.helper.InfoFragments;

public class DeniedFragment extends Fragment {

    private OnFragmentInteractionListener mListener;

    private int[] mTextsTitle = new int[]{
            R.id.info_forever,
            R.id.info_temp
    };
    private int[] mTextsDescription = new int[]{
            R.id.info_forever_desc,
            R.id.info_temp_desc
    };

    private LinearLayout mLayoutForever;
    private LinearLayout mLayoutTemp;
    public DeniedFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_denied, container, false);
        handleViews(rootView);
        return rootView;
    }

    private void handleViews(View rootView) {
        mLayoutForever = (LinearLayout)rootView.findViewById(R.id.layout_forever);
        mLayoutForever.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                gotoInfo(InfoFragments.DENIED_FOREVER);
            }
        });
        mLayoutTemp = (LinearLayout)rootView.findViewById(R.id.layout_temp);
        mLayoutTemp.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                gotoInfo(InfoFragments.DENIED_TEMPORARY);
            }
        });
        setTypefaces(rootView);
    }
    private void gotoInfo(InfoFragments fragment) {
        if (mListener!=null)
            mListener.toInfo(fragment);
    }

    private void setTypefaces(View rootView){
        for (int id:mTextsTitle){
            try {
                TextView view = (TextView) rootView.findViewById(id);
                view.setTypeface(FontsHelper.getFont(FontsHelper.FontName.ROBOTO, FontsHelper.FontsStyle.BOLD));
            }catch(ClassCastException e){
                e.printStackTrace();
            }
        }
        for (int id:mTextsDescription){
            try {
                TextView view = (TextView) rootView.findViewById(id);
                view.setTypeface(FontsHelper.getFont(FontsHelper.FontName.ROBOTO, FontsHelper.FontsStyle.REGULAR));
            }catch(ClassCastException e){
                e.printStackTrace();
            }
        }

    }
    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        try {
            mListener = (OnFragmentInteractionListener) activity;
        } catch (ClassCastException e) {
            throw new ClassCastException(activity.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    @Override
    public void onResume() {
        super.onResume();
        if (mListener!=null)
            mListener.setTitle(R.string.title_fragment_denied);
    }
}
