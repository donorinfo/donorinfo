package com.lexadev.donorinfo.ui;

import android.app.Activity;
import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.lexadev.donorinfo.AppConstants;
import com.lexadev.donorinfo.OnFragmentInteractionListener;
import com.lexadev.donorinfo.R;
import com.lexadev.donorinfo.helper.DbAdapter;
import com.lexadev.donorinfo.helper.FontsHelper;
import com.lexadev.donorinfo.helper.FragmentState;
import com.lexadev.donorinfo.helper.Utils;
import com.lexadev.donorinfo.model.ParseRequest;
import com.lexadev.donorinfo.model.RegionObject;
import com.lexadev.donorinfo.parse.AppUser;
import com.parse.DeleteCallback;
import com.parse.GetCallback;
import com.parse.ParseException;
import com.parse.ParsePush;
import com.parse.ParseQuery;
import com.parse.SaveCallback;

import java.util.ArrayList;
import java.util.List;

public class RequestFragment extends Fragment {
    private static final String ARG_REQUEST_ID="request_id";

    private String mRequestId;

    private ArrayList<TextView> mValueViews = new ArrayList<>();
    private ArrayList<View> mEditViews = new ArrayList<>();
    private int[] mTitles = new int[]{
            R.id.text_title_region,
            R.id.text_title_city,
            R.id.text_title_phone_number,
            R.id.text_title_name,
            R.id.text_title_hospital,
            R.id.text_title_room,
            R.id.text_title_blood,
            R.id.text_title_needed,
            R.id.text_title_comments
    };

    //View views
    private TextView mViewRegion;
    private TextView mViewCity;
    private TextView mViewPhone;
    private TextView mViewName;
    private TextView mViewHospital;
    private TextView mViewRoom;
    private TextView mViewBlood;
    private TextView mViewNeeded;
    private TextView mViewComments;

    //Edit views
    private Spinner mSpinnerRegion;
    private Spinner mSpinnerCity;
    private EditText mEditNumber;
    private EditText mEditName;
    private EditText mEditHospital;
    private EditText mEditRoom;
    private Spinner mSpinnerBlood;
    private Spinner mSpinnerNeeded;
    private EditText mEditComments;
    private CheckBox mCheckHideNumber;

    //Buttons
    private Button mButtonPlace;
    private Button mButtonApprove;
    private Button mButtonDelete;

    private LinearLayout mLayoutPhone;

    private FragmentState mState = FragmentState.VIEW;

    private OnFragmentInteractionListener mListener;

    private ParseRequest mRequest = null;
    private AppUser mUser = null;
    private boolean mIsAdmin = false;

    private List<RegionObject> mRegions;
    private List<RegionObject> mCities;

    public static RequestFragment newInstance(String request_id){
        RequestFragment fragment = new RequestFragment();
        if (request_id!=null){
            Bundle bundle = new Bundle();
            bundle.putString(ARG_REQUEST_ID,request_id);
            fragment.setArguments(bundle);
        }
        return fragment;
    }
    public RequestFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mUser = AppUser.getCurrentUser();
        if (mUser!=null) {
            mIsAdmin = mUser.isAdmin();
            if (mListener!=null)
                mIsAdmin=mIsAdmin&&mListener.isUseAdmin();
        }
        if (getArguments()!=null){
            mRequestId = getArguments().getString(ARG_REQUEST_ID);
        }
        if (mRequestId==null)
            mState = FragmentState.EDIT;
        else
            mState = FragmentState.VIEW;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View rootView =inflater.inflate(R.layout.fragment_request, container, false);
        if (mListener!=null)
            mListener.setupUI(rootView);
        handleViews(rootView);
        showHideViews();
        if (mRequestId!=null){
            loadRequest();
        }
        return rootView;
    }

    private void loadRequest() {
        ParseQuery<ParseRequest> query = ParseRequest.getRequest();
        if (mListener!=null)
            mListener.startWaiting(R.string.progress_loading,null);
        query.getInBackground(mRequestId,new GetCallback<ParseRequest>() {
                    @Override
                    public void done(ParseRequest parseRequest, ParseException e) {
                        if (mListener!=null)
                            mListener.stopWaiting();
                        if (e==null){
                            if (parseRequest!=null){
                                mRequest = parseRequest;
                                if (mRequest!=null) {
                                    if (mIsAdmin) {
                                        mState = FragmentState.EDIT;
                                    } else {
                                        mState = FragmentState.VIEW;
                                    }
                                    fillViews();
                                }
                                else{
                                    //TODO
                                }

                            }
                        }
                        else{

                            //TODO
                        }
                    }
                }
        );
    }

    private void fillViews() {
        if (mRequest!=null){
            String region = mRequest.getRegion();
            if (region!=null) {
                for (int i=0;i<mRegions.size();i++){
                    if (mRegions.get(i).id.equals(region)){
                        mSpinnerRegion.setSelection(i);
                        mViewRegion.setText(mRegions.get(i).name);
                    }
                }
                updateCities(region);
            }
            mEditNumber.setText(Utils.notNull(mRequest.getPhone()));
            if (mIsAdmin || mRequest.getShowNumber())
                mViewPhone.setText(Utils.notNull(mRequest.getPhone()));
            else{
                mLayoutPhone.setVisibility(View.GONE);
            }

            mEditName.setText(Utils.notNull(mRequest.getName()));
            mViewName.setText(Utils.notNull(mRequest.getName()));

            mEditHospital.setText(Utils.notNull(mRequest.getHospital()));
            mViewHospital.setText(Utils.notNull(mRequest.getHospital()));

            mEditRoom.setText(Utils.notNull(mRequest.getRoom()));
            mViewRoom.setText(Utils.notNull(mRequest.getRoom()));

            String blood = mRequest.getBlood();
            if (blood!=null){
                for (int i=0;i<mSpinnerBlood.getCount();i++){
                    if (mSpinnerBlood.getItemAtPosition(i).equals(blood)){
                        mSpinnerBlood.setSelection(i);
                        break;
                    }
                }
            }
            mViewBlood.setText(Utils.notNull(blood));

            String needed = mRequest.getNeeded();
            if (needed!=null){
                for (int i=0;i<mSpinnerNeeded.getCount();i++){
                    if (mSpinnerNeeded.getItemAtPosition(i).equals(needed)){
                        mSpinnerNeeded.setSelection(i);
                        break;
                    }
                }
            }
            mViewNeeded.setText(Utils.notNull(needed));

            mEditComments.setText(Utils.notNull(mRequest.getNote()));
            mViewComments.setText(Utils.notNull(mRequest.getNote()));

        }
        showHideViews();
    }

    private void handleViews(View rootView) {
        //Handle view views
        mLayoutPhone = (LinearLayout)rootView.findViewById(R.id.layout_phone);

        mViewRegion = (TextView)rootView.findViewById(R.id.text_region);
        mViewCity = (TextView)rootView.findViewById(R.id.text_city);
        mViewPhone = (TextView)rootView.findViewById(R.id.text_phone_number);
        mViewName = (TextView)rootView.findViewById(R.id.text_name);
        mViewHospital = (TextView)rootView.findViewById(R.id.text_hospital);
        mViewRoom = (TextView)rootView.findViewById(R.id.text_hospital_room);
        mViewBlood = (TextView)rootView.findViewById(R.id.text_blood);
        mViewNeeded = (TextView)rootView.findViewById(R.id.text_needed);
        mViewComments = (TextView)rootView.findViewById(R.id.text_comments);
        mValueViews.add(mViewRegion);
        mValueViews.add(mViewCity);
        mValueViews.add(mViewPhone);
        mValueViews.add(mViewName);
        mValueViews.add(mViewHospital);
        mValueViews.add(mViewRoom);
        mValueViews.add(mViewBlood);
        mValueViews.add(mViewNeeded);
        mValueViews.add(mViewComments);

        //Handle edit views
        mSpinnerRegion = (Spinner)rootView.findViewById(R.id.spinner_region);
        mSpinnerCity = (Spinner)rootView.findViewById(R.id.spinner_city);
        mSpinnerRegion.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                RegionObject object = (RegionObject)mSpinnerRegion.getItemAtPosition(position);
                updateCities(object.id);
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
        mRegions = DbAdapter.getInstance(getActivity()).getRegions();
        RegionSpinnerAdapter adapter = new RegionSpinnerAdapter(getActivity(),mRegions);
        mSpinnerRegion.setAdapter(adapter);

        mEditNumber = (EditText)rootView.findViewById(R.id.edit_phone_number);
        mEditNumber.setTypeface(FontsHelper.getFont(FontsHelper.FontName.ROBOTO, FontsHelper.FontsStyle.REGULAR));
        mEditName = (EditText)rootView.findViewById(R.id.edit_name);
        mEditName.setTypeface(FontsHelper.getFont(FontsHelper.FontName.ROBOTO, FontsHelper.FontsStyle.REGULAR));
        mEditHospital = (EditText)rootView.findViewById(R.id.edit_hospital);
        mEditHospital.setTypeface(FontsHelper.getFont(FontsHelper.FontName.ROBOTO, FontsHelper.FontsStyle.REGULAR));
        mEditRoom = (EditText)rootView.findViewById(R.id.edit_hospital_room);
        mEditRoom.setTypeface(FontsHelper.getFont(FontsHelper.FontName.ROBOTO, FontsHelper.FontsStyle.REGULAR));
        mSpinnerBlood = (Spinner)rootView.findViewById(R.id.spinner_blood);
        CustomSpinnerAdapter adapterBlood = new CustomSpinnerAdapter(getActivity(),R.layout.simple_spinner_item,getResources().getStringArray(R.array.array_blood));
        adapterBlood.setDropDownViewResource(R.layout.simple_spinner_dropdown_item);
        mSpinnerBlood.setAdapter(adapterBlood);

        mSpinnerNeeded = (Spinner)rootView.findViewById(R.id.spinner_needed);
        CustomSpinnerAdapter adapterNeeded = new CustomSpinnerAdapter(getActivity(),R.layout.simple_spinner_item,getResources().getStringArray(R.array.array_needed));
        adapterNeeded.setDropDownViewResource(R.layout.simple_spinner_dropdown_item);
        mSpinnerNeeded.setAdapter(adapterNeeded);

        mEditComments = (EditText)rootView.findViewById(R.id.edit_comments);
        mEditComments.setTypeface(FontsHelper.getFont(FontsHelper.FontName.ROBOTO, FontsHelper.FontsStyle.REGULAR));
        mCheckHideNumber = (CheckBox)rootView.findViewById(R.id.check_hide_number);
        mCheckHideNumber.setTypeface(FontsHelper.getFont(FontsHelper.FontName.ROBOTO, FontsHelper.FontsStyle.REGULAR));
        mEditViews.add(mSpinnerRegion);
        mEditViews.add(mSpinnerCity);
        mEditViews.add(mEditNumber);
        mEditViews.add(mEditName);
        mEditViews.add(mEditHospital);
        mEditViews.add(mEditRoom);
        mEditViews.add(mSpinnerBlood);
        mEditViews.add(mSpinnerNeeded);
        mEditViews.add(mEditComments);
        mEditViews.add(mCheckHideNumber);

        //handle buttons
        mButtonPlace = (Button)rootView.findViewById(R.id.button_place);
        mButtonPlace.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                place();
            }
        });
        mButtonPlace.setTypeface(FontsHelper.getFont(FontsHelper.FontName.ROBOTO, FontsHelper.FontsStyle.REGULAR));

        mButtonApprove = (Button)rootView.findViewById(R.id.button_approve);
        mButtonApprove.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                approve();
            }
        });
        mButtonApprove.setTypeface(FontsHelper.getFont(FontsHelper.FontName.ROBOTO, FontsHelper.FontsStyle.REGULAR));

        mButtonDelete = (Button)rootView.findViewById(R.id.button_delete);
        mButtonDelete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                delete();
            }
        });
        mButtonDelete.setTypeface(FontsHelper.getFont(FontsHelper.FontName.ROBOTO, FontsHelper.FontsStyle.REGULAR));


        for (int i=0;i<mTitles.length;i++){
            TextView text = (TextView)rootView.findViewById(mTitles[i]);
            if (text!=null)
                text.setTypeface(FontsHelper.getFont(FontsHelper.FontName.ROBOTO, FontsHelper.FontsStyle.MEDIUM));
        }
        for (TextView view:mValueViews) {
            view.setTypeface(FontsHelper.getFont(FontsHelper.FontName.ROBOTO, FontsHelper.FontsStyle.REGULAR));
        }
    }

    private void updateCities(String regionId) {
        mCities = DbAdapter.getInstance(getActivity()).getCities(regionId);
        RegionSpinnerAdapter adapter = new RegionSpinnerAdapter(getActivity(),mCities);
        mSpinnerCity.setAdapter(adapter);
        if (mRequest!=null && mRequest.getCity()!=null){
            for (int i=0;i<mCities.size();i++){
                if (mCities.get(i).id.equals(mRequest.getCity())){
                    mSpinnerCity.setSelection(i);
                    mViewCity.setText(mCities.get(i).name);
                    break;
                }
            }
        }

    }

    private void delete() {
        if (mRequest!=null){
            if (mListener!=null)
                mListener.startWaiting(R.string.progress_deleting,null);
            mRequest.deleteInBackground(new DeleteCallback() {
                @Override
                public void done(ParseException e) {
                    if (mListener!=null)
                        mListener.stopWaiting();
                    if (e==null){
                        if (mListener!=null)
                            mListener.back();
                    }
                    else{
                        //TODO
                    }
                }
            });
        }
    }

    private void approve() {
        fillRequestObject();
        mRequest.setStatus(ParseRequest.Status.APPROVED);
        saveRequest();
    }

    private void saveRequest() {
        if (checkFiled()) {
            if (mRequest != null) {
                if (mListener != null)
                    mListener.startWaiting(R.string.progress_saving, null);

                mRequest.saveInBackground(new SaveCallback() {
                    @Override
                    public void done(ParseException e) {
                        if (mListener != null)
                            mListener.stopWaiting();
                        if (e == null) {
                            sendPushToAdmins();
                            if (getActivity()!=null)
                                Toast.makeText(getActivity(),R.string.toast_request,Toast.LENGTH_LONG).show();

                            if (mListener != null)
                                mListener.back();
                        } else {
                            //TODO
                        }
                    }
                });
            }
        }
    }

    private void sendPushToAdmins() {
        ParsePush push = new ParsePush();
        push.setChannel(AppConstants.REQUESTS_CHANNEL);
        push.setMessage(getString(R.string.push_new_request));
        push.sendInBackground();
    }

    private boolean checkFiled() {
        boolean ret = true;
        if (!mIsAdmin){
            if (mEditName.getText().toString().length()==0){
                mEditName.setError(getString(R.string.error_you_must_set_this_field));
                ret = false;
            }
            if (mEditNumber.getText().toString().length()==0) {
                mEditNumber.setError(getString(R.string.error_you_must_set_this_field));
                ret = false;
            }
            if (mEditHospital.getText().toString().length()==0) {
                mEditHospital.setError(getString(R.string.error_you_must_set_this_field));
                ret = false;
            }
        }
        else
            return true;
        return ret;
    }

    private void place() {
        fillRequestObject();
        mRequest.setStatus(ParseRequest.Status.NEW);
        saveRequest();
    }

    private void fillRequestObject() {
        if (mRequest==null){
            mRequest = new ParseRequest();
            mRequest.setUser(mUser);
        }
        RegionObject region = (RegionObject)mSpinnerRegion.getSelectedItem();
        mRequest.setRegion(region.id);
        RegionObject city = (RegionObject)mSpinnerCity.getSelectedItem();
        mRequest.setCity(city.id);
        mRequest.setName(mEditName.getText().toString());
        mRequest.setPhone(mEditNumber.getText().toString());
        mRequest.setHospital(mEditHospital.getText().toString());
        mRequest.setRoom(mEditRoom.getText().toString());
        mRequest.setBlood((String)mSpinnerBlood.getSelectedItem());
        mRequest.setNeeded((String) mSpinnerNeeded.getSelectedItem());
        mRequest.setNote(mEditComments.getText().toString());
        mRequest.setShowNumber(!mCheckHideNumber.isChecked());
    }

    private void showHideViews() {
        if (mState==FragmentState.VIEW){
            for (View view:mEditViews){
                view.setVisibility(View.GONE);
            }
            for (View view:mValueViews){
                view.setVisibility(View.VISIBLE);
            }
            mButtonApprove.setVisibility(View.GONE);
            mButtonDelete.setVisibility(View.GONE);
            mButtonPlace.setVisibility(View.GONE);
        }
        else{
            for (View view:mEditViews){
                view.setVisibility(View.VISIBLE);
            }
            for (View view:mValueViews){
                view.setVisibility(View.GONE);
            }
            if (mIsAdmin){
                if (mRequest == null){
                    mButtonApprove.setVisibility(View.VISIBLE);
                    mButtonDelete.setVisibility(View.GONE);
                    mButtonPlace.setVisibility(View.GONE);
                }
                else{
                    mButtonApprove.setVisibility(View.VISIBLE);
                    mButtonDelete.setVisibility(View.VISIBLE);
                    mButtonPlace.setVisibility(View.GONE);
                }
            }
            else{
                mButtonApprove.setVisibility(View.GONE);
                mButtonDelete.setVisibility(View.GONE);
                mButtonPlace.setVisibility(View.VISIBLE);
            }
        }
    }

    private class CustomSpinnerAdapter extends ArrayAdapter<String> {

        public CustomSpinnerAdapter(Context context, int resource, String[] objects) {
            super(context, resource, objects);
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {
            View rootView = super.getView(position, convertView, parent);
            TextView text = (TextView)rootView.findViewById(android.R.id.text1);
            if (text!=null){
                text.setTypeface(FontsHelper.getFont(FontsHelper.FontName.ROBOTO, FontsHelper.FontsStyle.REGULAR));
            }
            return rootView;
        }
    }

    private class RegionSpinnerAdapter extends ArrayAdapter<RegionObject> {

        private LayoutInflater mInflater;
        public RegionSpinnerAdapter(Context context, List<RegionObject> objects) {
            super(context, 0, objects);
            mInflater = LayoutInflater.from(context);
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {
            ViewsHolder holder;
            if (convertView==null){
                convertView = mInflater.inflate(R.layout.simple_spinner_item,parent,false);
                holder = new ViewsHolder(convertView);
            }
            else{
                holder = (ViewsHolder)convertView.getTag();
            }
            holder.build(position);
            return convertView;
        }

        @Override
        public View getDropDownView(int position, View convertView, ViewGroup parent) {
            ViewsHolder holder;
            if (convertView==null){
                convertView = mInflater.inflate(R.layout.simple_spinner_dropdown_item,parent,false);
                holder = new ViewsHolder(convertView);
            }
            else{
                holder = (ViewsHolder)convertView.getTag();
            }
            holder.build(position);
            return convertView;

        }

        private class ViewsHolder{
            TextView text;
            public ViewsHolder(View convertView){
                text = (TextView)convertView.findViewById(android.R.id.text1);
                text.setTypeface(FontsHelper.getFont(FontsHelper.FontName.ROBOTO, FontsHelper.FontsStyle.REGULAR));
                convertView.setTag(this);
            }
            public void build(int position){
                RegionObject item = getItem(position);
                text.setText(Utils.notNull(item.name));
            }
        }
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        try {
            mListener = (OnFragmentInteractionListener) activity;
        } catch (ClassCastException e) {
            throw new ClassCastException(activity.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    @Override
    public void onResume() {
        super.onResume();
        if (mListener!=null)
            mListener.setTitle(R.string.title_fragment_request);

    }
}
