package com.lexadev.donorinfo.ui;

import android.app.Activity;
import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;

import com.lexadev.donorinfo.OnFragmentInteractionListener;
import com.lexadev.donorinfo.OnPossibleEventUpdates;
import com.lexadev.donorinfo.R;
import com.lexadev.donorinfo.model.ParseEvent;
import com.lexadev.donorinfo.parse.AppUser;
import com.parse.CountCallback;
import com.parse.FindCallback;
import com.parse.GetCallback;
import com.parse.ParseException;
import com.parse.ParseQuery;

import java.text.DateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.List;

public class ListEventsFragment extends Fragment {
    private static final String LIST = "list";

    private OnFragmentInteractionListener mListener;
    private OnPossibleEventUpdates mPListener;

    private List<Year> mExistsYears = null;
    private Year mSelectedYear = null;
    private List<ListItem> mItems;
    private ListView mListView;
    private ListItem mClickedItem = null;
    private ParseEvent mBloodEvent = null;
    private ParseEvent mPlazmaEvent = null;
    private ParseEvent mTromboEvent = null;

    private boolean mEventQueried = false;
    private boolean mBloodQueried = false;
    private boolean mPlazmaQueried = false;
    private boolean mTromboQueried = false;

    private class Year {
        int year;
        int count;

        public Year(int year, int count) {
            this.year = year;
            this.count = count;
        }
    }

    private class ListItem {
        public Year year;
        public ParseEvent event;

        public ListItem(Year year) {
            this.year = year;
            this.event = null;
        }

        public ListItem(ParseEvent event) {
            this.event = event;
            this.year = null;
        }
    }

    public ListEventsFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_list_events, container, false);
        handldeViews(rootView);
        if (savedInstanceState != null)
            if (savedInstanceState.containsKey(LIST))
                mListView.onRestoreInstanceState(savedInstanceState.getParcelable(LIST));

        if (mExistsYears == null) {
            loadExistsYears();
        } else {
            if (mClickedItem != null) {
                checkClickedItem();
            } else {
                fillAdapter();
            }
        }
        return rootView;
    }

    private void fillAdapter() {
        if (!mEventQueried || !mBloodQueried || !mPlazmaQueried || !mTromboQueried)
            return;
        if (mListener != null)
            mListener.stopWaiting();

        if (mBloodEvent != null)
            fillItemsWith(mBloodEvent);
        if (mTromboEvent != null)
            fillItemsWith(mTromboEvent);
        if (mPlazmaEvent != null)
            fillItemsWith(mPlazmaEvent);

        EventAdapter adapter = new EventAdapter(getActivity(), mItems);
        mListView.setAdapter(adapter);
        mListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                ListItem item = mItems.get(position);
                if (item.year == null) {
                    clickEvent(item.event);
                } else {
                    clickYear(item.year);
                }
            }
        });
    }

    private void fillItemsWith(ParseEvent parseEvent) {
        if (parseEvent.getYear() != mSelectedYear.year)
            return;
        int eventYear = parseEvent.getYear();
        Date eventDate = parseEvent.getDate();
        for (int i = 0; i < mItems.size(); i++) {
            ListItem item = mItems.get(i);
            if (item.year != null && item.year.year < eventYear ||
                    item.year == null && item.event.getDate().before(eventDate)) {
                if (!checkManualDayEvents(i, parseEvent))
                    mItems.add(i, new ListItem(parseEvent));
                return;
            }
        }
        if (!checkManualDayEvents(mItems.size(), parseEvent))
            mItems.add(new ListItem(parseEvent));
    }

    private boolean checkManualDayEvents(int position, ParseEvent event) {
        Calendar calendarBegin = GregorianCalendar.getInstance();
        calendarBegin.setTime(event.getDate());
        calendarBegin.set(Calendar.HOUR_OF_DAY, 0);
        calendarBegin.set(Calendar.MINUTE, 0);
        calendarBegin.set(Calendar.SECOND, 0);
        calendarBegin.set(Calendar.MILLISECOND, 0);
        Calendar calendarEnd = GregorianCalendar.getInstance();
        calendarEnd.setTime(event.getDate());
        calendarEnd.set(Calendar.HOUR_OF_DAY, calendarEnd.getActualMaximum(Calendar.HOUR_OF_DAY));
        calendarEnd.set(Calendar.MINUTE, calendarEnd.getActualMaximum(Calendar.MINUTE));
        calendarEnd.set(Calendar.SECOND, calendarEnd.getActualMaximum(Calendar.SECOND));
        calendarEnd.set(Calendar.MILLISECOND, calendarEnd.getActualMaximum(Calendar.MILLISECOND));
        int eventType = event.getEventType();
        for (int i = position; i < mItems.size(); i++) {
            ListItem item = mItems.get(i);
            if (item.year != null)
                break;
            if (item.event.getDate().before(calendarBegin.getTime()))
                break;
            if (item.event.getEvent() == ParseEvent.BLOOD_EVENT && item.event.getEventType() == eventType)
                return true;
        }
        for (int i = position - 1; i >= 0; i--) {
            ListItem item = mItems.get(i);
            if (item.year != null)
                break;
            if (item.event.getDate().after(calendarEnd.getTime()))
                break;
            if (item.event.getEvent() == ParseEvent.BLOOD_EVENT && item.event.getEventType() == eventType)
                return true;
        }
        return false;
    }

    private void clickYear(Year year) {
        mSelectedYear = year;
        loadItemsForTheYear();
    }

    private void clickEvent(ParseEvent event) {
        //TODO
    }

    private void checkClickedItem() {
        //TODO
    }

    private class EventAdapter extends ArrayAdapter<ListItem> {
        private LayoutInflater mInflater;
        private int mColorRed;
        private int mColorBlue;

        public EventAdapter(Context context, List<ListItem> objects) {
            super(context, 0, objects);
            mInflater = LayoutInflater.from(context);
            mColorRed = context.getResources().getColor(R.color.base);
            mColorBlue = context.getResources().getColor(R.color.blue);
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {
            ViewsHolder holder;
            if (convertView == null) {
                convertView = mInflater.inflate(R.layout.event_list_item, parent, false);
                holder = new ViewsHolder(convertView);
            } else {
                holder = (ViewsHolder) convertView.getTag();
            }
            holder.build(position);
            return convertView;
        }

        private class ViewsHolder {
            TextView yearView;
            LinearLayout eventLayout;
            TextView eventTitle;
            TextView eventDate;
            TextView eventDesc;
            ImageView iconView;

            public ViewsHolder(View rootView) {
                yearView = (TextView) rootView.findViewById(R.id.title_year);
                eventLayout = (LinearLayout) rootView.findViewById(R.id.layout_event);
                eventTitle = (TextView) rootView.findViewById(R.id.title_event);
                eventDate = (TextView) rootView.findViewById(R.id.date);
                eventDesc = (TextView) rootView.findViewById(R.id.event_desc);
                iconView = (ImageView) rootView.findViewById(R.id.icon);
                rootView.setTag(this);
            }

            public void build(int position) {
                ListItem item = getItem(position);
                if (item.year == null) {
                    ParseEvent event = item.event;
                    eventLayout.setVisibility(View.VISIBLE);
                    yearView.setVisibility(View.GONE);
                    Date date = event.getDate();
                    DateFormat format = DateFormat.getInstance();
                    eventDate.setText(format.format(date));
                    int idx = event.getEvent();
                    int type = event.getEventType();
                    int icon = 0;
                    int strTitle = 0;
                    int colorTitle = 0;
                    int strDesc = 0;

                    if (event.getStatus() == ParseEvent.STATUS_DONE) {
                        strTitle = R.string.list_done;
                        colorTitle = mColorBlue;
                        if (idx == ParseEvent.BLOOD_EVENT) {
                            if (type == ParseEvent.BLOOD_EVENT_BLOOD) {
                                strDesc = R.string.list_blood;
                                icon = R.drawable.ic_red_blood;
                            } else if (type == ParseEvent.BLOOD_EVENT_PLAZMA) {
                                strDesc = R.string.list_plazma;
                                icon = R.drawable.ic_yellow_blood;
                            } else if (type == ParseEvent.BLOOD_EVENT_TROMBO) {
                                strDesc = R.string.list_trombo;
                                icon = R.drawable.ic_green_blood;
                            }

                        } else if (idx == ParseEvent.DENIED_EVENT) {
                            strDesc = R.string.list_denied;
                            icon = R.drawable.ic_denied;
                        } else if (idx == ParseEvent.ANALIZE_EVENT) {
                            strDesc = R.string.list_analize;
                            icon = R.drawable.ic_analize;
                        }

                    } else {
                        if (event.getManualSave()) {
                            strTitle = R.string.list_planned;
                            colorTitle = mColorRed;
                            if (idx == ParseEvent.BLOOD_EVENT) {
                                if (type == ParseEvent.BLOOD_EVENT_BLOOD) {
                                    strDesc = R.string.list_blood;
                                    icon = R.drawable.ic_red_blood;
                                } else if (type == ParseEvent.BLOOD_EVENT_PLAZMA) {
                                    strDesc = R.string.list_plazma;
                                    icon = R.drawable.ic_yellow_blood;
                                } else if (type == ParseEvent.BLOOD_EVENT_TROMBO) {
                                    strDesc = R.string.list_trombo;
                                    icon = R.drawable.ic_green_blood;
                                }
                            } else if (idx == ParseEvent.DENIED_EVENT) {
                                strDesc = R.string.list_denied;
                                icon = R.drawable.ic_denied;
                            } else if (idx == ParseEvent.ANALIZE_EVENT) {
                                strDesc = R.string.list_analize;
                                icon = R.drawable.ic_analize;
                            }
                        } else {
                            strTitle = R.string.list_possible;
                            colorTitle = mColorRed;
                            if (type == ParseEvent.BLOOD_EVENT_BLOOD) {
                                strDesc = R.string.list_blood;
                                icon = R.drawable.ic_possible_blood;
                            } else if (type == ParseEvent.BLOOD_EVENT_PLAZMA) {
                                strDesc = R.string.list_plazma;
                                icon = R.drawable.ic_possible_plazma;
                            } else if (type == ParseEvent.BLOOD_EVENT_TROMBO) {
                                strDesc = R.string.list_trombo;
                                icon = R.drawable.ic_possible_trombo;
                            }

                        }
                    }
                    if (strTitle != 0)
                        eventTitle.setText(strTitle);
                    else
                        eventTitle.setText("");
                    if (strDesc != 0)
                        eventDesc.setText(strDesc);
                    else
                        eventDesc.setText("");
                    if (colorTitle != 0)
                        eventTitle.setTextColor(colorTitle);
                    if (icon != 0)
                        iconView.setImageResource(icon);
                } else {
                    eventLayout.setVisibility(View.GONE);
                    yearView.setVisibility(View.VISIBLE);
                    yearView.setText(Integer.toString(item.year.year));
                }
            }
        }

    }

    private void loadExistsYears() {
        if (AppUser.getCurrentUser() != null) {
            final List<Integer> years = AppUser.getCurrentUser().getYears();
            if (years != null && years.size() > 0) {
                mListener.startWaiting(R.string.progress_loading, null);
                mExistsYears = new ArrayList<>();
                for (Integer year : years) {
                    final int currentYear = year;
                    ParseQuery<ParseEvent> query = ParseEvent.getEventsYear(currentYear, 0);
                    query.countInBackground(new CountCallback() {
                        @Override
                        public void done(int i, ParseException e) {
                            mExistsYears.add(new Year(currentYear, i));
                            if (mExistsYears.size() == years.size()) {
                                yearsLoaded();
                            }
                        }
                    });
                }
            }
        }
    }

    private void yearsLoaded() {
        ArrayList<Year> exists = new ArrayList<>();
        for (Year year : mExistsYears) {
            if (year.count > 0) {
                boolean added = false;
                for (int i = 0; i < exists.size(); i++) {
                    if (exists.get(i).year < year.year) {
                        exists.add(i, year);
                        added = true;
                        break;
                    }
                }
                if (!added)
                    exists.add(year);
            }
        }
        mExistsYears = exists;
        Calendar calendar = GregorianCalendar.getInstance();
        int currentYear = calendar.get(Calendar.YEAR);
        for (int i = 0; i < mExistsYears.size(); i++) {
            if (mExistsYears.get(i).year == currentYear) {
                mSelectedYear = mExistsYears.get(i);
                break;
            }
        }
        if (mExistsYears.size() > 0) {
            if (mSelectedYear == null) {
                mSelectedYear = mExistsYears.get(0);
            }
            loadItemsForTheYear();
        } else if (mListener != null)
            mListener.stopWaiting();

    }

    private void loadItemsForTheYear() {
        if (mListener != null)
            mListener.startWaiting(R.string.progress_loading, null);
        mEventQueried = false;
        mBloodQueried = false;
        mPlazmaQueried = false;
        mTromboQueried = false;

        mItems = new ArrayList<>();
        for (int i = 0; i < mExistsYears.size(); i++) {
            mItems.add(new ListItem(mExistsYears.get(i)));
        }
        startLoadEvents();
        if (mPListener != null)
            mBloodEvent = mPListener.getEvent(ParseEvent.BLOOD_EVENT_BLOOD);
        if (mBloodEvent == null) {
            ParseQuery<ParseEvent> queryBlood = ParseEvent.getLastBloodEvent(ParseEvent.BLOOD_EVENT_BLOOD);
            if (queryBlood != null) {
                queryBlood.getFirstInBackground(new GetCallback<ParseEvent>() {
                    @Override
                    public void done(ParseEvent parseEvent, ParseException e) {
                        if (e == null) {
                            mBloodEvent = parseEvent;
                            if (mPListener!=null&&mBloodEvent!=null)
                                mPListener.updateEvent(ParseEvent.BLOOD_EVENT_BLOOD,mBloodEvent);
                        } else {
                            if (e.getCode()!= ParseException.OBJECT_NOT_FOUND)
                                if (mListener != null)
                                    mListener.showToast(R.string.toast_loading_error, e.getMessage());
                        }
                        mBloodQueried = true;
                        fillAdapter();
                    }
                });
            } else {
                mBloodQueried = true;
                fillAdapter();
            }
        } else {
            mBloodQueried = true;
            fillAdapter();
        }
        if (mPListener != null)
            mPlazmaEvent = mPListener.getEvent(ParseEvent.BLOOD_EVENT_PLAZMA);
        if (mPlazmaEvent == null) {

            ParseQuery<ParseEvent> queryPlazma = ParseEvent.getLastBloodEvent(ParseEvent.BLOOD_EVENT_PLAZMA);
            if (queryPlazma != null) {
                queryPlazma.getFirstInBackground(new GetCallback<ParseEvent>() {
                    @Override
                    public void done(ParseEvent parseEvent, ParseException e) {
                        if (e == null) {
                            mPlazmaEvent = parseEvent;
                            if (mPListener!=null&&mPlazmaEvent!=null)
                                mPListener.updateEvent(ParseEvent.BLOOD_EVENT_PLAZMA,mPlazmaEvent);
                        } else {
                            if (e.getCode()!= ParseException.OBJECT_NOT_FOUND)
                                if (mListener != null)
                                    mListener.showToast(R.string.toast_loading_error, e.getMessage());
                        }
                        mPlazmaQueried = true;
                        fillAdapter();
                    }
                });
            } else {
                mPlazmaQueried = true;
                fillAdapter();
            }
        } else {
            mPlazmaQueried = true;
            fillAdapter();
        }

        if (mPListener != null)
            mTromboEvent = mPListener.getEvent(ParseEvent.BLOOD_EVENT_TROMBO);
        if (mTromboEvent == null) {

            ParseQuery<ParseEvent> queryTrombo = ParseEvent.getLastBloodEvent(ParseEvent.BLOOD_EVENT_TROMBO);
            if (queryTrombo != null) {
                queryTrombo.getFirstInBackground(new GetCallback<ParseEvent>() {
                    @Override
                    public void done(ParseEvent parseEvent, ParseException e) {
                        if (e == null) {
                            mTromboEvent = parseEvent;
                            if (mPListener!=null&&mTromboEvent!=null)
                                mPListener.updateEvent(ParseEvent.BLOOD_EVENT_TROMBO,mTromboEvent);
                        } else {
                            if (e.getCode()!= ParseException.OBJECT_NOT_FOUND)
                                if (mListener != null)
                                    mListener.showToast(R.string.toast_loading_error, e.getMessage());
                        }
                        mTromboQueried = true;
                        fillAdapter();
                    }
                });
            } else {
                mTromboQueried = true;
                fillAdapter();
            }
        } else {
            mTromboQueried = true;
            fillAdapter();
        }

    }

    private void startLoadEvents() {
        mOffset = 0;
        loadEvents();
    }

    private int mOffset = 0;

    private void loadEvents() {
        ParseQuery<ParseEvent> query = ParseEvent.getEventsYear(mSelectedYear.year, mOffset);
        query.findInBackground(mLoadedItems);
    }

    private FindCallback<ParseEvent> mLoadedItems = new FindCallback<ParseEvent>() {
        @Override
        public void done(List<ParseEvent> parseEvents, ParseException e) {
            if (e == null && parseEvents != null && parseEvents.size() > 0) {

                mOffset += parseEvents.size();
                fillItemsWith(parseEvents);
                loadEvents();
            } else {
                mEventQueried = true;
                fillAdapter();
            }

        }
    };

    private void fillItemsWith(List<ParseEvent> parseEvents) {
        for (int i = mItems.size() - 1; i >= 0; i--) {
            ListItem item = mItems.get(i);
            if (item.year == mSelectedYear || item.year == null) {
                fillFrom(i + 1, parseEvents);
                return;
            }
        }
        fillFrom(0, parseEvents);
    }

    private void fillFrom(int start, List<ParseEvent> parseEvents) {
        for (int i = 0; i < parseEvents.size(); i++) {
            mItems.add(i + start, new ListItem(parseEvents.get(i)));
        }
    }

    private void handldeViews(View rootView) {
        mListView = (ListView) rootView.findViewById(R.id.list_events);
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        try {
            mListener = (OnFragmentInteractionListener) activity;
        } catch (ClassCastException e) {
            throw new ClassCastException(activity.toString()
                    + " must implement OnFragmentInteractionListener");
        }
        try {
            mPListener = (OnPossibleEventUpdates) activity;
        } catch (ClassCastException e) {
            throw new ClassCastException(activity.toString()
                    + " must implement OnPossibleEventUpdates");
        }

    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
        mPListener = null;
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putParcelable(LIST, mListView.onSaveInstanceState());
    }

}
