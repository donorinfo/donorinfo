package com.lexadev.donorinfo.ui;

import android.app.Activity;
import android.content.res.Resources;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.lexadev.donorinfo.OnFragmentInteractionListener;
import com.lexadev.donorinfo.R;
import com.lexadev.donorinfo.helper.Contras;
import com.lexadev.donorinfo.helper.FontsHelper;

import java.util.List;

public class ForeverDeniedFragment extends Fragment {

    private OnFragmentInteractionListener mListener;
    private LinearLayout mLayoutMain;
    public ForeverDeniedFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_forever_denied, container, false);
        mLayoutMain = (LinearLayout)rootView.findViewById(R.id.layout_main);
        showItems(inflater);
        return rootView;
    }

    private void showItems(LayoutInflater inflater) {
        Resources res = getResources();
        int darkDividerColor = res.getColor(R.color.dark_gray);
        int lightDividerColor = res.getColor(R.color.profile_divider);
        View title = inflater.inflate(R.layout.denied_title,mLayoutMain,false);
        TextView view = (TextView)title.findViewById(R.id.text);
        view.setText(R.string.text_info_forever);
        view.setTypeface(FontsHelper.getFont(FontsHelper.FontName.ROBOTO, FontsHelper.FontsStyle.BOLD));
        mLayoutMain.addView(title);
        List<Contras.Category> contras = Contras.newInstance(getActivity()).getContras(Contras.Type.PERMANENT);
        for (Contras.Category category:contras){
            View divider = new View(getActivity());
            divider.setLayoutParams(new LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT,1));
            divider.setBackgroundColor(darkDividerColor);
            mLayoutMain.addView(divider);
            title = inflater.inflate(R.layout.denied_title_1,mLayoutMain,false);
            view = (TextView)title.findViewById(R.id.text);
            view.setText(category.name);
            view.setTypeface(FontsHelper.getFont(FontsHelper.FontName.ROBOTO, FontsHelper.FontsStyle.REGULAR));
            mLayoutMain.addView(title);
            divider = new View(getActivity());
            divider.setLayoutParams(new LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT,1));
            divider.setBackgroundColor(darkDividerColor);
            mLayoutMain.addView(divider);
            for (Contras.Contra contra:category.items){
                divider = new View(getActivity());
                divider.setLayoutParams(new LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT,1));
                divider.setBackgroundColor(lightDividerColor);
                mLayoutMain.addView(divider);
                title = inflater.inflate(R.layout.denied_title_2,mLayoutMain,false);
                view = (TextView)title.findViewById(R.id.text);
                view.setText(contra.name);
                view.setTypeface(FontsHelper.getFont(FontsHelper.FontName.ROBOTO, FontsHelper.FontsStyle.REGULAR));
                mLayoutMain.addView(title);
                if (contra.list!=null){
                    title = inflater.inflate(R.layout.denied_desc,mLayoutMain,false);
                    view = (TextView)title.findViewById(R.id.text);
                    view.setText(contra.list);
                    view.setTypeface(FontsHelper.getFont(FontsHelper.FontName.ROBOTO, FontsHelper.FontsStyle.REGULAR));
                    mLayoutMain.addView(title);
                }
            }
        }
    }


    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        try {
            mListener = (OnFragmentInteractionListener) activity;
        } catch (ClassCastException e) {
            throw new ClassCastException(activity.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    @Override
    public void onResume() {
        super.onResume();
        if (mListener!=null)
            mListener.setTitle(R.string.title_fragment_denied);
    }
}
