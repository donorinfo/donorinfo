package com.lexadev.donorinfo.ui;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.graphics.Paint;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;

import com.lexadev.donorinfo.OnFragmentInteractionListener;
import com.lexadev.donorinfo.OnPossibleEventUpdates;
import com.lexadev.donorinfo.R;
import com.lexadev.donorinfo.helper.FontsHelper;
import com.lexadev.donorinfo.helper.FragmentState;
import com.lexadev.donorinfo.helper.Utils;
import com.lexadev.donorinfo.parse.AppUser;
import com.parse.ParseException;
import com.parse.SaveCallback;

import java.util.ArrayList;


public class ProfileFragment extends Fragment {

    private AppUser mCurrentUser = AppUser.getCurrentUser();
    private TextView mFirstName;
    private EditText mEditFirstName;

    private TextView mLastName;
    private EditText mEditLastName;

    private TextView mSex;
    private Spinner mSpinnerSex;

    private TextView mTextBlood;
    private Spinner mSpinnerBlood;

    private TextView mBloodCount;
    private EditText mEditBloodCount;

    private TextView mPhone;
    private EditText mEditPhone;

    private TextView mRegion;
    private TextView mEditRegion;

    private int mSelectedRegion = -1;

    private CheckBox mAdmin;

    private ArrayList<View> mValueViews = new ArrayList<>();
    private ArrayList<View> mEditViews = new ArrayList<>();
    private int[] mTitles = new int[]{
            R.id.text_title_last_name,
            R.id.text_title_first_name,
            R.id.text_title_sex,
            R.id.text_title_blood,
            R.id.text_title_count_blood,
            R.id.text_title_phone_number,
            R.id.text_title_region
    };
    private String[] mRegionIdxArray;
    private String[] mRegionNames = null;
    private FragmentState mState = FragmentState.VIEW;
    private OnFragmentInteractionListener mListener;
    private OnPossibleEventUpdates mPListener;

    public ProfileFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        if (getResources()!=null) {
            mRegionIdxArray = getResources().getStringArray(R.array.array_regions_idx);
            mRegionNames = getResources().getStringArray(R.array.array_region_names);
        }

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        if (mCurrentUser == null){
            if (mListener != null) {
                mListener.toProfile(false);

            }
            return null;
        }
        View rootView =inflater.inflate(R.layout.fragment_profile, container, false);
        handleViews(rootView);
        if (mListener!=null)
            mListener.setupUI(rootView);
        fillForm();
        return rootView;
    }

    private void handleViews(View rootView) {
        mFirstName = (TextView)rootView.findViewById(R.id.text_first_name_value);
        mLastName = (TextView)rootView.findViewById(R.id.text_second_name_value);
        mSex = (TextView)rootView.findViewById(R.id.text_sex_value);
        mTextBlood = (TextView)rootView.findViewById(R.id.text_blood_value);
        mBloodCount = (TextView)rootView.findViewById(R.id.text_count_blood_value);
        mPhone = (TextView)rootView.findViewById(R.id.text_phone_number_value);
        mRegion = (TextView)rootView.findViewById(R.id.text_region_value);
        mValueViews.add(mFirstName);
        mValueViews.add(mLastName);
        mValueViews.add(mSex);
        mValueViews.add(mTextBlood);
        mValueViews.add(mBloodCount);
        mValueViews.add(mPhone);
        mValueViews.add(mRegion);
        for (View view:mValueViews){
            if (view instanceof TextView){
                TextView text = (TextView)view;
                text.setTypeface(FontsHelper.getFont(FontsHelper.FontName.ROBOTO, FontsHelper.FontsStyle.REGULAR));
            }
        }

        mEditFirstName = (EditText)rootView.findViewById(R.id.edit_first_name);
        mEditLastName = (EditText)rootView.findViewById(R.id.edit_second_name);
        mSpinnerSex = (Spinner)rootView.findViewById(R.id.spinner_sex);
        mSpinnerBlood = (Spinner)rootView.findViewById(R.id.spinner_blood);
        mEditBloodCount = (EditText)rootView.findViewById(R.id.edit_count_blood);
        mEditPhone = (EditText)rootView.findViewById(R.id.edit_phone_number);
        mEditRegion = (TextView)rootView.findViewById(R.id.text_select_region);
        mEditRegion.setPaintFlags(mEditRegion.getPaintFlags()|Paint.UNDERLINE_TEXT_FLAG);
        mEditViews.add(mEditFirstName);
        mEditViews.add(mEditLastName);
        mEditViews.add(mSpinnerSex);
        mEditViews.add(mSpinnerBlood);
        mEditViews.add(mEditBloodCount);
        mEditViews.add(mEditPhone);
        mEditViews.add(mEditRegion);
        for (View view:mEditViews){
            if (view instanceof EditText){
                EditText text = (EditText)view;
                text.setTypeface(FontsHelper.getFont(FontsHelper.FontName.ROBOTO, FontsHelper.FontsStyle.REGULAR));
            }
        }

        CustomSpinnerAdapter adapterSex = new CustomSpinnerAdapter(getActivity(),R.layout.simple_spinner_item,getResources().getStringArray(R.array.array_sex));
        adapterSex.setDropDownViewResource(R.layout.simple_spinner_dropdown_item);
        mSpinnerSex.setAdapter(adapterSex);
        CustomSpinnerAdapter adapterBlood = new CustomSpinnerAdapter(getActivity(),R.layout.simple_spinner_item,getResources().getStringArray(R.array.array_blood));
        adapterBlood.setDropDownViewResource(R.layout.simple_spinner_dropdown_item);
        mSpinnerBlood.setAdapter(adapterBlood);
        mEditRegion.setOnClickListener(mSelectRegionClick);
        for (int i=0;i<mTitles.length;i++){
            TextView text = (TextView)rootView.findViewById(mTitles[i]);
            if (text!=null)
                text.setTypeface(FontsHelper.getFont(FontsHelper.FontName.ROBOTO, FontsHelper.FontsStyle.MEDIUM));
        }
        mAdmin = (CheckBox)rootView.findViewById(R.id.check_admin);
        mAdmin.setTypeface(FontsHelper.getFont(FontsHelper.FontName.ROBOTO, FontsHelper.FontsStyle.REGULAR));
        if (mCurrentUser.isAdmin())
            mEditViews.add(mAdmin);
    }
    private View.OnClickListener mSelectRegionClick = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
            builder.setTitle(R.string.title_select_region);
            builder.setSingleChoiceItems(mRegionNames,mSelectedRegion,new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    dialog.dismiss();
                    String idx = null;
                    if (which>=0) {
                        idx = mRegionIdxArray[which];
                    }
                    mSelectedRegion = which;
                    if (which>=0) {
                        mRegion.setText(mRegionNames[which]);
                        mEditRegion.setText(mRegionNames[which]);
                    }

                }
            });
            builder.setNegativeButton(android.R.string.cancel,null);
            builder.show();
        }
    };

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        try {
            mListener = (OnFragmentInteractionListener) activity;
        } catch (ClassCastException e) {
            throw new ClassCastException(activity.toString()
                    + " must implement OnFragmentInteractionListener");
        }
        try {
            mPListener = (OnPossibleEventUpdates) activity;
        } catch (ClassCastException e) {
            throw new ClassCastException(activity.toString()
                    + " must implement OnPossibleEventUpdates");
        }

    }

    private void fillForm(){
        String firstName = mCurrentUser.getFirstName();
        mFirstName.setText(Utils.notNull(firstName));
        mEditFirstName.setText(Utils.notNull(firstName));
        String lastName = mCurrentUser.getLastName();
        mLastName.setText(Utils.notNull(lastName));
        mEditLastName.setText(Utils.notNull(lastName));
        int sex = mCurrentUser.getSex();
        mSpinnerSex.setSelection(sex);
        mSex.setText((String)mSpinnerSex.getSelectedItem());
        String blood = mCurrentUser.getBlood();
        for (int i=0;i<mSpinnerBlood.getAdapter().getCount();i++){
            if ((mSpinnerBlood.getAdapter().getItem(i)).equals(blood)){
                mSpinnerBlood.setSelection(i);
                break;
            }
        }
        mTextBlood.setText((String)mSpinnerBlood.getSelectedItem());
        String countBlood = mCurrentUser.getCountBlood();
        mBloodCount.setText(Utils.notNull(countBlood));
        mEditBloodCount.setText(Utils.notNull(countBlood));
        String phone = mCurrentUser.getPhone();
        mPhone.setText(Utils.notNull(phone));
        mEditPhone.setText(Utils.notNull(phone));
        if (mRegionIdxArray!=null) {
            String idx = mCurrentUser.getRegion();
            if (idx==null)
                idx="77";
            for (int i = 0; i < mRegionIdxArray.length; i++){
                if (mRegionIdxArray[i].equals(idx)){
                    String name = mRegionNames[i];
                    mRegion.setText(name);
                    mEditRegion.setText(name);
                    mSelectedRegion = i;
                }
            }
        }
        if (mListener!=null)
            mAdmin.setChecked(mListener.isUseAdmin());
    }
    private void saveForm(){

        mCurrentUser.setFirstName(mEditFirstName.getText().toString());
        mCurrentUser.setLastName(mEditLastName.getText().toString());
        mCurrentUser.setSex(mSpinnerSex.getSelectedItemPosition());
        mCurrentUser.setBlood((String)mSpinnerBlood.getSelectedItem());
        mCurrentUser.setCountBlood(mEditBloodCount.getText().toString());
        mCurrentUser.setPhone(mEditPhone.getText().toString());
        mCurrentUser.setRegion(mRegionIdxArray[mSelectedRegion]);
        if (mListener!=null)
            mListener.startWaiting(R.string.progress_saving,null);
        mCurrentUser.saveInBackground(new SaveCallback() {
            @Override
            public void done(ParseException e) {
                if (mListener!=null)
                    mListener.stopWaiting();
                if (e==null){
                    fillForm();
                    toValueViews();
                }
                else{
                    if (mListener!=null)
                        mListener.showToast(R.string.toast_failed_to_save,e.getMessage());
                }
            }
        });
        if (mListener!=null)
            mListener.setUseAdmin(mAdmin.isChecked());
    }
    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
        mPListener = null;
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        setHasOptionsMenu(true);
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        super.onCreateOptionsMenu(menu, inflater);
        inflater.inflate(R.menu.profile_menu,menu);
        MenuItem itemSave = menu.findItem(R.id.action_save);
        MenuItem itemEdit = menu.findItem(R.id.action_edit);
        if (mState==FragmentState.VIEW) {
            itemSave.setVisible(false);
            itemEdit.setVisible(true);
        }
        else{
            itemSave.setVisible(true);
            itemEdit.setVisible(false);
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()){
            case R.id.action_edit:
                edit();
                return true;
            case R.id.action_save:
                save();
                return true;
            case R.id.action_exit:
                logout();
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

    private void logout() {
        if (!isAdded())
            return;
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        builder.setTitle(R.string.title_logout);
        builder.setMessage(R.string.message_logout);
        builder.setNegativeButton(android.R.string.no,null);
        builder.setPositiveButton(android.R.string.yes,new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                mCurrentUser.logOut();
                if (mPListener!=null)
                    mPListener.invalidate();
                if (mListener!=null)
                    mListener.toProfile(false);
            }
        });
        builder.show();
    }

    private void save() {
        mState = FragmentState.VIEW;
        getActivity().supportInvalidateOptionsMenu();
        saveForm();
    }

    private void toValueViews() {
        for (View view:mEditViews){
            view.setVisibility(View.GONE);
        }
        for (View view:mValueViews){
            view.setVisibility(View.VISIBLE);
        }
    }
    private void toEditViews() {
        for (View view:mEditViews){
            view.setVisibility(View.VISIBLE);
        }
        for (View view:mValueViews){
            view.setVisibility(View.GONE);
        }
    }

    private void edit() {
        mState = FragmentState.EDIT;
        getActivity().supportInvalidateOptionsMenu();
        toEditViews();
    }
    private class CustomSpinnerAdapter extends ArrayAdapter<String>{

        public CustomSpinnerAdapter(Context context, int resource, String[] objects) {
            super(context, resource, objects);
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {
            View rootView = super.getView(position, convertView, parent);
            TextView text = (TextView)rootView.findViewById(android.R.id.text1);
            if (text!=null){
                text.setTypeface(FontsHelper.getFont(FontsHelper.FontName.ROBOTO, FontsHelper.FontsStyle.REGULAR));
            }
            return rootView;
        }
    }

    @Override
    public void onResume() {
        super.onResume();
        if (mListener!=null)
            mListener.setTitle(R.string.title_profile);
    }
}
