package com.lexadev.donorinfo.ui;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.BaseAdapter;
import android.widget.GridView;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.lexadev.donorinfo.AppConstants;
import com.lexadev.donorinfo.OnFragmentInteractionListener;
import com.lexadev.donorinfo.OnPossibleEventUpdates;
import com.lexadev.donorinfo.R;
import com.lexadev.donorinfo.helper.FontsHelper;
import com.lexadev.donorinfo.helper.Utils;
import com.lexadev.donorinfo.model.ParseEvent;
import com.lexadev.donorinfo.parse.AppUser;
import com.lexadev.donorinfo.widgets.RatioFrameLayout;
import com.parse.FindCallback;
import com.parse.GetCallback;
import com.parse.ParseException;
import com.parse.ParseQuery;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.List;

public class MonthFragment extends Fragment {
    private static final String ARG_DATE = "date";
    private static final String TAG = "MonthFragment";
    private static final String CURRENT_POSITION = "state.current_position";

    private long mDate;
    private final Calendar mCalendarCurrent = GregorianCalendar.getInstance();
    private String mRegion = null;
    private TextView mMonthName;
    private GridView mGridDays;
    private ListView mListEvents;
    private ProgressBar mProgressBar;
    private OnFragmentInteractionListener mListener;
    private OnPossibleEventUpdates mPListener;
    private RatioFrameLayout mSelected = null;
    private int mCurrentPosition = -1;
    private LinearLayout mLayoutEvents;
    private int mTodayPosition = -1;

    private String[] mEventsNames;
    private String[] mBloodEventsType;
    private List<ParseEvent> mEvents = new ArrayList<ParseEvent>();
    private boolean mSelectToday = false;
    private ParseEvent mBloodEvent = null;
    private ParseEvent mPlazmaEvent = null;
    private ParseEvent mTromboEvent = null;

    private boolean mEventQueried = false;
    private boolean mBloodQueried = false;
    private boolean mPlazmaQueried = false;
    private boolean mTromboQueried = false;

    private class CalendarItem {


        public CalendarItem(long date, boolean isCurrentMonth) {
            this.date = date;
            this.isCurrentMonth = isCurrentMonth;
        }

        long date;
        public boolean isToday = false;
        public boolean isCurrentMonth = false;
        public boolean red = false;
        public boolean yellow = false;
        public boolean green = false;
        public boolean analize = false;
        public boolean denied = false;
        public boolean redC = false;
        public boolean yellowC = false;
        public boolean greenC = false;
        public boolean isRedP = false;
        public boolean isYellowP = false;
        public boolean isGreenP = false;
        public boolean isRegional = false;
        public ArrayList<ParseEvent> eventsList = new ArrayList<>();
    }

    private ArrayList<CalendarItem> mItems = new ArrayList<>();

    public static MonthFragment newInstance(long date) {
        MonthFragment fragment = new MonthFragment();
        Bundle args = new Bundle();
        args.putLong(ARG_DATE, date);
        fragment.setArguments(args);
        return fragment;
    }

    public MonthFragment() {
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mDate = getArguments().getLong(ARG_DATE);
            mCalendarCurrent.setTimeInMillis(mDate);
            mCalendarCurrent.set(Calendar.HOUR_OF_DAY, 12);
            mCalendarCurrent.set(Calendar.MINUTE, 0);
            mCalendarCurrent.set(Calendar.SECOND, 0);
            mCalendarCurrent.set(Calendar.MILLISECOND, 0);
        }
        if (savedInstanceState != null) {
            mCurrentPosition = savedInstanceState.getInt(CURRENT_POSITION, -1);
        }
        mEventsNames = getResources().getStringArray(R.array.array_events);
        mBloodEventsType = getResources().getStringArray(R.array.array_events_type);
        if (mListener != null) {
            String idx = mListener.getSelectedRegion();
            if (idx == null && AppUser.getCurrentUser() != null) {
                idx = AppUser.getCurrentUser().getRegion();
            }
            mRegion = idx;
        }

    }

    private void createDaysList() {
        mEvents.clear();
        mBloodEvent = null;
        mPlazmaEvent = null;
        mTromboEvent = null;

        mEventQueried = false;
        mBloodQueried = false;
        mPlazmaQueried = false;
        mTromboQueried = false;

        mProgressBar.setVisibility(View.VISIBLE);
        mGridDays.setVisibility(View.GONE);
        mLayoutEvents.setVisibility(View.GONE);
        mItems.clear();
        final Calendar calendarFrom = GregorianCalendar.getInstance();
        calendarFrom.setTimeInMillis(mDate);
        calendarFrom.set(Calendar.DAY_OF_MONTH, 1);
        calendarFrom.set(Calendar.HOUR_OF_DAY, 12);
        calendarFrom.set(Calendar.MINUTE, 0);
        calendarFrom.set(Calendar.SECOND, 0);
        calendarFrom.set(Calendar.MILLISECOND, 0);
        int numDay = calendarFrom.get(Calendar.DAY_OF_WEEK);
        if (numDay == Calendar.SUNDAY)
            calendarFrom.add(Calendar.DAY_OF_MONTH, -6);
        else
            calendarFrom.add(Calendar.DAY_OF_MONTH, 2 - numDay);

        final Calendar calendarTo = GregorianCalendar.getInstance();
        calendarTo.setTimeInMillis(mDate);
        calendarTo.set(Calendar.DAY_OF_MONTH, calendarTo.getActualMaximum(Calendar.DAY_OF_MONTH));
        numDay = calendarTo.get(Calendar.DAY_OF_WEEK);
        if (numDay != Calendar.SUNDAY)
            calendarTo.add(Calendar.DAY_OF_MONTH, 8 - numDay);
        calendarTo.set(Calendar.HOUR_OF_DAY, 12);
        calendarTo.set(Calendar.MINUTE, 0);
        calendarTo.set(Calendar.SECOND, 0);
        calendarTo.set(Calendar.MILLISECOND, 0);
        ParseQuery<ParseEvent> query = ParseEvent.getEvents(calendarFrom.getTime(), calendarTo.getTime(), mRegion);
        if (query != null) {
            query.findInBackground(new FindCallback<ParseEvent>() {
                @Override
                public void done(List<ParseEvent> parseEvents, ParseException e) {
                    if (e == null) {
                        if (parseEvents!=null)
                            mEvents = parseEvents;
                    } else {
                        if (mListener != null)
                            mListener.showToast(R.string.toast_loading_error, e.getMessage());
                    }
                    mEventQueried = true;
                    fillAdapter(calendarFrom, calendarTo);
                }
            });
        } else {
            mEventQueried = true;
            fillAdapter(calendarFrom, calendarTo);
        }
        if (mPListener != null)
            mBloodEvent = mPListener.getEvent(ParseEvent.BLOOD_EVENT_BLOOD);
        if (mBloodEvent == null) {

            ParseQuery<ParseEvent> queryBlood = ParseEvent.getLastBloodEvent(ParseEvent.BLOOD_EVENT_BLOOD);
            if (queryBlood != null) {
                queryBlood.getFirstInBackground(new GetCallback<ParseEvent>() {
                    @Override
                    public void done(ParseEvent parseEvent, ParseException e) {
                        if (e == null) {
                            mBloodEvent = parseEvent;
                            if (mPListener != null && mBloodEvent != null)
                                mPListener.updateEvent(ParseEvent.BLOOD_EVENT_BLOOD, mBloodEvent);
                        } else {
                            if (e.getCode()!= ParseException.OBJECT_NOT_FOUND)
                                if (mListener != null)
                                    mListener.showToast(R.string.toast_loading_error, e.getMessage());
                        }
                        mBloodQueried = true;
                        fillAdapter(calendarFrom, calendarTo);
                    }
                });
            } else {
                mBloodQueried = true;
                fillAdapter(calendarFrom, calendarTo);
            }
        } else {
            mBloodQueried = true;
            fillAdapter(calendarFrom, calendarTo);
        }
        if (mPListener != null)
            mPlazmaEvent = mPListener.getEvent(ParseEvent.BLOOD_EVENT_PLAZMA);
        if (mPlazmaEvent == null) {

            ParseQuery<ParseEvent> queryPlazma = ParseEvent.getLastBloodEvent(ParseEvent.BLOOD_EVENT_PLAZMA);
            if (queryPlazma != null) {
                queryPlazma.getFirstInBackground(new GetCallback<ParseEvent>() {
                    @Override
                    public void done(ParseEvent parseEvent, ParseException e) {
                        if (e == null) {
                            mPlazmaEvent = parseEvent;
                            if (mPListener != null && mPlazmaEvent != null)
                                mPListener.updateEvent(ParseEvent.BLOOD_EVENT_PLAZMA, mPlazmaEvent);

                        } else {
                            if (e.getCode()!= ParseException.OBJECT_NOT_FOUND)
                                if (mListener != null)
                                    mListener.showToast(R.string.toast_loading_error, e.getMessage());
                            Log.d(TAG,"Error: "+e.getCode());
                        }
                        mPlazmaQueried = true;
                        fillAdapter(calendarFrom, calendarTo);
                    }
                });
            } else {
                mPlazmaQueried = true;
                fillAdapter(calendarFrom, calendarTo);
            }
        } else {
            mPlazmaQueried = true;
            fillAdapter(calendarFrom, calendarTo);
        }

        if (mPListener != null)
            mTromboEvent = mPListener.getEvent(ParseEvent.BLOOD_EVENT_TROMBO);
        if (mTromboEvent == null) {

            ParseQuery<ParseEvent> queryTrombo = ParseEvent.getLastBloodEvent(ParseEvent.BLOOD_EVENT_TROMBO);
            if (queryTrombo != null) {
                queryTrombo.getFirstInBackground(new GetCallback<ParseEvent>() {
                    @Override
                    public void done(ParseEvent parseEvent, ParseException e) {
                        if (e == null) {
                            mTromboEvent = parseEvent;
                            if (mPListener != null && mTromboEvent != null)
                                mPListener.updateEvent(ParseEvent.BLOOD_EVENT_TROMBO, mTromboEvent);

                        } else {
                            if (e.getCode()!= ParseException.OBJECT_NOT_FOUND)
                                if (mListener != null)
                                    mListener.showToast(R.string.toast_loading_error, e.getMessage());
                        }
                        mTromboQueried = true;
                        fillAdapter(calendarFrom, calendarTo);
                    }
                });
            } else {
                mTromboQueried = true;
                fillAdapter(calendarFrom, calendarTo);
            }
        } else {
            mTromboQueried = true;
            fillAdapter(calendarFrom, calendarTo);
        }

    }

    private void fillAdapter(Calendar calendarFrom, Calendar calendarTo) {
        Date fromDate = calendarFrom.getTime();
        Date toDate = calendarTo.getTime();
        if (AppConstants.DEBUG)
            Log.d(TAG, "fillAdapter: " + fromDate + " - " + toDate + ": " + mEventQueried + ", " + mBloodQueried + ", " + mPlazmaQueried + ", " + mTromboQueried);
        if (!mEventQueried || !mBloodQueried || !mPlazmaQueried || !mTromboQueried)
            return;
        if (mBloodEvent != null && mBloodEvent.getDate().after(fromDate) && mBloodEvent.getDate().before(toDate))
            mEvents.add(mBloodEvent);
        if (mTromboEvent != null && mTromboEvent.getDate().after(fromDate) && mTromboEvent.getDate().before(toDate))
            mEvents.add(mTromboEvent);
        if (mPlazmaEvent != null && mPlazmaEvent.getDate().after(fromDate) && mPlazmaEvent.getDate().before(toDate))
            mEvents.add(mPlazmaEvent);

        final Calendar today = GregorianCalendar.getInstance();
        today.set(Calendar.HOUR_OF_DAY, 12);
        today.set(Calendar.MINUTE, 0);
        today.set(Calendar.SECOND, 0);
        today.set(Calendar.MILLISECOND, 0);

        Calendar current = GregorianCalendar.getInstance();
        current.setTimeInMillis(mDate);
        final int currentMonth = current.get(Calendar.MONTH);
        do {
            CalendarItem item = new CalendarItem(calendarFrom.getTimeInMillis(), calendarFrom.get(Calendar.MONTH) == currentMonth);
            if (today.compareTo(calendarFrom) == 0) {
                item.isToday = true;
            }
            Date dayBegin = Utils.getBeginDay(new Date(item.date));
            for (int i = 0; i < mEvents.size(); i++) {
                boolean exists = false;
                ParseEvent event = mEvents.get(i);

                if (Utils.getBeginDay(event.getDate()).equals(dayBegin)) {

                    if (!event.getManualSave()){
                        for (ParseEvent ev:item.eventsList){
                            if (ev.getEvent()==ParseEvent.BLOOD_EVENT&&ev.getEventType()==event.getEventType())
                                exists = true;
                            break;
                        }
                    }
                    if (exists)
                        continue;
                    item.eventsList.add(event);
                    if (event.isPersonal()) {
                        if (event.getEvent() == ParseEvent.BLOOD_EVENT) {
                            if (event.getEventType() == ParseEvent.BLOOD_EVENT_BLOOD) {
                                item.red = true;
                                if (event.getStatus() == ParseEvent.STATUS_DONE)
                                    item.redC = true;
                                item.isRedP = event.getManualSave();
                            } else if (event.getEventType() == ParseEvent.BLOOD_EVENT_PLAZMA) {
                                item.yellow = true;
                                if (event.getStatus() == ParseEvent.STATUS_DONE)
                                    item.yellowC = true;
                                item.isYellowP = event.getManualSave();
                            } else if (event.getEventType() == ParseEvent.BLOOD_EVENT_TROMBO) {
                                item.green = true;
                                if (event.getStatus() == ParseEvent.STATUS_DONE)
                                    item.greenC = true;
                                item.isGreenP = event.getManualSave();
                            }
                        } else if (event.getEvent() == ParseEvent.ANALIZE_EVENT) {
                            item.analize = true;
                        } else if (event.getEvent() == ParseEvent.DENIED_EVENT) {
                            item.denied = true;
                        }
                    } else {
                        item.isRegional = true;
                    }

                }
            }
            if (item.isToday)
                mTodayPosition = mItems.size();
            mItems.add(item);
            calendarFrom.add(Calendar.DAY_OF_MONTH, 1);
        } while (calendarFrom.compareTo(calendarTo) <= 0);
        mProgressBar.setVisibility(View.GONE);
        mGridDays.setVisibility(View.VISIBLE);
        if (AppUser.getCurrentUser() != null)
            mLayoutEvents.setVisibility(View.VISIBLE);
        if (mSelectToday && mTodayPosition >= 0) {
            mGridDays.setItemChecked(mTodayPosition, true);
            showEvents(mTodayPosition);
        }
        mSelectToday = false;

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        View rootView = inflater.inflate(R.layout.fragment_month, container, false);
        handleViews(rootView);
        createDaysList();
        fillCalendar(inflater);
        return rootView;
    }

    public static class DaysAdapter extends ArrayAdapter<CalendarItem> {
        private Context context;
        private int resource;
        private List<CalendarItem> items;
        private LayoutInflater inflater;
        private int bloodIcSize = 16;

        public DaysAdapter(Context context, int resource, List<CalendarItem> objects) {
            super(context, resource, objects);
            this.context = context;
            this.resource = resource;
            this.items = objects;
            bloodIcSize = context.getResources().getDimensionPixelSize(R.dimen.blood_item_size);
            inflater = LayoutInflater.from(context);
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {
            ViewsHolder holder = null;
            if (convertView == null) {
                convertView = inflater.inflate(resource, parent, false);
                holder = new ViewsHolder(convertView);
            } else {
                holder = (ViewsHolder) convertView.getTag();
            }
            holder.build(position);
            return convertView;
        }


        private class ViewsHolder {
            public TextView text;
            public LinearLayout events;
            public RatioFrameLayout layout;

            public ViewsHolder(View rootView) {
                text = (TextView) rootView.findViewById(R.id.text);
                text.setTypeface(FontsHelper.getFont(FontsHelper.FontName.ROBOTO, FontsHelper.FontsStyle.REGULAR));
                events = (LinearLayout) rootView.findViewById(R.id.events);
                layout = (RatioFrameLayout) rootView.findViewById(R.id.layout_day);
                rootView.setTag(this);
            }

            public void build(int position) {
                CalendarItem item = getItem(position);
                Calendar calendar = GregorianCalendar.getInstance();
                calendar.setTimeInMillis(item.date);
                boolean isFuture = calendar.getTimeInMillis() - System.currentTimeMillis() > 0;
                text.setText(Integer.toString(calendar.get(Calendar.DAY_OF_MONTH)));
                events.removeAllViews();
                if (!item.isCurrentMonth)
                    text.setTextColor(context.getResources().getColor(R.color.day_gray));
                else {
                    if (item.red) {
                        if (isFuture && !item.isRedP)
                            events.addView(getImageView(R.drawable.ic_possible_blood));
                        else
                            events.addView(getImageView(R.drawable.ic_red_blood));
                    }
                    if (item.yellow) {
                        if (isFuture && !item.isYellowP)
                            events.addView(getImageView(R.drawable.ic_possible_plazma));
                        else
                            events.addView(getImageView(R.drawable.ic_yellow_blood));
                    }
                    if (item.green) {
                        if (isFuture && !item.isGreenP)
                            events.addView(getImageView(R.drawable.ic_possible_trombo));
                        else
                            events.addView(getImageView(R.drawable.ic_green_blood));
                    }
                    if (item.analize) {
                        events.addView(getImageView(R.drawable.ic_analize));
                    }
                    if (item.denied) {
                        events.addView(getImageView(R.drawable.ic_denied));
                    }
                    if (item.isRegional) {
                        events.addView(getImageView(R.drawable.ic_regional_news));
                    }
                    setItemBackground(item);

                }
            }

            private ImageView getImageView(int res) {
                ImageView view = new ImageView(context);
                LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(bloodIcSize, bloodIcSize);
                view.setLayoutParams(params);
                view.setImageResource(res);
                return view;
            }

            private void setItemBackground(CalendarItem item) {
                if (item.isToday) {
                    text.setTextColor(context.getResources().getColor(android.R.color.white));
                    layout.setBackgroundResource(R.drawable.day_today);
                } else {
                    if (item.redC)
                        layout.setBackgroundResource(R.drawable.day_blood);
                    else if (item.yellowC)
                        layout.setBackgroundResource(R.drawable.day_plazma);
                    else if (item.greenC)
                        layout.setBackgroundResource(R.drawable.day_trombo);
                    else
                        layout.setBackgroundResource(R.drawable.day_no_events);
                }

            }

        }
    }

    private void fillCalendar(LayoutInflater inflater) {
        DaysAdapter adapter = new DaysAdapter(getActivity(), R.layout.calendar_day, mItems);
        mGridDays.setAdapter(adapter);
        mGridDays.setChoiceMode(GridView.CHOICE_MODE_SINGLE);
        mGridDays.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                showEvents(position);
            }
        });
        int monthNum = mCalendarCurrent.get(Calendar.MONTH);
        String monthName = CalendarFragment.MONTHS_NAMES[monthNum];
        if (mCalendarCurrent.get(Calendar.YEAR) != GregorianCalendar.getInstance().get(Calendar.YEAR)) {
            monthName += ", " + mCalendarCurrent.get(Calendar.YEAR);
        }
        mMonthName.setText(monthName);

    }


    private void showEvents(int position) {
        CalendarItem item = mItems.get(position);
        mCurrentPosition = position;
        ((EventsAdapter) mListEvents.getAdapter()).notifyDataSetChanged();
    }

    private void handleViews(View rootView) {
        mMonthName = (TextView) rootView.findViewById(R.id.text_month_name);
        mMonthName.setTypeface(FontsHelper.getFont(FontsHelper.FontName.ROBOTO, FontsHelper.FontsStyle.REGULAR));
        mGridDays = (GridView) rootView.findViewById(R.id.grid);
        mListEvents = (ListView) rootView.findViewById(R.id.list_events);
        mLayoutEvents = (LinearLayout) rootView.findViewById(R.id.layout_events);
        final EventsAdapter adapter = new EventsAdapter(getActivity());
        mListEvents.setAdapter(adapter);
        mListEvents.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                ParseEvent event = (ParseEvent) adapter.getItem(position);
                onEventClick(mItems.get(mCurrentPosition), event);
            }
        });
        TextView titleDayEvents = (TextView) rootView.findViewById(R.id.text_day_events);
        titleDayEvents.setTypeface(FontsHelper.getFont(FontsHelper.FontName.ROBOTO, FontsHelper.FontsStyle.REGULAR));
        TextView titleDay = (TextView) rootView.findViewById(R.id.text_monday);
        titleDay.setTypeface(FontsHelper.getFont(FontsHelper.FontName.ROBOTO, FontsHelper.FontsStyle.REGULAR));
        titleDay = (TextView) rootView.findViewById(R.id.text_tursday);
        titleDay.setTypeface(FontsHelper.getFont(FontsHelper.FontName.ROBOTO, FontsHelper.FontsStyle.REGULAR));
        titleDay = (TextView) rootView.findViewById(R.id.text_wednesday);
        titleDay.setTypeface(FontsHelper.getFont(FontsHelper.FontName.ROBOTO, FontsHelper.FontsStyle.REGULAR));
        titleDay = (TextView) rootView.findViewById(R.id.text_thursday);
        titleDay.setTypeface(FontsHelper.getFont(FontsHelper.FontName.ROBOTO, FontsHelper.FontsStyle.REGULAR));
        titleDay = (TextView) rootView.findViewById(R.id.text_friday);
        titleDay.setTypeface(FontsHelper.getFont(FontsHelper.FontName.ROBOTO, FontsHelper.FontsStyle.REGULAR));
        titleDay = (TextView) rootView.findViewById(R.id.text_saturday);
        titleDay.setTypeface(FontsHelper.getFont(FontsHelper.FontName.ROBOTO, FontsHelper.FontsStyle.REGULAR));
        titleDay = (TextView) rootView.findViewById(R.id.text_sunday);
        titleDay.setTypeface(FontsHelper.getFont(FontsHelper.FontName.ROBOTO, FontsHelper.FontsStyle.REGULAR));
        mProgressBar = (ProgressBar) rootView.findViewById(R.id.progress);
    }

    private void onEventClick(CalendarItem calendarItem, ParseEvent event) {
        if (mListener != null) {
            if (event == null) {
                mListener.addNewEvent(calendarItem != null ? calendarItem.date : mCalendarCurrent.getTimeInMillis());
            } else {
                if (event.getEvent() == ParseEvent.REGION_EVENT) {
                    if (event.getNote() != null) {
                        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
                        builder.setTitle(R.string.title_regional_event);
                        String message = getString(R.string.message_regional_event, event.getNote(), Utils.getDateString(getActivity(), event.getDate().getTime()));
                        builder.setMessage(message);
                        builder.setPositiveButton(android.R.string.ok, null);
                        builder.show();
                    }
                } else {
                    mListener.editEvent(event);
                }
            }
        }

    }

    private class EventsAdapter extends BaseAdapter {

        private LayoutInflater inflater;

        public EventsAdapter(Context context) {
            inflater = LayoutInflater.from(context);
        }

        @Override
        public int getCount() {
            if (mCurrentPosition >= 0 && mItems.size() > mCurrentPosition) {
                return mItems.get(mCurrentPosition).eventsList.size() + 1;
            } else {
                return 0;
            }
        }

        @Override
        public Object getItem(int position) {
            int itemCount = 0;
            if (mCurrentPosition >= 0) {
                itemCount = mItems.get(mCurrentPosition).eventsList.size();
            }
            if (position < itemCount)
                return mItems.get(mCurrentPosition).eventsList.get(position);
            return null;
        }

        @Override
        public long getItemId(int position) {
            return position;
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {
            EventsAdapter.ViewsHolder holder = null;
            if (convertView == null) {
                convertView = inflater.inflate(R.layout.event_row, parent, false);
                holder = new ViewsHolder(convertView);
            } else {
                holder = (ViewsHolder) convertView.getTag();
            }
            holder.build(position);
            return convertView;
        }

        private class ViewsHolder {
            TextView text;
            ImageView imageOpen;
            ImageView imageNew;
            ImageView imageDone;

            public ViewsHolder(View rootView) {
                text = (TextView) rootView.findViewById(R.id.text);
                text.setTypeface(FontsHelper.getFont(FontsHelper.FontName.ROBOTO, FontsHelper.FontsStyle.REGULAR));
                imageNew = (ImageView) rootView.findViewById(R.id.image_new);
                imageOpen = (ImageView) rootView.findViewById(R.id.image_open);
                imageDone = (ImageView) rootView.findViewById(R.id.image_done);
                rootView.setTag(this);
            }

            public void build(int position) {
                ParseEvent event = (ParseEvent) getItem(position);
                if (event == null) {
                    if (mCurrentPosition >= 0 && mCurrentPosition < mItems.size()) {
                        CalendarItem item = mItems.get(mCurrentPosition);
                        if (item != null && item.date < System.currentTimeMillis()) {
                            text.setText(R.string.text_add_old_event);
                        } else {
                            text.setText(R.string.text_add_new_event);
                        }
                    } else {
                        text.setText(R.string.text_add_new_event);
                    }
                    imageNew.setVisibility(View.VISIBLE);
                    imageOpen.setVisibility(View.GONE);
                    imageDone.setVisibility(View.GONE);

                } else {
                    int eventIdx = event.getEvent();
                    imageNew.setVisibility(View.GONE);
                    String str;
                    if (!event.isPersonal()) {
                        imageDone.setVisibility(View.VISIBLE);
                        imageOpen.setVisibility(View.GONE);
                        str = getString(R.string.event_region) + " " + Utils.notNull(event.getNote());
                    } else {
                        long eventDate = event.getDate().getTime();
                        boolean isPlanned = event.getManualSave();
                        if (eventDate - System.currentTimeMillis() > 0) {
                            if (eventIdx == ParseEvent.BLOOD_EVENT) {
                                int bloodEventType = event.getEventType();
                                if (bloodEventType == ParseEvent.BLOOD_EVENT_BLOOD) {
                                    if (isPlanned)
                                        str = getString(R.string.status_do_planed_blood);
                                    else
                                        str = getString(R.string.status_do_blood);
                                } else if (bloodEventType == ParseEvent.BLOOD_EVENT_PLAZMA) {
                                    if (isPlanned)
                                        str = getString(R.string.status_do_planed_plazma);
                                    else
                                        str = getString(R.string.status_do_plazma);
                                } else if (bloodEventType == ParseEvent.BLOOD_EVENT_TROMBO) {
                                    if (isPlanned)
                                        str = getString(R.string.status_do_planed_trombo);
                                    else
                                        str = getString(R.string.status_do_trombo);
                                } else {
                                    if (isPlanned)
                                        str = getString(R.string.status_do_planed_event);
                                    else
                                        str = getString(R.string.status_do_event);
                                }

                            } else if (eventIdx == ParseEvent.ANALIZE_EVENT) {
                                if (isPlanned)
                                    str = getString(R.string.status_do_planed_analize);
                                else
                                    str = getString(R.string.status_do_analize);
                            } else if (eventIdx == ParseEvent.DENIED_EVENT) {
                                if (isPlanned)
                                    str = getString(R.string.status_do_planed_dinied);
                                else
                                    str = getString(R.string.status_do_dinied);
                            } else {
                                if (isPlanned)
                                    str = getString(R.string.status_do_planed_event);
                                else
                                    str = getString(R.string.status_do_event);
                            }
                            imageDone.setVisibility(View.GONE);
                            imageOpen.setVisibility(View.VISIBLE);
                        } else {
                            int status = event.getStatus();
                            if (eventIdx == ParseEvent.BLOOD_EVENT) {
                                int bloodEventType = event.getEventType();
                                if (bloodEventType == ParseEvent.BLOOD_EVENT_BLOOD) {
                                    if (status == ParseEvent.STATUS_NONE)
                                        if (isPlanned)
                                            str = getString(R.string.status_was_planed_blood);
                                        else
                                            str = getString(R.string.status_was_blood);
                                    else
                                        str = getString(R.string.status_blood_done);
                                } else if (bloodEventType == ParseEvent.BLOOD_EVENT_PLAZMA) {
                                    if (status == ParseEvent.STATUS_NONE)
                                        if (isPlanned)
                                            str = getString(R.string.status_was_planed_plazma);
                                        else
                                            str = getString(R.string.status_was_plazma);
                                    else
                                        str = getString(R.string.status_plazma_done);
                                } else if (bloodEventType == ParseEvent.BLOOD_EVENT_TROMBO) {
                                    if (status == ParseEvent.STATUS_NONE)
                                        if (isPlanned)
                                            str = getString(R.string.status_was_planed_trombo);
                                        else
                                            str = getString(R.string.status_was_trombo);
                                    else
                                        str = getString(R.string.status_trombo_done);
                                } else {
                                    if (status == ParseEvent.STATUS_NONE)
                                        if (isPlanned)
                                            str = getString(R.string.status_was_planed_event);
                                        else
                                            str = getString(R.string.status_was_event);
                                    else
                                        str = getString(R.string.status_event_done);
                                }
                            } else if (eventIdx == ParseEvent.DENIED_EVENT) {
                                if (status == ParseEvent.STATUS_NONE)
                                    if (isPlanned)
                                        str = getString(R.string.status_was_planed_denied);
                                    else
                                        str = getString(R.string.status_was_denied);
                                else
                                    str = getString(R.string.status_denied_done);
                            } else if (eventIdx == ParseEvent.ANALIZE_EVENT) {
                                if (status == ParseEvent.STATUS_NONE)
                                    if (isPlanned)
                                        str = getString(R.string.status_was_planed_analize);
                                    else
                                        str = getString(R.string.status_was_analize);
                                else
                                    str = getString(R.string.status_analize_done);
                            } else {
                                if (status == ParseEvent.STATUS_NONE)
                                    if (isPlanned)
                                        str = getString(R.string.status_was_planed_event);
                                    else
                                        str = getString(R.string.status_was_event);
                                else
                                    str = getString(R.string.status_event_done);
                            }

                            if (event.getStatus() == ParseEvent.STATUS_NONE) {
                                imageDone.setVisibility(View.GONE);
                                imageOpen.setVisibility(View.VISIBLE);

                            } else {
                                imageDone.setVisibility(View.VISIBLE);
                                imageOpen.setVisibility(View.GONE);
                            }
                        }

                    }
                    text.setText(str);

                }
            }
        }
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        try {
            mListener = (OnFragmentInteractionListener) activity;
            registerReceiver();
        } catch (ClassCastException e) {
            throw new ClassCastException(activity.toString()
                    + " must implement OnFragmentInteractionListener");
        }
        try {
            mPListener = (OnPossibleEventUpdates) activity;
        } catch (ClassCastException e) {
            throw new ClassCastException(activity.toString()
                    + " must implement OnPossibleEventUpdates");
        }

    }

    private void registerReceiver() {
        Activity activity = getActivity();
        if (activity != null) {
            IntentFilter filter = new IntentFilter(AppConstants.ACTION_EVENT_BROADCAST);
            activity.registerReceiver(onEventsUpdated, filter);
        }
    }

    private void unregisterReceiver() {
        Activity activity = getActivity();
        if (activity != null) {
            activity.unregisterReceiver(onEventsUpdated);
        }
    }

    private BroadcastReceiver onEventsUpdated = new BroadcastReceiver() {

        @Override
        public void onReceive(Context context, Intent intent) {
            if (intent != null && AppConstants.ACTION_EVENT_BROADCAST.equals(intent.getAction()) && mItems.size() > 0) {
                /*
                long fromDate = intent.getLongExtra(AppConstants.ARG_FROM_DATE,0);
                long toDate = intent.getLongExtra(AppConstants.ARG_NEW_DATE,0);
                long beginPeriod = DbAdapter.getDayBegin(mItems.get(0).date);
                long endPeriod = DbAdapter.getDayEnd(mItems.get(mItems.size()-1).date);
                if (fromDate>=beginPeriod&&fromDate<=endPeriod || toDate>=beginPeriod&&toDate<=endPeriod){
                    createDaysList();
                    ((DaysAdapter)mGridDays.getAdapter()).notifyDataSetChanged();
                    ((EventsAdapter)mListEvents.getAdapter()).notifyDataSetChanged();
                }
                */
                //TODO
            }
        }
    };


    @Override
    public void onDetach() {
        super.onDetach();
        unregisterReceiver();
        mListener = null;
        mPListener = null;
    }


    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putInt(CURRENT_POSITION, mCurrentPosition);
    }

    @Override
    public void onViewStateRestored(@Nullable Bundle savedInstanceState) {
        super.onViewStateRestored(savedInstanceState);
    }

    public void setlectToday() {
        mSelectToday = true;
        if (mTodayPosition >= 0) {
            mGridDays.setItemChecked(mTodayPosition, true);
            showEvents(mTodayPosition);
            mSelectToday = false;
        }
    }


}
