package com.lexadev.donorinfo.ui;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.SharedPreferences;
import android.graphics.Paint;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.text.InputType;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import com.lexadev.donorinfo.AppConstants;
import com.lexadev.donorinfo.OnFragmentInteractionListener;
import com.lexadev.donorinfo.R;
import com.lexadev.donorinfo.helper.FontsHelper;
import com.lexadev.donorinfo.parse.AppUser;
import com.parse.LogInCallback;
import com.parse.ParseException;
import com.parse.ParseUser;
import com.parse.RequestPasswordResetCallback;

import java.util.ArrayList;

public class LoginFragment extends Fragment {

    private OnFragmentInteractionListener mListener;
    private EditText mEditEmail;
    private EditText mEditPassword;
    private Button mButtonLogin;
    private TextView mTextForget;
    private ArrayList<View> mEditViews = new ArrayList<>();

    private SharedPreferences mSettings;
    private int[] mTitles = new int[]{
            R.id.text_title_email,
            R.id.text_title_password,
            R.id.text_forget_password,
    };

    public LoginFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        if (mListener!=null)
            mSettings = mListener.getSettings();
        View rootView = inflater.inflate(R.layout.fragment_login, container, false);
        if (mListener!=null)
            mListener.setupUI(rootView);
        handleViews(rootView);
        return rootView;
    }

    private void handleViews(View rootView) {
        mEditEmail = (EditText)rootView.findViewById(R.id.edit_email);
        String email = null;
        if (mSettings!=null)
            email = mSettings.getString(AppConstants.PREF_EMAIL,null);
        if (email!=null)
            mEditEmail.setText(email);
        mEditPassword = (EditText)rootView.findViewById(R.id.edit_password);
        mEditViews.add(mEditEmail);
        mEditViews.add(mEditPassword);

        for (View view:mEditViews){
            if (view instanceof EditText){
                EditText text = (EditText)view;
                text.setTypeface(FontsHelper.getFont(FontsHelper.FontName.ROBOTO, FontsHelper.FontsStyle.REGULAR));
            }
        }

        mButtonLogin = (Button)rootView.findViewById(R.id.button_login);
        mButtonLogin.setTypeface(FontsHelper.getFont(FontsHelper.FontName.ROBOTO, FontsHelper.FontsStyle.REGULAR));
        mButtonLogin.setOnClickListener(mOnLoginClick);
        mTextForget = (TextView)rootView.findViewById(R.id.text_forget_password);
        mTextForget.setPaintFlags(mTextForget.getPaintFlags()| Paint.UNDERLINE_TEXT_FLAG);
        mTextForget.setOnClickListener(mOnForgetClick);
    }

    private View.OnClickListener mOnForgetClick = new View.OnClickListener() {

        @Override
        public void onClick(View v) {
            AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
            builder.setTitle(R.string.title_forget);
            final EditText edit = new EditText(getActivity());
            edit.setInputType(InputType.TYPE_TEXT_VARIATION_EMAIL_ADDRESS);
            edit.setText(mEditEmail.getText().toString());
            builder.setView(edit);
            builder.setNegativeButton(android.R.string.cancel,null);
            builder.setPositiveButton(R.string.button_reset_password,new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    String email = edit.getText().toString();
                    if (mListener!=null)
                        mListener.startWaiting(R.string.progress_waiting,null);
                    ParseUser.requestPasswordResetInBackground(email,new RequestPasswordResetCallback() {
                        @Override
                        public void done(ParseException e) {
                            if (mListener!=null)
                                mListener.stopWaiting();
                            if (e==null){
                                if (!isAdded())
                                    return;
                                AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
                                builder.setTitle(R.string.title_reset_password);
                                builder.setMessage(R.string.message_reset_password);
                                builder.setPositiveButton(android.R.string.ok,null);
                                builder.show();
                            }
                            else{
                                if (mListener!=null)
                                   mListener.showToast(R.string.toast_failed_reset_password,e.getMessage());
                            }
                        }
                    });
                }
            });
            builder.show();

        }
    };
    private View.OnClickListener mOnLoginClick = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            if (checkFields()){
                if (mListener!=null){
                    mListener.startWaiting(R.string.progress_registering,null);
                }
                final String userName = mEditEmail.getText().toString();
                String password = mEditPassword.getText().toString();
                AppUser.logInInBackground(userName, password, new LogInCallback() {
                    @Override
                    public void done(ParseUser parseUser, ParseException e) {
                        if (!isAdded())
                            return;
                        if (mListener!=null){
                            mListener.stopWaiting();
                        }
                        if (e==null){
                            if (mSettings!=null)
                                mSettings.edit().putString(AppConstants.PREF_EMAIL,userName).commit();

                            mListener.toProfile(false);
                        }
                        else{
                            switch (e.getCode()){
                                case ParseException.INVALID_EMAIL_ADDRESS:
                                case ParseException.EMAIL_MISSING:
                                case ParseException.EMAIL_NOT_FOUND:{
                                    mEditEmail.setError(getString(R.string.error_enter_correct_email));
                                    return;
                                }
                                case ParseException.PASSWORD_MISSING:{
                                    mEditPassword.setError(getString(R.string.error_enter_correct_password));
                                    return;
                                }

                            }
                            if (mListener!=null)
                                mListener.showToast(R.string.toast_failed_login,e.getMessage()+"("+e.getCode()+")");
                        }

                    }
                });
            }
        }
    };

    private boolean checkFields() {
        boolean bRet = true;
        String email = mEditEmail.getText().toString();
        if (email.trim().length()<5||!email.contains("@")){
            bRet = false;
            mEditEmail.setError(getString(R.string.error_enter_correct_email));
        }
        String password = mEditPassword.getText().toString();
        if (password.trim().length()==0){
            bRet = false;
            mEditPassword.setError(getString(R.string.error_enter_password));
        }
        return bRet;
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        try {
            mListener = (OnFragmentInteractionListener) activity;
        } catch (ClassCastException e) {
            throw new ClassCastException(activity.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    @Override
    public void onResume() {
        super.onResume();
        if (mListener!=null)
            mListener.setTitle(R.string.title_login);
    }

}
