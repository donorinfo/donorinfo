package com.lexadev.donorinfo.ui;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.res.Resources;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;
import android.support.v4.view.ViewPager;
import android.util.SparseArray;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.TextView;

import com.lexadev.donorinfo.AppConstants;
import com.lexadev.donorinfo.OnFragmentInteractionListener;
import com.lexadev.donorinfo.R;
import com.lexadev.donorinfo.helper.FontsHelper;
import com.lexadev.donorinfo.parse.AppUser;
import com.lexadev.donorinfo.widgets.SmoothViewPager;

import java.util.Calendar;

public class CalendarFragment extends Fragment {

    private OnFragmentInteractionListener mListener;
    private SmoothViewPager mCalendarPager;
    private FrameLayout mSelectRegion;
    private TextView mTextSelectedRegion;
    private ImageView mLeftArrow;
    private ImageView mRightArrow;

    private CalendarPagerAdapter mAdapter;

    private String[] mRegionIdxArray=null;
    private String[] mRegionNames = null;

    public CalendarFragment() {
        // Required empty public constructor
    }

    public class ItemsIds{
        int frameId;
        int textId;
        int eventsId;
        public ItemsIds(String packageName,Resources res,int idx){
            frameId = res.getIdentifier("day_"+idx,"id",packageName);
            textId = res.getIdentifier("day_text_"+idx,"id",packageName);
            eventsId = res.getIdentifier("day_events_"+idx,"id",packageName);
        }
    }
    public static final ItemsIds[] ITEMS_IDS = new ItemsIds[7];
    public static String[] MONTHS_NAMES;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getActivity()!=null&&getResources()!=null){
            if (MONTHS_NAMES==null) {
                for (int i = 0; i < 7; i++) {
                    ITEMS_IDS[i] = new ItemsIds(getActivity().getPackageName(), getResources(), i + 1);
                }
                MONTHS_NAMES = getResources().getStringArray(R.array.monthes_names);
            }
            mRegionIdxArray = getResources().getStringArray(R.array.array_regions_idx);
            mRegionNames = getResources().getStringArray(R.array.array_region_names);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_calendar, container, false);
        if (mListener!=null)
            mListener.setupUI(rootView);
        handleViews(rootView);
        updateAdapter();
        return rootView;
    }

    private void handleViews(View rootView) {
        mCalendarPager = (SmoothViewPager)rootView.findViewById(R.id.pager);
        mCalendarPager.setOffscreenPageLimit(1);
        mSelectRegion = (FrameLayout)rootView.findViewById(R.id.select_region);
        mSelectRegion.setOnClickListener(mSelectRegionClick);
        mLeftArrow = (ImageView)rootView.findViewById(R.id.arrow_left);
        mLeftArrow.setOnClickListener(mLeftArrowClick);
        mRightArrow = (ImageView)rootView.findViewById(R.id.arrow_right);
        mRightArrow.setOnClickListener(mRightArrowClick);
        mTextSelectedRegion = (TextView)rootView.findViewById(R.id.text_select_region);
        mTextSelectedRegion.setTypeface(FontsHelper.getFont(FontsHelper.FontName.ROBOTO, FontsHelper.FontsStyle.REGULAR));
        if (mListener!=null){
            String idx = mListener.getSelectedRegion();
            if (idx==null&& AppUser.getCurrentUser()!=null){
                idx = AppUser.getCurrentUser().getRegion();
            }
            if (idx!=null) {
                for (int i = 0; i < mRegionIdxArray.length; i++) {
                    if (mRegionIdxArray[i].equals(idx)) {
                        String region = mRegionNames[i];
                        mTextSelectedRegion.setText(region);
                    }
                }
            }
        }

    }
    private View.OnClickListener mSelectRegionClick = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            int selectedItem = -1;
            String idx = mListener.getSelectedRegion();
            if (idx!=null) {
                for (int i = 0; i < mRegionIdxArray.length; i++) {
                    if (mRegionIdxArray[i].equals(idx)) {
                        selectedItem = i;
                    }
                }
            }
            AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
            builder.setTitle(R.string.title_select_region);
            builder.setSingleChoiceItems(mRegionNames,selectedItem,new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    dialog.dismiss();
                    String idx = null;
                    if (which>=0) {
                        idx = mRegionIdxArray[which];
                    }
                    if (mListener!=null){
                        mListener.setSelectedRegion(idx);
                    }
                    if (which>=0)
                        mTextSelectedRegion.setText(mRegionNames[which]);
                    else
                        mTextSelectedRegion.setText(R.string.text_select_region);
                    mAdapter.notifyDataSetChanged();
                }
            });
            builder.setNegativeButton(android.R.string.cancel,null);
            builder.show();
        }
    };
    private View.OnClickListener mLeftArrowClick = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            int current = mCalendarPager.getCurrentItem();
            if (current>0) {
                current--;
                mCalendarPager.setCurrentItem(current);
            }
        }
    };
    private View.OnClickListener mRightArrowClick = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            int current = mCalendarPager.getCurrentItem();
            if (current<AppConstants.MONTHS_COUNT-1) {
                current++;
                mCalendarPager.setCurrentItem(current);
            }
        }
    };

    private class CalendarPagerAdapter extends FragmentStatePagerAdapter {
        private SparseArray<MonthFragment> mapFragments = new SparseArray<MonthFragment>();
        public CalendarPagerAdapter(FragmentManager fm) {
            super(fm);
        }

        @Override
        public int getItemPosition(Object object) {
            return POSITION_NONE;
        }

        @Override
        public Fragment getItem(int num) {
            MonthFragment fragment = getFragment(num);
            if (fragment==null) {
                int todayIdx = num - AppConstants.MONTHS_COUNT / 2;
                Calendar calendar = Calendar.getInstance();
                calendar.set(Calendar.HOUR_OF_DAY, 12);
                calendar.set(Calendar.MINUTE, 0);
                calendar.set(Calendar.SECOND, 0);
                calendar.set(Calendar.MILLISECOND, 0);
                calendar.add(Calendar.MONTH, todayIdx);
                fragment = MonthFragment.newInstance(calendar.getTimeInMillis());
                mapFragments.put(num, fragment);
            }
            return fragment;
        }
        @Override
        public void destroyItem(ViewGroup container, int position, Object object) {
            super.destroyItem(container, position, object);
            mapFragments.remove(position);
        }
        @Override
        public int getCount() {
            return AppConstants.MONTHS_COUNT;
        }
        public MonthFragment getFragment(int position){
            return mapFragments.get(position);
        }

    }

    private void updateAdapter(){
        mAdapter = new CalendarPagerAdapter(getChildFragmentManager());
        mCalendarPager.setAdapter(mAdapter);
        gotoCurrentDate();
        mCalendarPager.setOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageSelected(int position) {
                updatePrevNextVisible(position);
                if (position == AppConstants.MONTHS_COUNT/2&&mSelectToday){
                    mSelectToday = false;
                    MonthFragment fragment = (MonthFragment)mAdapter.getItem(position);
                    if (fragment!=null)
                        fragment.setlectToday();
                }
            }
            @Override
            public void onPageScrolled(int arg0, float arg1, int arg2) {
            }
            @Override
            public void onPageScrollStateChanged(int arg0) {
            }
        });
    }

    private void gotoCurrentDate() {
        mCalendarPager.setCurrentItem(AppConstants.MONTHS_COUNT/2);
    }
    private void updatePrevNextVisible(int position){
        if (position==0)
            mLeftArrow.setVisibility(View.GONE);
        else
            mLeftArrow.setVisibility(View.VISIBLE);

        if (position>=AppConstants.MONTHS_COUNT)
            mRightArrow.setVisibility(View.GONE);
        else
            mRightArrow.setVisibility(View.VISIBLE);


    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        try {
            mListener = (OnFragmentInteractionListener) activity;
        } catch (ClassCastException e) {
            throw new ClassCastException(activity.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }
    @Override
    public void onResume() {
        super.onResume();
        if (mListener!=null)
            mListener.setTitle(R.string.title_page_calendar);
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        setHasOptionsMenu(true);
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        super.onCreateOptionsMenu(menu, inflater);
        inflater.inflate(R.menu.calendar_menu,menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()){
            case R.id.action_to_current:
                toToday();
                return true;
            case R.id.action_info:
                openInfo();
                return true;
            case R.id.action_list:
                openList();
                return true;


        }
        return super.onOptionsItemSelected(item);
    }

    private void openList() {
        if (mListener!=null)
            mListener.openList();
    }

    private void openInfo() {
        if (mListener!=null)
            mListener.showLegend();
    }

    private boolean mSelectToday = false;
    private void toToday() {
        mSelectToday = true;
        mCalendarPager.setCurrentItem(AppConstants.MONTHS_COUNT/2);
    }

}

