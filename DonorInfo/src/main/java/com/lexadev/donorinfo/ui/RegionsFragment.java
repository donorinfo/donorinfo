package com.lexadev.donorinfo.ui;

import android.app.Activity;
import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ListView;
import android.widget.TextView;

import com.lexadev.donorinfo.OnFragmentInteractionListener;
import com.lexadev.donorinfo.R;
import com.lexadev.donorinfo.helper.DbAdapter;
import com.lexadev.donorinfo.model.RegionsSearch;

import java.util.List;

public class RegionsFragment extends Fragment {

    private static final String ARG_SELECTION_MODE = "selection_mode";
    public static enum SelectionMode{
        STATION,
        REGION,
        NONE
    }
    private OnFragmentInteractionListener mListener;

    private ListView mListRegions;
    private TextView mTextSearch;
    private ImageButton mButtonSearch;

    private SelectionMode mSelectionMode = SelectionMode.NONE;
    public RegionsFragment() {
        // Required empty public constructor
    }


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments()!=null) {
            String selectionMode = getArguments().getString(ARG_SELECTION_MODE);
            if (selectionMode==null)
                selectionMode = SelectionMode.NONE.name();
            mSelectionMode = SelectionMode.valueOf(selectionMode);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_regions, container, false);
        if (mListener!=null)
            mListener.setupUI(rootView);
        handleViews(rootView);
        updateList("");
        return rootView;
    }

    private void handleViews(View rootView) {
        mListRegions = (ListView)rootView.findViewById(R.id.list_regions);
        mTextSearch = (EditText)rootView.findViewById(R.id.edit_search);
        mButtonSearch = (ImageButton)rootView.findViewById(R.id.button_search);
        mTextSearch.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {}

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {}

            @Override
            public void afterTextChanged(Editable s) {
                updateList(s.toString());
            }
        });
        mListRegions.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                RegionsSearch item = (RegionsSearch)mListRegions.getItemAtPosition(position);
                if (mListener!=null){
                    mListener.regionSelected(item.region_id,item.city_id,mSelectionMode);
                }
            }
        });
    }

    private void updateList(String s) {
        List<RegionsSearch> items = DbAdapter.getInstance(getActivity()).getRegionsSearch(s,mSelectionMode!=SelectionMode.REGION);
        items.add(0,new RegionsSearch(getString(R.string.item_all_regions),null,null,null));
        RegionsAdapter adapter = new RegionsAdapter(getActivity(),items);
        mListRegions.setAdapter(adapter);
    }

    public static RegionsFragment newInstance(SelectionMode selectionMode) {
        RegionsFragment fragment = new RegionsFragment();
        Bundle bundle = new Bundle();
        bundle.putString(ARG_SELECTION_MODE,selectionMode.name());
        fragment.setArguments(bundle);
        return fragment;
    }

    private static class RegionsAdapter extends ArrayAdapter<RegionsSearch>{

        private List<RegionsSearch> mItems;
        private LayoutInflater mInflater;

        public RegionsAdapter(Context context,List<RegionsSearch> items) {
            super(context, 0, items);
            mInflater = LayoutInflater.from(context);
            mItems = items;
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {
            ViewsHolder holder = null;
            if (convertView==null){
                convertView = mInflater.inflate(R.layout.regions_search_item,parent,false);
                holder = new ViewsHolder(convertView);
            }
            else{
                holder = (ViewsHolder)convertView.getTag();
            }
            holder.build(position);
            return convertView;
        }
        private class ViewsHolder{
            private TextView region;
            private TextView city;
            public ViewsHolder(View rootView){
                region = (TextView)rootView.findViewById(R.id.region);
                city = (TextView)rootView.findViewById(R.id.city);
                rootView.setTag(this);
            }
            public void build(int position){
                RegionsSearch item = getItem(position);
                if (item.city==null){
                    city.setVisibility(View.GONE);
                    region.setVisibility(View.VISIBLE);
                    region.setText(item.region);
                }
                else{
                    city.setVisibility(View.VISIBLE);
                    region.setVisibility(View.GONE);
                    city.setText(item.city);
                }
            }
        }
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        try {
            mListener = (OnFragmentInteractionListener) activity;
        } catch (ClassCastException e) {
            throw new ClassCastException(activity.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }


    @Override
    public void onResume() {
        super.onResume();
        if (mListener!=null)
            mListener.setTitle(R.string.title_fragment_regions);

    }
}
