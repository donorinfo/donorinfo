package com.lexadev.donorinfo.ui;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.DatePickerDialog;
import android.app.TimePickerDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.text.format.DateFormat;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.TimePicker;

import com.lexadev.donorinfo.OnFragmentInteractionListener;
import com.lexadev.donorinfo.R;
import com.lexadev.donorinfo.helper.DbAdapter;
import com.lexadev.donorinfo.helper.FontsHelper;
import com.lexadev.donorinfo.helper.Utils;
import com.lexadev.donorinfo.model.ParseEvent;
import com.lexadev.donorinfo.model.RegionObject;
import com.parse.DeleteCallback;
import com.parse.GetCallback;
import com.parse.ParseException;
import com.parse.ParseQuery;
import com.parse.SaveCallback;

import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.List;

public class NewRegionEventFragment extends Fragment {
    private static final String ARG_EVENT = "event";
    private String mEventId = null;


    private TextView mTextDate;
    private Spinner mSpinnerRegion;
    private EditText mEditNote;


    private long mDate = 0;

    private ParseEvent mParseEvent = null;

    private boolean mIsNewEvent = true;
    private String[] mRegionIdxArray;

    private int[] mTitles = new int[]{
            R.id.text_title_date,
            R.id.text_title_place,
            R.id.text_title_note
    };

    private List<RegionObject> mRegions;

    private OnFragmentInteractionListener mListener;

    public static NewRegionEventFragment newInstance(ParseEvent event) {
        NewRegionEventFragment fragment = new NewRegionEventFragment();
        Bundle args = new Bundle();
        if (event!=null)
            args.putString(ARG_EVENT,event.getObjectId());
        fragment.setArguments(args);
        return fragment;
    }

    public NewRegionEventFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mDate = System.currentTimeMillis();
        if (getArguments() != null) {
            mEventId = getArguments().getString(ARG_EVENT);
        }
        if (getResources() != null)
            mRegionIdxArray = getResources().getStringArray(R.array.array_regions_idx);

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_new_region_event, container, false);
        handleViews(rootView);
        if (mListener!=null)
            mListener.setupUI(rootView);
        handleViews(rootView);
        if (mParseEvent==null && mEventId != null) {
            ParseQuery<ParseEvent> query = ParseEvent.getEventQuery();
            if (mListener != null) {
                mListener.startWaiting(R.string.progress_loading, null);
            }
            query.getInBackground(mEventId, new GetCallback<ParseEvent>() {
                @Override
                public void done(ParseEvent parseEvent, ParseException e) {
                    if (mListener != null)
                        mListener.stopWaiting();
                    if (e == null) {
                        mParseEvent = parseEvent;
                        Date date = mParseEvent.getDate();
                        if (date != null)
                            mDate = date.getTime();
                    } else {
                        if (mListener != null)
                            mListener.showToast(R.string.toast_failed_load_event, e.getMessage());
                    }
                    fillViews();
                }
            });
        } else {
            fillViews();
        }

        return rootView;
    }

    private void fillViews() {
        if (mParseEvent == null) {
            mParseEvent = new ParseEvent();
        } else {
            mEditNote.setText(mParseEvent.getNote() != null ? mParseEvent.getNote() : "");
            mIsNewEvent = false;
            String region = mParseEvent.getRegion();
            if (region!=null) {
                for (int i=0;i<mRegions.size();i++){
                    if (mRegions.get(i).id.equals(region)){
                        mSpinnerRegion.setSelection(i);
                    }
                }
            }
        }

        mTextDate.setText(Utils.getDateString(getActivity(), mDate));
    }




    private void handleViews(View rootView) {
        mTextDate = (TextView) rootView.findViewById(R.id.text_date);
        mTextDate.setTypeface(FontsHelper.getFont(FontsHelper.FontName.ROBOTO, FontsHelper.FontsStyle.REGULAR));
        mTextDate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showDateTimePicker();
            }
        });

        mEditNote = (EditText) rootView.findViewById(R.id.edit_note);
        mEditNote.setTypeface(FontsHelper.getFont(FontsHelper.FontName.ROBOTO, FontsHelper.FontsStyle.REGULAR));

        for (int i=0;i<mTitles.length;i++){
            TextView text = (TextView)rootView.findViewById(mTitles[i]);
            if (text!=null)
                text.setTypeface(FontsHelper.getFont(FontsHelper.FontName.ROBOTO, FontsHelper.FontsStyle.MEDIUM));
        }
        mSpinnerRegion = (Spinner)rootView.findViewById(R.id.spinner_region);
        mRegions = DbAdapter.getInstance(getActivity()).getRegions();
        RegionSpinnerAdapter adapter = new RegionSpinnerAdapter(getActivity(),mRegions);
        mSpinnerRegion.setAdapter(adapter);


    }

    private boolean mIsDlgCanceled = false;

    public class IBSDatePickerDialog extends DatePickerDialog {

        public IBSDatePickerDialog(final Context context, final OnDateSetListener callBack, final int year, final int monthOfYear, final int dayOfMonth) {
            super(context, callBack, year, monthOfYear, dayOfMonth);
        }

        public IBSDatePickerDialog(final Context context, final int theme, final OnDateSetListener callBack, final int year, final int monthOfYear, final int dayOfMonth) {
            super(context, theme, callBack, year, monthOfYear, dayOfMonth);
        }

        @Override
        public void onClick(final DialogInterface dialog, final int which) {
            // Prevent calling onDateSet handler when clicking to dialog buttons other, then "OK"
            if (which == DialogInterface.BUTTON_POSITIVE)
                super.onClick(dialog, which);
        }

        @Override
        protected void onStop() {
            // prevent calling onDateSet handler when stopping dialog for whatever reason (because this includes
            // closing by BACK button or by tapping outside dialog, this is exactly what we try to avoid)

            //super.onStop();
        }
    }

    private void showDateTimePicker() {
        final Calendar calendar = GregorianCalendar.getInstance();
        calendar.setTimeInMillis(mDate);
        IBSDatePickerDialog dlg = new IBSDatePickerDialog(getActivity(), new DatePickerDialog.OnDateSetListener() {
            @Override
            public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {
                if (!mIsDlgCanceled) {
                    calendar.set(Calendar.YEAR, year);
                    calendar.set(Calendar.MONTH, monthOfYear);
                    calendar.set(Calendar.DAY_OF_MONTH, dayOfMonth);
                    TimePickerDialog dlgTime = new TimePickerDialog(getActivity(), new TimePickerDialog.OnTimeSetListener() {
                        @Override
                        public void onTimeSet(TimePicker view, int hourOfDay, int minute) {
                            if (!mIsDlgCanceled) {
                                calendar.set(Calendar.HOUR_OF_DAY, hourOfDay);
                                calendar.set(Calendar.MINUTE, minute);
                                calendar.set(Calendar.SECOND, 0);
                                calendar.set(Calendar.MILLISECOND, 0);
                                mDate = calendar.getTimeInMillis();
                                mTextDate.setText(Utils.getDateString(getActivity(), mDate));
                            }
                        }
                    }, calendar.get(Calendar.HOUR_OF_DAY), calendar.get(Calendar.MINUTE), DateFormat.is24HourFormat(getActivity()));
                    dlgTime.setOnCancelListener(new DialogInterface.OnCancelListener() {
                        @Override
                        public void onCancel(DialogInterface dialog) {
                            mIsDlgCanceled = true;
                        }
                    });
                    mIsDlgCanceled = false;
                    dlgTime.show();
                }
            }
        }, calendar.get(Calendar.YEAR), calendar.get(Calendar.MONTH), calendar.get(Calendar.DAY_OF_MONTH));
        dlg.setOnCancelListener(new DialogInterface.OnCancelListener() {
            @Override
            public void onCancel(DialogInterface dialog) {
                mIsDlgCanceled = true;
            }
        });
        mIsDlgCanceled = false;
        dlg.show();
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        super.onCreateOptionsMenu(menu, inflater);
        inflater.inflate(R.menu.new_region_event_menu, menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.action_save:
                save();
                return true;
            case R.id.action_delete:
                delete();
                return true;

        }
        return super.onOptionsItemSelected(item);
    }

    private void delete() {
        if (mParseEvent!=null && mParseEvent.getObjectId()!=null){
            mParseEvent.deleteInBackground(new DeleteCallback() {
                @Override
                public void done(ParseException e) {
                    if (e==null){
                        if (mListener!=null) {
                            mListener.back();
                        }

                    }
                    else{
                        //TODO error
                    }
                }
            });
        }
        else{
            if (mListener!=null)
                mListener.back();
        }
    }

    private void save() {

        saveFieldsToObject();
        mParseEvent.setManualSave(true);
        if (mListener != null) {
            mListener.startWaiting(R.string.progress_saving, null);
        }

        mParseEvent.saveInBackground(new SaveCallback() {
            @Override
            public void done(ParseException e) {

                if (e == null) {
                    if (mListener != null) {
                        mListener.stopWaiting();
                    }

                    if (mListener != null)
                        mListener.back();

                } else {
                    if (mListener != null)
                        mListener.stopWaiting();

                    if (getActivity() != null) {
                        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
                        builder.setTitle(R.string.title_cant_save);
                        builder.setMessage(R.string.message_failed_save_event);
                        builder.setPositiveButton(android.R.string.ok, null);
                        builder.show();
                    }

                }
            }
        });
    }

    private class RegionSpinnerAdapter extends ArrayAdapter<RegionObject> {

        private LayoutInflater mInflater;
        public RegionSpinnerAdapter(Context context, List<RegionObject> objects) {
            super(context, 0, objects);
            mInflater = LayoutInflater.from(context);
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {
            ViewsHolder holder;
            if (convertView==null){
                convertView = mInflater.inflate(R.layout.simple_spinner_item,parent,false);
                holder = new ViewsHolder(convertView);
            }
            else{
                holder = (ViewsHolder)convertView.getTag();
            }
            holder.build(position);
            return convertView;
        }

        @Override
        public View getDropDownView(int position, View convertView, ViewGroup parent) {
            ViewsHolder holder;
            if (convertView==null){
                convertView = mInflater.inflate(R.layout.simple_spinner_dropdown_item,parent,false);
                holder = new ViewsHolder(convertView);
            }
            else{
                holder = (ViewsHolder)convertView.getTag();
            }
            holder.build(position);
            return convertView;

        }

        private class ViewsHolder{
            TextView text;
            public ViewsHolder(View convertView){
                text = (TextView)convertView.findViewById(android.R.id.text1);
                text.setTypeface(FontsHelper.getFont(FontsHelper.FontName.ROBOTO, FontsHelper.FontsStyle.REGULAR));
                convertView.setTag(this);
            }
            public void build(int position){
                RegionObject item = getItem(position);
                text.setText(Utils.notNull(item.name));
            }
        }
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        setHasOptionsMenu(true);
    }


    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        try {
            mListener = (OnFragmentInteractionListener) activity;
        } catch (ClassCastException e) {
            throw new ClassCastException(activity.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }
    private void saveFieldsToObject() {
        mParseEvent.setDate(new Date(mDate));
        RegionObject region = (RegionObject)mSpinnerRegion.getSelectedItem();
        mParseEvent.setRegion(region.id);
        mParseEvent.setEvent(ParseEvent.REGION_EVENT);
        mParseEvent.setNote(mEditNote.getText().toString());
    }

    @Override
    public void onResume() {
        super.onResume();
        if (mListener != null)
            mListener.setTitle(R.string.title_region_event);

    }

}
