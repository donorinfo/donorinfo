package com.lexadev.donorinfo.ui;

import android.app.Activity;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.lexadev.donorinfo.OnFragmentInteractionListener;
import com.lexadev.donorinfo.R;
import com.lexadev.donorinfo.helper.FontsHelper;
import com.lexadev.donorinfo.helper.InfoFragments;

public class InfoFragment extends Fragment {

    private OnFragmentInteractionListener mListener;
    private TextView mTextAbout;
    private TextView mTextDenied;
    private TextView mTextBefore;

    public InfoFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View rootView =inflater.inflate(R.layout.fragment_info, container, false);
        handleViews(rootView);
        return rootView;
    }

    private void handleViews(View rootView) {
        mTextDenied = (TextView)rootView.findViewById(R.id.info_denied);
        mTextDenied.setTypeface(FontsHelper.getFont(FontsHelper.FontName.ROBOTO, FontsHelper.FontsStyle.BOLD));
        mTextDenied.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                gotoInfo(InfoFragments.DENIED);
            }
        });
        mTextAbout = (TextView)rootView.findViewById(R.id.info_info);
        mTextAbout.setTypeface(FontsHelper.getFont(FontsHelper.FontName.ROBOTO, FontsHelper.FontsStyle.BOLD));
        mTextAbout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                gotoInfo(InfoFragments.ABOUT);
            }
        });
        mTextBefore = (TextView)rootView.findViewById(R.id.info_before);
        mTextBefore.setTypeface(FontsHelper.getFont(FontsHelper.FontName.ROBOTO, FontsHelper.FontsStyle.BOLD));
        mTextBefore.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                gotoInfo(InfoFragments.BEFORE);
            }
        });
    }

    private void gotoInfo(InfoFragments fragment) {
        if (mListener!=null)
            mListener.toInfo(fragment);
    }


    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        try {
            mListener = (OnFragmentInteractionListener) activity;
        } catch (ClassCastException e) {
            throw new ClassCastException(activity.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    @Override
    public void onResume() {
        super.onResume();
        if (mListener!=null)
            mListener.setTitle(R.string.title_fragment_info);

    }

}
