package com.lexadev.donorinfo.ui;

import android.app.Activity;
import android.content.Context;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;

import com.lexadev.donorinfo.AppConstants;
import com.lexadev.donorinfo.OnFragmentInteractionListener;
import com.lexadev.donorinfo.R;
import com.lexadev.donorinfo.helper.FontsHelper;
import com.lexadev.donorinfo.parse.AppUser;
import com.parse.ParseException;
import com.parse.SignUpCallback;

import java.util.ArrayList;

public class RegistrationFragment extends Fragment {


    private EditText mEditEmail;
    private EditText mEditFirstName;
    private EditText mEditLastName;
    private EditText mEditPassword;
    private EditText mEditConfirmation;
    private Spinner mSpinnerBlood;
    private CheckBox mAgreed;
    private Button mButtonRegistration;

    private ArrayList<View> mEditViews = new ArrayList<>();
    private int[] mTitles = new int[]{
            R.id.text_title_email,
            R.id.text_title_password,
            R.id.text_title_password_c,
            R.id.text_title_first_name,
            R.id.text_title_last_name,
            R.id.text_title_phone_number,
            R.id.text_title_blood,
            R.id.text_title_text_agreed
    };

    private OnFragmentInteractionListener mListener;


    public RegistrationFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View rootView=inflater.inflate(R.layout.fragment_registration, container, false);
        if (mListener!=null)
            mListener.setupUI(rootView);
        handleViews(rootView);
        return rootView;
    }

    private void handleViews(View rootView) {
        mEditEmail = (EditText)rootView.findViewById(R.id.edit_email);
        mEditPassword = (EditText)rootView.findViewById(R.id.edit_password);
        mEditConfirmation = (EditText)rootView.findViewById(R.id.edit_password_c);
        mEditFirstName = (EditText)rootView.findViewById(R.id.edit_first_name);
        mEditLastName = (EditText)rootView.findViewById(R.id.edit_last_name);
        mSpinnerBlood = (Spinner)rootView.findViewById(R.id.spinner_blood);
        mEditViews.add(mEditEmail);
        mEditViews.add(mEditPassword);
        mEditViews.add(mEditConfirmation);
        mEditViews.add(mEditFirstName);
        mEditViews.add(mEditLastName);
        mEditViews.add(mSpinnerBlood);

        for (View view:mEditViews){
            if (view instanceof EditText){
                EditText text = (EditText)view;
                text.setTypeface(FontsHelper.getFont(FontsHelper.FontName.ROBOTO, FontsHelper.FontsStyle.REGULAR));
            }
        }

        CustomSpinnerAdapter adapterBlood = new CustomSpinnerAdapter(getActivity(),R.layout.simple_spinner_item,getResources().getStringArray(R.array.array_blood));
        adapterBlood.setDropDownViewResource(R.layout.simple_spinner_dropdown_item);
        mSpinnerBlood.setAdapter(adapterBlood);
        for (int i=0;i<mTitles.length;i++){
            TextView text = (TextView)rootView.findViewById(mTitles[i]);
            if (text!=null)
                text.setTypeface(FontsHelper.getFont(FontsHelper.FontName.ROBOTO, FontsHelper.FontsStyle.MEDIUM));
        }
        mButtonRegistration = (Button)rootView.findViewById(R.id.button_registration);
        mButtonRegistration.setTypeface(FontsHelper.getFont(FontsHelper.FontName.ROBOTO, FontsHelper.FontsStyle.REGULAR));
        mButtonRegistration.setOnClickListener(mOnRegistrationClick);
        mAgreed = (CheckBox)rootView.findViewById(R.id.checkbox_agreed);
        mAgreed.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                mButtonRegistration.setEnabled(isChecked);
            }
        });
        mButtonRegistration.setEnabled(mAgreed.isChecked());
    }
    private View.OnClickListener mOnRegistrationClick = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            if (checkFields()){
                if (mListener!=null){
                    mListener.startWaiting(R.string.progress_registering,null);
                }
                AppUser user = new AppUser();
                user.setUsername(mEditEmail.getText().toString());
                user.setPassword(mEditPassword.getText().toString());
                user.setEmail(mEditEmail.getText().toString());
                user.setFirstName(mEditFirstName.getText().toString());
                user.setLastName(mEditLastName.getText().toString());
                user.setBlood((String)mSpinnerBlood.getSelectedItem());
                user.signUpInBackground(new SignUpCallback() {
                    @Override
                    public void done(ParseException e) {
                        if (!isAdded())
                            return;
                        if (mListener!=null){
                            mListener.stopWaiting();
                        }
                        if (e==null){
                            if (mListener!=null) {
                                SharedPreferences settings = mListener.getSettings();
                                if (settings != null)
                                    settings.edit().putString(AppConstants.PREF_EMAIL, mEditEmail.getText().toString()).commit();
                            }
                            mListener.toProfile(false);
                        }
                        else{
                            switch (e.getCode()){
                                case ParseException.INVALID_EMAIL_ADDRESS:{
                                    mEditEmail.setError(getString(R.string.error_enter_correct_email));
                                    return;
                                }
                                case ParseException.EMAIL_TAKEN:{
                                    mEditEmail.setError(getString(R.string.error_account_already_exist));
                                    return;
                                }
                            }
                            if (mListener!=null)
                                mListener.showToast(R.string.toast_failed_signup,e.getMessage());
                        }
                    }
                });
            }
        }
    };

    private boolean checkFields() {
        boolean bRet = true;
        String email = mEditEmail.getText().toString();
        if (email.trim().length()<5||!email.contains("@")){
            bRet = false;
            mEditEmail.setError(getString(R.string.error_enter_correct_email));
        }
        String password = mEditPassword.getText().toString();
        String passwordC = mEditConfirmation.getText().toString();
        if (password.trim().length()==0||!password.equals(passwordC)){
            bRet = false;
            mEditPassword.setError(getString(R.string.error_enter_password));
            mEditConfirmation.setError(getString(R.string.error_enter_password));
        }
        return bRet;
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        try {
            mListener = (OnFragmentInteractionListener) activity;
        } catch (ClassCastException e) {
            throw new ClassCastException(activity.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    @Override
    public void onResume() {
        super.onResume();
        if (mListener!=null)
            mListener.setTitle(R.string.title_registration);
    }
    private class CustomSpinnerAdapter extends ArrayAdapter<String> {

        public CustomSpinnerAdapter(Context context, int resource, String[] objects) {
            super(context, resource, objects);
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {
            View rootView = super.getView(position, convertView, parent);
            TextView text = (TextView)rootView.findViewById(android.R.id.text1);
            if (text!=null){
                text.setTypeface(FontsHelper.getFont(FontsHelper.FontName.ROBOTO, FontsHelper.FontsStyle.REGULAR));
            }
            return rootView;
        }
    }
}
