package com.lexadev.donorinfo.ui;

import android.app.Activity;
import android.content.Context;
import android.content.DialogInterface;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.FrameLayout;
import android.widget.ListView;
import android.widget.TextView;

import com.lexadev.donorinfo.AppConstants;
import com.lexadev.donorinfo.OnFragmentInteractionListener;
import com.lexadev.donorinfo.R;
import com.lexadev.donorinfo.helper.DbAdapter;
import com.lexadev.donorinfo.helper.FontsHelper;
import com.lexadev.donorinfo.helper.LoadingProgressDialog;
import com.lexadev.donorinfo.model.ParseEvent;
import com.parse.FindCallback;
import com.parse.ParseException;
import com.parse.ParseQuery;

import java.text.DateFormat;
import java.util.List;

public class RegionNewsFragment extends Fragment {

    private static final String PROGRESS = "progress";
    private static final String LIST = "list";

    private FrameLayout mLayoutNext;
    private FrameLayout mLayoutPrev;
    private ListView mList;

    private OnFragmentInteractionListener mListener;
    private EventsAdapter mAdapter = null;

    private int mProgress = 0;
    private int mPreviousProgress = -1;
    //private boolean mIsLoaded = false;

    public RegionNewsFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (savedInstanceState != null)
            mProgress = savedInstanceState.getInt(PROGRESS);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        mPreviousProgress = -1;
        View rootView = inflater.inflate(R.layout.fragment_region_news, container, false);
        if (mListener != null)
            mListener.setupUI(rootView);
        handleViews(rootView);
        if (savedInstanceState != null)
            if (savedInstanceState.containsKey(LIST))
                mList.onRestoreInstanceState(savedInstanceState.getParcelable(LIST));
        loadNews();
        return rootView;
    }

    private void loadNews() {
        final ParseQuery<ParseEvent> query = ParseEvent.getRegionsEvents();
        if (mListener != null)
            mListener.startWaiting(R.string.progress_loading, new LoadingProgressDialog.OnCancelListener() {
                @Override
                public void onCancel(DialogInterface dialog) {

                    new AsyncTask<Void, Void, Void>() {
                        @Override
                        protected Void doInBackground(Void... voids) {
                            query.cancel();
                            return null;
                        }
                    };
                }
            });
        query.setSkip(mProgress);
        query.setLimit(AppConstants.REGION_NEWS_PER_PAGE);
        query.findInBackground(new FindCallback<ParseEvent>() {
            @Override
            public void done(List<ParseEvent> parseEvents, ParseException e) {
                if (mListener != null)
                    mListener.stopWaiting();
                if (e == null) {
                    if ((parseEvents == null || parseEvents.size() == 0) && mPreviousProgress >= 0) {
                        mProgress = mPreviousProgress;
                    } else {
                        mAdapter = new EventsAdapter(getActivity(), parseEvents);
                        mList.setAdapter(mAdapter);
                    }
                } else {
                    //TODO
                }

            }
        });
    }

    private static class EventsAdapter extends ArrayAdapter<ParseEvent> {
        private LayoutInflater mInflater;
        private boolean mIsAdamin;

        public EventsAdapter(Context context, List<ParseEvent> objects) {
            super(context, 0, objects);
            mInflater = LayoutInflater.from(context);
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {
            ViewsHolder holder;
            if (convertView == null) {
                convertView = mInflater.inflate(R.layout.layout_region_event, parent, false);
                holder = new ViewsHolder(convertView);
            } else {
                holder = (ViewsHolder) convertView.getTag();
            }
            holder.build(position);
            return convertView;
        }

        private class ViewsHolder {
            private TextView date;
            private TextView region;
            private TextView text;

            public ViewsHolder(View rootView) {
                date = (TextView) rootView.findViewById(R.id.date);
                date.setTypeface(FontsHelper.getFont(FontsHelper.FontName.ROBOTO, FontsHelper.FontsStyle.BOLD));
                region = (TextView) rootView.findViewById(R.id.region);
                region.setTypeface(FontsHelper.getFont(FontsHelper.FontName.ROBOTO, FontsHelper.FontsStyle.BOLD));
                text = (TextView) rootView.findViewById(R.id.text);
                text.setTypeface(FontsHelper.getFont(FontsHelper.FontName.ROBOTO, FontsHelper.FontsStyle.REGULAR));
                rootView.setTag(this);
            }

            public void build(int position) {
                ParseEvent event = getItem(position);
                DateFormat format = DateFormat.getInstance();
                date.setText(format.format(event.getDate()));
                String regionStr = event.getRegion();
                region.setText(DbAdapter.getInstance(getContext()).getRegionName(regionStr));
                text.setText(event.getNote());
            }
        }
    }


    private void handleViews(View rootView) {
        mLayoutPrev = (FrameLayout) rootView.findViewById(R.id.layout_prev);
        mLayoutPrev.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (mProgress > 0)
                    mProgress -= AppConstants.REGION_NEWS_PER_PAGE;
                if (mProgress < 0)
                    mProgress = 0;
                loadNews();
            }
        });

        mLayoutNext = (FrameLayout) rootView.findViewById(R.id.layout_next);
        mLayoutNext.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                int items = mList.getCount();
                if (items > 0) {
                    mPreviousProgress = mProgress;
                    mProgress += items;
                    loadNews();
                }
            }
        });
        mList = (ListView) rootView.findViewById(R.id.list_events);
        mList.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                ParseEvent event = (ParseEvent) mList.getItemAtPosition(position);
                if (mListener != null)
                    mListener.viewRegionEvent(event);
            }
        });
        if (mAdapter != null)
            mList.setAdapter(mAdapter);
    }


    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        try {
            mListener = (OnFragmentInteractionListener) activity;
        } catch (ClassCastException e) {
            throw new ClassCastException(activity.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        setHasOptionsMenu(true);
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        super.onCreateOptionsMenu(menu, inflater);
        inflater.inflate(R.menu.region_news_menu,menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()){
            case R.id.action_new_event:
                if (mListener!=null) {
                    mListener.viewRegionEvent(null);
                    return true;
                }
        }
        return super.onOptionsItemSelected(item);
    }



    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    @Override
    public void onResume() {
        super.onResume();
        if (mListener != null)
            mListener.setTitle(R.string.title_fragment_region_news);

    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putInt(PROGRESS, mProgress);
        outState.putParcelable(LIST, mList.onSaveInstanceState());
    }
}