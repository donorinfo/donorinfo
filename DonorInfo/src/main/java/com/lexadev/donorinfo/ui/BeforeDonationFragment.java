package com.lexadev.donorinfo.ui;

import android.app.Activity;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.lexadev.donorinfo.OnFragmentInteractionListener;
import com.lexadev.donorinfo.R;
import com.lexadev.donorinfo.helper.FontsHelper;

public class BeforeDonationFragment extends Fragment {

    private int[] mTextsTitle = new int[]{
            R.id.text_title
    };
    private int[] mTextsDescription = new int[]{
            R.id.text_1,
            R.id.text_2,
            R.id.text_3,
            R.id.text_4,
            R.id.text_5,
            R.id.text_6,
            R.id.text_7,
            R.id.text_8,
            R.id.text_9,
    };

    private OnFragmentInteractionListener mListener;

    public BeforeDonationFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_before_donation, container, false);
        setTypefaces(rootView);
        return rootView;
    }

    private void setTypefaces(View rootView){
        for (int id:mTextsTitle){
            try {
                TextView view = (TextView) rootView.findViewById(id);
                view.setTypeface(FontsHelper.getFont(FontsHelper.FontName.ROBOTO, FontsHelper.FontsStyle.BOLD));
            }catch(ClassCastException e){
                e.printStackTrace();
            }
        }
        for (int id:mTextsDescription){
            try {
                TextView view = (TextView) rootView.findViewById(id);
                view.setTypeface(FontsHelper.getFont(FontsHelper.FontName.ROBOTO, FontsHelper.FontsStyle.REGULAR));
            }catch(ClassCastException e){
                e.printStackTrace();
            }
        }

    }
    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        try {
            mListener = (OnFragmentInteractionListener) activity;
        } catch (ClassCastException e) {
            throw new ClassCastException(activity.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    @Override
    public void onResume() {
        super.onResume();
        if (mListener!=null)
            mListener.setTitle(R.string.title_fragment_before_donation);
    }
}
