package com.lexadev.donorinfo.ui;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.DatePickerDialog;
import android.app.TimePickerDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Paint;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.text.format.DateFormat;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.TimePicker;
import android.widget.Toast;

import com.lexadev.donorinfo.AppConstants;
import com.lexadev.donorinfo.OnFragmentInteractionListener;
import com.lexadev.donorinfo.OnPossibleEventUpdates;
import com.lexadev.donorinfo.R;
import com.lexadev.donorinfo.helper.Contras;
import com.lexadev.donorinfo.helper.DbAdapter;
import com.lexadev.donorinfo.helper.FontsHelper;
import com.lexadev.donorinfo.helper.FragmentState;
import com.lexadev.donorinfo.helper.Utils;
import com.lexadev.donorinfo.model.ParseEvent;
import com.lexadev.donorinfo.model.Station;
import com.lexadev.donorinfo.parse.AppUser;
import com.parse.DeleteCallback;
import com.parse.FindCallback;
import com.parse.GetCallback;
import com.parse.ParseException;
import com.parse.ParseQuery;
import com.parse.SaveCallback;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.List;

public class NewEventFragment extends Fragment {
    public static final String TAG="NewEventFragment";

    private static final String ARG_DATE = "date";
    private static final String ARG_EVENT = "event";

    private Spinner mSpinnerEvent;
    private Spinner mSpinnerEventType;
    private TextView mTextEentTypeTitle;
    private TextView mTextDate;
    private TextView mTextPlace;
    private Spinner mSpinnerRemind;
    private EditText mEditNote;
    private TextView mTextEvent;
    private TextView mTextEventType;
    private TextView mTextRemind;
    private TextView mTextNote;
    private FrameLayout mFrameDo;
    private FrameLayout mFrameDone;
    private Button mButtonDo;
    private Button mButtonDone;

    private View mRootView = null;
    private CustomSpinnerAdapter mBloodAdapter;
    private CustomSpinner3RowAdapter mDeniedAdapter;
    private long mDate = 0;
    private String mEventId = null;
    private boolean mIsStatusUpdated = false;

    private int mStatus = ParseEvent.STATUS_NONE;
    private List<Contras.Contra> mTemporaryContras;
    private ParseEvent mParseEvent = null;

    private String mStationId = null;
    private long mDateOnOpen = 0;
    private boolean mIsSelectStation = false;
    private boolean mIsNewEvent = true;
    private OnFragmentInteractionListener mListener;
    private OnPossibleEventUpdates mPListener;
    private String[] mRegionIdxArray;
    private boolean mInited=true;
    private FragmentState mFragmentState = FragmentState.VIEW;

    private int[] mTitles = new int[]{
            R.id.text_title_event,
            R.id.text_title_event_type,
            R.id.text_title_date,
            R.id.text_title_place,
            R.id.text_title_remind,
            R.id.text_title_note
    };
    private String[] mBloodEventsType;
    private String[] mBloodEvents;
    private String[] mReminds;
    private boolean mIsEnableChangeStatus = true;

    public static NewEventFragment newInstance(long date) {
        NewEventFragment fragment = new NewEventFragment();
        Bundle args = new Bundle();
        args.putLong(ARG_DATE, date);
        fragment.setArguments(args);
        return fragment;
    }

    public static NewEventFragment newInstance(ParseEvent event) {
        NewEventFragment fragment = new NewEventFragment();
        if (event != null) {
            Bundle args = new Bundle();
            args.putString(ARG_EVENT, event.getObjectId());
            fragment.setArguments(args);
        }
        return fragment;
    }

    public NewEventFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setRetainInstance(true);
        mBloodEventsType = getResources().getStringArray(R.array.array_events_type);
        mBloodEvents = getResources().getStringArray(R.array.array_events);
        mReminds = getResources().getStringArray(R.array.array_remind);
        mTemporaryContras = Contras.newInstance(getActivity()).getContrasList(Contras.Type.TEMPORARY);
        if (getArguments() != null) {
            mDate = getArguments().getLong(ARG_DATE);
            mEventId = getArguments().getString(ARG_EVENT);
            mDateOnOpen = mDate;
        }
        if (getResources() != null)
            mRegionIdxArray = getResources().getStringArray(R.array.array_regions_idx);

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        if (mIsSelectStation) {
            if (mListener!=null){
                String stationId = mListener.getSelectedStationId();
                if (stationId!=null){
                    mStationId = stationId;
                }
            }
        }
        mRootView = inflater.inflate(R.layout.fragment_new_event, container, false);
        if (mListener!=null)
            mListener.setupUI(mRootView);
        handleViews(mRootView);
        if (mParseEvent==null && mEventId != null) {
            mFragmentState = FragmentState.VIEW;
            ParseQuery<ParseEvent> query = ParseEvent.getEventQuery();
            if (mListener != null) {
                mListener.startWaiting(R.string.progress_loading, null);
            }
            query.getInBackground(mEventId, new GetCallback<ParseEvent>() {
                @Override
                public void done(ParseEvent parseEvent, ParseException e) {
                    if (mListener != null)
                        mListener.stopWaiting();
                    if (e == null) {
                        mParseEvent = parseEvent;
                        Date date = mParseEvent.getDate();
                        if (date != null)
                            mDate = date.getTime();
                    } else {
                        if (mListener != null)
                            mListener.showToast(R.string.toast_failed_load_event, e.getMessage());
                    }
                    fillViews();
                }
            });
        } else {
            mFragmentState = FragmentState.EDIT;
            fillViews();
        }
        return mRootView;
    }


    private void fillViews() {
        if (mParseEvent == null) {
            if (mListener != null) {
                if (mListener!=null){
                    mStationId = mListener.getSettings().getString(AppConstants.PREF_DEFAULT_STATION,null);
                }
            }
            mParseEvent = new ParseEvent();
            mFragmentState = FragmentState.EDIT;

        } else {

            mSpinnerEvent.setSelection(mParseEvent.getEvent());
            mTextEvent.setText(mBloodEvents[mParseEvent.getEvent()]);
            updateSpinnerAdapter();
            int eventType = mParseEvent.getEventType();
            if (mParseEvent.getEvent() == ParseEvent.BLOOD_EVENT) {
                mSpinnerEventType.setSelection(eventType);
                mTextEventType.setText(mBloodEventsType[eventType]);
                mTextEentTypeTitle.setText(R.string.text_event_type);
            }
            else if (mParseEvent.getEvent()==ParseEvent.DENIED_EVENT){
                for (int i=0;i<mTemporaryContras.size();i++){
                    if (mTemporaryContras.get(i).id==eventType) {
                        mSpinnerEventType.setSelection(i);
                        mTextEventType.setText(getContraTitle(mTemporaryContras.get(i)));
                        break;
                    }
                }
                mTextEentTypeTitle.setText(R.string.text_event_type_denied);
            }
            mInited = false;
            mSpinnerRemind.setSelection(mParseEvent.getRemind());
            mTextRemind.setText(mReminds[mParseEvent.getRemind()]);
            mEditNote.setText(mParseEvent.getNote() != null ? mParseEvent.getNote() : "");
            mTextNote.setText(mParseEvent.getNote() != null ? mParseEvent.getNote() : "");
            mStatus = mParseEvent.getStatus();
            mIsNewEvent = false;

            if (mStationId==null&&mParseEvent.getPlace()!=null){
                mStationId = mParseEvent.getPlace();
            }
        }
        fillStation();
        mTextDate.setText(Utils.getDateString(getActivity(),mDate));
        //updateStatus();
        setButtonsEnableDisable();
        updateEditMode();
        checkDateForDenied();
        checkForOldEvent();
    }

    private void checkForOldEvent() {
        if (mParseEvent!=null&&mParseEvent.getObjectId()!=null){
            if (mDate-System.currentTimeMillis()<0) {
                if (mParseEvent.getEvent() == ParseEvent.BLOOD_EVENT || mParseEvent.getEvent() == ParseEvent.DENIED_EVENT) {
                    if (mParseEvent.getStatus() != ParseEvent.STATUS_DONE) {
                        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
                        builder.setTitle(R.string.title_old_event);
                        builder.setMessage(R.string.message_old_event);
                        builder.setNegativeButton(R.string.button_leave,null);
                        builder.setNeutralButton(R.string.button_delete,new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                if (mListener!=null)
                                    mListener.startWaiting(R.string.progress_deleting,null);
                                mParseEvent.deleteInBackground(new DeleteCallback() {
                                    @Override
                                    public void done(ParseException e) {
                                        if (mListener!=null)
                                            mListener.stopWaiting();
                                        if (e==null){
                                            if (mListener!=null)
                                                mListener.back();
                                        }
                                        else{
                                            Toast.makeText(getActivity(),getString(R.string.toast_failed_delete_event,e.getMessage()),Toast.LENGTH_LONG).show();
                                        }
                                    }
                                });
                            }
                        });
                        builder.setPositiveButton(R.string.button_set_done,new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                if (mListener!=null)
                                    mListener.startWaiting(R.string.progress_saving,null);
                                mParseEvent.setStatus(ParseEvent.STATUS_DONE);
                                mParseEvent.saveInBackground(new SaveCallback() {
                                    @Override
                                    public void done(ParseException e) {
                                        updateBloodCount(false);
                                        if (mListener!=null)
                                            mListener.stopWaiting();
                                        if (e==null){
                                            if (mListener!=null)
                                                mListener.back();
                                        }
                                        else{
                                            Toast.makeText(getActivity(),getString(R.string.toast_failed_save_event,e.getMessage()),Toast.LENGTH_LONG).show();
                                        }
                                    }
                                });

                            }
                        });
                        builder.show();
                    }
                }
            }
        }
    }

    private void updateEditMode() {
        if (mFragmentState == FragmentState.EDIT){
            mTextEvent.setVisibility(View.GONE);
            mTextEventType.setVisibility(View.GONE);
            mTextRemind.setVisibility(View.GONE);
            mTextNote.setVisibility(View.GONE);
            mSpinnerEvent.setVisibility(View.VISIBLE);
            mSpinnerEventType.setVisibility(View.VISIBLE);
            mSpinnerRemind.setVisibility(View.VISIBLE);
            mEditNote.setVisibility(View.VISIBLE);
            //updateStatus();
        }
        else{
            mTextEvent.setVisibility(View.VISIBLE);
            mTextEventType.setVisibility(View.VISIBLE);
            mTextRemind.setVisibility(View.VISIBLE);
            mTextNote.setVisibility(View.VISIBLE);
            mSpinnerEvent.setVisibility(View.GONE);
            mSpinnerEventType.setVisibility(View.GONE);
            mSpinnerRemind.setVisibility(View.GONE);
            mEditNote.setVisibility(View.GONE);
        }
    }

    private void fillStation(){
        if (mStationId!=null) {
            Station station = DbAdapter.getInstance(getActivity()).getStation(mStationId);
            if (station!=null) {
                mTextPlace.setText(station.getName());
            }
            else{
                mStationId = null;
            }
        }
    }

    //TODO
    private void setButtonsEnableDisable() {
        if (mDate - System.currentTimeMillis() > 0) {
            if (mSpinnerEvent.getSelectedItemPosition() != ParseEvent.DENIED_EVENT) {
                mStatus = ParseEvent.STATUS_NONE;
                mFrameDo.setVisibility(View.GONE);
                mFrameDone.setVisibility(View.GONE);
                mIsEnableChangeStatus = false;
            }
            else{
                mFrameDo.setVisibility(View.GONE);
                mFrameDone.setVisibility(View.GONE);
            }
        } else {
            if (mSpinnerEvent.getSelectedItemPosition() != ParseEvent.DENIED_EVENT) {
                if (mStatus == ParseEvent.STATUS_DONE) {
                    mIsEnableChangeStatus = false;
                    mFrameDo.setVisibility(View.GONE);
                    mFrameDone.setVisibility(View.VISIBLE);
                } else if (mIsNewEvent) {
                    mStatus = ParseEvent.STATUS_DONE;
                    mIsEnableChangeStatus = false;
                    mFrameDo.setVisibility(View.GONE);
                    mFrameDone.setVisibility(View.VISIBLE);
                } else {
                    mIsEnableChangeStatus = true;
                    mFrameDo.setVisibility(View.VISIBLE);
                    mFrameDone.setVisibility(View.GONE);
                }
            }
            else{
                mFrameDo.setVisibility(View.GONE);
                mFrameDone.setVisibility(View.GONE);
            }
        }
    }

    private void handleViews(View rootView) {
        mTextEvent = (TextView)rootView.findViewById(R.id.text_event);
        mTextEvent.setTypeface(FontsHelper.getFont(FontsHelper.FontName.ROBOTO, FontsHelper.FontsStyle.REGULAR));
        mTextEventType = (TextView)rootView.findViewById(R.id.text_event_type);
        mTextEventType.setTypeface(FontsHelper.getFont(FontsHelper.FontName.ROBOTO, FontsHelper.FontsStyle.REGULAR));
        mTextEentTypeTitle = (TextView)rootView.findViewById(R.id.text_title_event_type);
        mTextRemind = (TextView)rootView.findViewById(R.id.text_remind);
        mTextRemind.setTypeface(FontsHelper.getFont(FontsHelper.FontName.ROBOTO, FontsHelper.FontsStyle.REGULAR));
        mTextNote = (TextView)rootView.findViewById(R.id.text_note);
        mTextNote.setTypeface(FontsHelper.getFont(FontsHelper.FontName.ROBOTO, FontsHelper.FontsStyle.REGULAR));
        mSpinnerEvent = (Spinner) rootView.findViewById(R.id.spinner_event);
        CustomSpinnerAdapter adapterEvent = new CustomSpinnerAdapter(getActivity(), R.layout.simple_spinner_item, mBloodEvents);
        adapterEvent.setDropDownViewResource(R.layout.simple_spinner_dropdown_item);
        mSpinnerEvent.setAdapter(adapterEvent);
        mSpinnerEvent.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                if (mInited) {
                    updateSpinnerAdapter();
                    setButtonsEnableDisable();
                    checkDateForDenied();
                }
                mInited = true;
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        mSpinnerEventType = (Spinner) rootView.findViewById(R.id.spinner_event_type);
        mBloodAdapter = new CustomSpinnerAdapter(getActivity(), R.layout.simple_spinner_item, mBloodEventsType);
        mBloodAdapter.setDropDownViewResource(R.layout.simple_spinner_dropdown_item);
        mDeniedAdapter = new CustomSpinner3RowAdapter(getActivity(), Contras.newInstance(getActivity()).getContrasList(Contras.Type.TEMPORARY));
        mDeniedAdapter.setDropDownViewResource(R.layout.simple_spinner_dropdown_item);

        mSpinnerEventType.setEnabled(false);

        mSpinnerRemind = (Spinner) rootView.findViewById(R.id.spinner_remind);
        CustomSpinnerAdapter adapterRemind = new CustomSpinnerAdapter(getActivity(), R.layout.simple_spinner_item, mReminds);
        adapterRemind.setDropDownViewResource(R.layout.simple_spinner_dropdown_item);
        mSpinnerRemind.setAdapter(adapterRemind);

        mTextPlace = (TextView)rootView.findViewById(R.id.text_place);
        mTextPlace.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (mFragmentState == FragmentState.EDIT)
                    selectPlace();
            }
        });
        mTextPlace.setPaintFlags(mTextPlace.getPaintFlags()| Paint.UNDERLINE_TEXT_FLAG);
        mTextPlace.setTypeface(FontsHelper.getFont(FontsHelper.FontName.ROBOTO, FontsHelper.FontsStyle.REGULAR));
        mTextDate = (TextView) rootView.findViewById(R.id.text_date);
        mTextDate.setTypeface(FontsHelper.getFont(FontsHelper.FontName.ROBOTO, FontsHelper.FontsStyle.REGULAR));
        mTextDate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (mFragmentState == FragmentState.EDIT)
                    showDateTimePicker();
            }
        });

        mEditNote = (EditText) rootView.findViewById(R.id.edit_note);
        mEditNote.setTypeface(FontsHelper.getFont(FontsHelper.FontName.ROBOTO, FontsHelper.FontsStyle.REGULAR));

        mButtonDo = (Button)rootView.findViewById(R.id.button_do);
        mButtonDo.setOnClickListener(mOnDoClick);
        mButtonDo.setTypeface(FontsHelper.getFont(FontsHelper.FontName.ROBOTO, FontsHelper.FontsStyle.REGULAR));
        mFrameDo = (FrameLayout)rootView.findViewById(R.id.layout_do);
        mButtonDone = (Button)rootView.findViewById(R.id.button_done);
        mButtonDone.setOnClickListener(mOnDoneClick);
        mButtonDone.setTypeface(FontsHelper.getFont(FontsHelper.FontName.ROBOTO, FontsHelper.FontsStyle.REGULAR));
        mFrameDone = (FrameLayout)rootView.findViewById(R.id.layout_done);
        for (int i=0;i<mTitles.length;i++){
            TextView text = (TextView)rootView.findViewById(mTitles[i]);
            if (text!=null)
                text.setTypeface(FontsHelper.getFont(FontsHelper.FontName.ROBOTO, FontsHelper.FontsStyle.MEDIUM));
        }

    }


    private void selectPlace() {
        saveFieldsToObject();
        if (mListener!=null) {
            mIsSelectStation = true;
            String stationId = mStationId;
            if (stationId!=null && stationId.length()>3){
                stationId = stationId.substring(0,stationId.length()-2);
            }
            mListener.selectPlace(stationId);
        }
    }

    private void updateSpinnerAdapter() {
        if (mSpinnerEvent.getSelectedItemPosition()==ParseEvent.BLOOD_EVENT){
            mTextEentTypeTitle.setText(R.string.text_event_type);
            mSpinnerEventType.setEnabled(true);
            mSpinnerEventType.setAdapter(mBloodAdapter);
        }
        else if (mSpinnerEvent.getSelectedItemPosition()==ParseEvent.DENIED_EVENT){
            mTextEentTypeTitle.setText(R.string.text_event_type_denied);
            mSpinnerEventType.setEnabled(true);
            mSpinnerEventType.setAdapter(mDeniedAdapter);
        }
        else{
            mSpinnerEventType.setEnabled(false);
        }
    }

    private View.OnClickListener mOnDoClick = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            if (mFragmentState == FragmentState.EDIT&&mIsEnableChangeStatus) {
                mIsStatusUpdated=!mIsStatusUpdated;
                mStatus = ParseEvent.STATUS_DONE;
                setButtonsEnableDisable();
            }
        }
    };
    private View.OnClickListener mOnDoneClick = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            if (mFragmentState == FragmentState.EDIT&&mIsEnableChangeStatus) {
                mIsStatusUpdated=!mIsStatusUpdated;
                mStatus = ParseEvent.STATUS_NONE;
                setButtonsEnableDisable();
            }
        }
    };

    private boolean mIsDlgCanceled = false;

    public class IBSDatePickerDialog extends DatePickerDialog {

        public IBSDatePickerDialog(final Context context, final OnDateSetListener callBack, final int year, final int monthOfYear, final int dayOfMonth) {
            super(context, callBack, year, monthOfYear, dayOfMonth);
        }

        public IBSDatePickerDialog(final Context context, final int theme, final OnDateSetListener callBack, final int year, final int monthOfYear, final int dayOfMonth) {
            super(context, theme, callBack, year, monthOfYear, dayOfMonth);
        }

        @Override
        public void onClick(final DialogInterface dialog, final int which) {
            // Prevent calling onDateSet handler when clicking to dialog buttons other, then "OK"
            if (which == DialogInterface.BUTTON_POSITIVE)
                super.onClick(dialog, which);
        }

        @Override
        protected void onStop() {
            // prevent calling onDateSet handler when stopping dialog for whatever reason (because this includes
            // closing by BACK button or by tapping outside dialog, this is exactly what we try to avoid)

            //super.onStop();
        }
    }

    private void showDateTimePicker() {
        final Calendar calendar = GregorianCalendar.getInstance();
        calendar.setTimeInMillis(mDate);
        IBSDatePickerDialog dlg = new IBSDatePickerDialog(getActivity(), new DatePickerDialog.OnDateSetListener() {
            @Override
            public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {
                if (!mIsDlgCanceled) {
                    calendar.set(Calendar.YEAR, year);
                    calendar.set(Calendar.MONTH, monthOfYear);
                    calendar.set(Calendar.DAY_OF_MONTH, dayOfMonth);
                    TimePickerDialog dlgTime = new TimePickerDialog(getActivity(), new TimePickerDialog.OnTimeSetListener() {
                        @Override
                        public void onTimeSet(TimePicker view, int hourOfDay, int minute) {
                            if (!mIsDlgCanceled) {
                                calendar.set(Calendar.HOUR_OF_DAY, hourOfDay);
                                calendar.set(Calendar.MINUTE, minute);
                                calendar.set(Calendar.SECOND, 0);
                                calendar.set(Calendar.MILLISECOND, 0);
                                mDate = calendar.getTimeInMillis();
                                mTextDate.setText(Utils.getDateString(getActivity(), mDate));
                                //updateStatus();
                                setButtonsEnableDisable();
                                checkDateForDenied();
                            }
                        }
                    }, calendar.get(Calendar.HOUR_OF_DAY), calendar.get(Calendar.MINUTE), DateFormat.is24HourFormat(getActivity()));
                    dlgTime.setOnCancelListener(new DialogInterface.OnCancelListener() {
                        @Override
                        public void onCancel(DialogInterface dialog) {
                            mIsDlgCanceled = true;
                        }
                    });
                    mIsDlgCanceled = false;
                    dlgTime.show();
                }
            }
        }, calendar.get(Calendar.YEAR), calendar.get(Calendar.MONTH), calendar.get(Calendar.DAY_OF_MONTH));
        dlg.setOnCancelListener(new DialogInterface.OnCancelListener() {
            @Override
            public void onCancel(DialogInterface dialog) {
                mIsDlgCanceled = true;
            }
        });
        mIsDlgCanceled = false;
        dlg.show();
    }

    private void updateStatus() {
        if (mDate-System.currentTimeMillis()>0){
            if (mStatus==ParseEvent.STATUS_DONE){
                mStatus = ParseEvent.STATUS_NONE;
                mIsStatusUpdated=!mIsStatusUpdated;
            }
        }
        else{
            if (mStatus==ParseEvent.STATUS_NONE){
                mStatus = ParseEvent.STATUS_DONE;
                mIsStatusUpdated=!mIsStatusUpdated;
            }
        }
        setButtonsEnableDisable();
    }

    private class CustomSpinnerAdapter extends ArrayAdapter<String> {

        public CustomSpinnerAdapter(Context context, int resource, String[] objects) {
            super(context, resource, objects);
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {
            View rootView = super.getView(position, convertView, parent);
            TextView text = (TextView) rootView.findViewById(android.R.id.text1);
            if (text != null) {
                text.setTypeface(FontsHelper.getFont(FontsHelper.FontName.ROBOTO, FontsHelper.FontsStyle.REGULAR));
            }
            return rootView;
        }
    }
    private class CustomSpinner3RowAdapter extends ArrayAdapter<Contras.Contra> {

        private LayoutInflater mInflater;
        public CustomSpinner3RowAdapter(Context context, List<Contras.Contra> objects) {
            super(context, 0, objects);
            mInflater = LayoutInflater.from(context);
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {
            ViewsHolder holder = null;
            if (convertView==null){
                convertView = mInflater.inflate(R.layout.simple_spinner_item,parent,false);
                holder = new ViewsHolder(convertView);
            }
            else{
                holder = (ViewsHolder)convertView.getTag();
            }
            holder.build(position);
            return convertView;
        }
        private class ViewsHolder{
            private TextView name;
            public ViewsHolder(View rootView){
                name = (TextView)rootView.findViewById(android.R.id.text1);
                name.setTypeface(FontsHelper.getFont(FontsHelper.FontName.ROBOTO, FontsHelper.FontsStyle.REGULAR));
                rootView.setTag(this);
            }
            public void build(int position){
                Contras.Contra contra = getItem(position);
                String text = getContraTitle(contra);
                name.setText(text);
            }
        }

        @Override
        public View getDropDownView(int position, View convertView, ViewGroup parent) {
            Views3Holder holder = null;
            if (convertView==null){
                convertView = mInflater.inflate(R.layout.simple_spinner_item_3,parent,false);
                holder = new Views3Holder(convertView);
            }
            else{
                holder = (Views3Holder)convertView.getTag();
            }
            holder.build(position);
            return convertView;
        }
        private class Views3Holder{
            private TextView category;
            private TextView name;
            private TextView period;
            public Views3Holder(View rootView){
                category = (TextView)rootView.findViewById(R.id.text1);
                category.setTypeface(FontsHelper.getFont(FontsHelper.FontName.ROBOTO, FontsHelper.FontsStyle.MEDIUM));
                name = (TextView)rootView.findViewById(R.id.text2);
                name.setTypeface(FontsHelper.getFont(FontsHelper.FontName.ROBOTO, FontsHelper.FontsStyle.REGULAR));
                period = (TextView)rootView.findViewById(R.id.text3);
                period.setTypeface(FontsHelper.getFont(FontsHelper.FontName.ROBOTO, FontsHelper.FontsStyle.REGULAR));
                rootView.setTag(this);
            }
            public void build(int position){
                Contras.Contra contra = getItem(position);
                category.setText(contra.category);
                if (contra.name==null){
                    name.setVisibility(View.GONE);
                }
                else{
                    name.setVisibility(View.VISIBLE);
                    name.setText(contra.name);
                }
                if (contra.period==null){
                    period.setVisibility(View.GONE);
                }
                else{
                    period.setVisibility(View.VISIBLE);
                    period.setText(contra.period);
                }
            }
        }
    }

    private String getContraTitle(Contras.Contra contra) {
        String text;
        if (contra.name==null){
            if (contra.list==null){
                text = contra.category;
            }
            else{
                text = contra.list;
            }
        }
        else{
            text = contra.name;
        }
        if (contra.period!=null){
            text+=" ("+contra.period+")";
        }
        return text;
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        try {
            mListener = (OnFragmentInteractionListener) activity;
        } catch (ClassCastException e) {
            throw new ClassCastException(activity.toString()
                    + " must implement OnFragmentInteractionListener");
        }
        try {
            mPListener = (OnPossibleEventUpdates) activity;
        } catch (ClassCastException e) {
            throw new ClassCastException(activity.toString()
                    + " must implement OnPossibleEventUpdates");
        }

    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        setHasOptionsMenu(true);
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        super.onCreateOptionsMenu(menu, inflater);
        inflater.inflate(R.menu.new_event_menu, menu);
        MenuItem itemSave = menu.findItem(R.id.action_save);
        MenuItem itemEdit = menu.findItem(R.id.action_edit);
        if (mFragmentState == FragmentState.VIEW) {
            itemSave.setVisible(false);
            itemEdit.setVisible(true);
        }
        else{
            itemSave.setVisible(true);
            itemEdit.setVisible(false);
        }

    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.action_edit:
                mFragmentState = FragmentState.EDIT;
                updateEditMode();
                getActivity().supportInvalidateOptionsMenu();
                checkDateForDenied();
                return true;

            case R.id.action_save:
                save();
                return true;
            case R.id.action_delete:
                delete();
                return true;

        }
        return super.onOptionsItemSelected(item);
    }

    private void delete() {
        if (mParseEvent!=null && mParseEvent.getObjectId()!=null){
            removeAutoEvents();
            if (mPListener!=null)
                mPListener.invalidate();
            mParseEvent.deleteInBackground(new DeleteCallback() {
                @Override
                public void done(ParseException e) {
                    if (e==null){
                        if (mParseEvent.getEvent()==ParseEvent.BLOOD_EVENT&&mParseEvent.getStatus()==ParseEvent.STATUS_DONE)
                            updateBloodCount(true);
                        if (mListener!=null) {
                            mListener.updateRemindTimer();
                            mListener.back();
                        }

                    }
                    else{
                        //TODO error
                    }
                }
            });
        }
        else{
            if (mListener!=null)
                mListener.back();
        }
    }

    private void save() {
        /*if (mSpinnerEvent.getSelectedItemPosition() == ParseEvent.DENIED_EVENT && mDate>=Utils.getEndDay(System.currentTimeMillis())){
            AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
            builder.setTitle(R.string.title_denied_in_future);
            builder.setMessage(R.string.message_denied_in_future);
            builder.setPositiveButton(android.R.string.ok,null);
            builder.show();
            return;

        }
        */

        /*if (mSpinnerEvent.getSelectedItemPosition() == ParseEvent.DENIED_EVENT && mStatus!=ParseEvent.STATUS_DONE){
            AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
            builder.setTitle(R.string.title_status_should_be_done);
            builder.setMessage(R.string.message_status_should_be_done);
            builder.setPositiveButton(android.R.string.ok,null);
            builder.show();
            return;
        }
        */

        saveFieldsToObject();
        mParseEvent.setManualSave(true);
        if (mListener != null) {
            mListener.startWaiting(R.string.progress_saving, null);
        }

        ParseQuery<ParseEvent> query = ParseEvent.checkExistEvent(new Date(mDate), mParseEvent);
        query.findInBackground(new FindCallback<ParseEvent>() {
            @Override
            public void done(List<ParseEvent> parseEvents, ParseException e) {
                if (e == null) {
                    if (parseEvents.size() != 0) {
                        if (mListener != null)
                            mListener.stopWaiting();
                        if (getActivity() != null) {
                            AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
                            builder.setTitle(R.string.title_cant_save);
                            builder.setMessage(R.string.message_already_exist);
                            builder.setPositiveButton(android.R.string.ok, null);
                            builder.show();

                        }
                    } else {
                        mParseEvent.setAutoEvents(updateFutureEvents());
                        if (mPListener!=null)
                            mPListener.invalidate();
                        mParseEvent.saveInBackground(new SaveCallback() {
                            @Override
                            public void done(ParseException e) {

                                if (e == null) {
                                    if (mListener != null) {
                                        mListener.updateRemindTimer();
                                        mListener.stopWaiting();
                                    }
                                    updateYears();
                                    if ((mIsStatusUpdated||mIsNewEvent) &&
                                            mParseEvent.getEvent() == ParseEvent.BLOOD_EVENT
                                            &&mParseEvent.getStatus()==ParseEvent.STATUS_DONE) {
                                        updateBloodCount(false);
                                        showThanksToast();
                                    }
                                    sendBroadcast();
                                    if (mListener != null)
                                        mListener.back();

                                } else {
                                    if (mListener != null)
                                        mListener.stopWaiting();

                                    if (getActivity() != null) {
                                        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
                                        builder.setTitle(R.string.title_cant_save);
                                        builder.setMessage(R.string.message_failed_save_event);
                                        builder.setPositiveButton(android.R.string.ok, null);
                                        builder.show();
                                    }

                                }
                            }
                        });
                    }
                } else {
                    if (mListener != null)
                        mListener.stopWaiting();
                    if (getActivity() != null) {
                        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
                        builder.setTitle(R.string.title_cant_save);
                        builder.setMessage(R.string.message_failed_save_event);
                        builder.setPositiveButton(android.R.string.ok, null);
                        builder.show();
                    }
                }
            }
        });
    }

    private void updateYears() {
        AppUser user  = AppUser.getCurrentUser();
        if (user==null)
            return;
        if (user.addYear(mParseEvent.getYear()))
            user.saveInBackground();
    }

    private void updateBloodCount(boolean isDelete) {
        AppUser user = AppUser.getCurrentUser();
        if (user!=null){
            int bloodCount = 0;
            if (mParseEvent.getStatus()==ParseEvent.STATUS_DONE) {
                if (isDelete)
                    bloodCount-=1;
                else {
                    bloodCount += 1;
                }
            }
            else if (mParseEvent.getStatus()==ParseEvent.STATUS_NONE)
                bloodCount-=1;
            int profileCount = 0;
            try{
                profileCount = Integer.parseInt(user.getCountBlood());
            }
            catch (NumberFormatException en){
                en.printStackTrace();
                profileCount = 0;
            }
            profileCount+=bloodCount;
            user.setCountBlood(Integer.toString(profileCount));
            user.saveInBackground();
        }
    }


    private List<ParseEvent> updateFutureEvents() {
        int blood_day=-1,plazma_day=-1,trombo_day=-1;
        removeAutoEvents();
        if (mParseEvent.getStatus()==ParseEvent.STATUS_DONE) {
            if (mParseEvent.getEvent() == ParseEvent.BLOOD_EVENT) {
                if (mParseEvent.getEventType() == ParseEvent.BLOOD_EVENT_BLOOD) {
                    blood_day = 60;
                    plazma_day = 30;
                    trombo_day = 30;
                } else {
                    blood_day = 14;
                    plazma_day = 14;
                    trombo_day = 14;
                }
            } else if (mParseEvent.getEvent() == ParseEvent.DENIED_EVENT) {
                int position = mSpinnerEventType.getSelectedItemPosition();
                Contras.Contra contra = mTemporaryContras.get(position);
                blood_day = contra.days;
                plazma_day = contra.days;
                trombo_day = contra.days;

            } else return null;
            final int fBloodDay = blood_day;
            final int fPlazmaDay = plazma_day;
            final int fTromboDay = trombo_day;
            return createNewEvents(fBloodDay, fPlazmaDay, fTromboDay);
        }
        else
            return null;
    }

    private void removeAutoEvents() {
        List<ParseEvent> events = mParseEvent.getAutoEvents();
        if (events!=null){
            for (ParseEvent event:events){
                if (!event.getManualSave())
                    event.deleteInBackground();
            }
        }
    }

    private List<ParseEvent> createNewEvents(int fBloodDay, int fPlazmaDay, int fTromboDay) {
        Calendar calendar = GregorianCalendar.getInstance();
        ArrayList<ParseEvent> events = new ArrayList<>();
        if (fBloodDay>=0){
            calendar.setTime(mParseEvent.getDate());
            calendar.add(Calendar.DAY_OF_MONTH,fBloodDay);
            if (calendar.getTimeInMillis()>=System.currentTimeMillis()) {
                ParseEvent event = new ParseEvent();
                event.setEvent(ParseEvent.BLOOD_EVENT);
                event.setUser(AppUser.getCurrentUser());
                event.setEventType(ParseEvent.BLOOD_EVENT_BLOOD);
                event.setDate(calendar.getTime());
                event.setPlace(mParseEvent.getPlace());
                event.setRemind(2);
                setRemindDate(event);
                event.setStatus(ParseEvent.STATUS_NONE);
                events.add(event);
            }
        }
        if (fPlazmaDay>=0){
            calendar.setTime(mParseEvent.getDate());
            calendar.add(Calendar.DAY_OF_MONTH,fPlazmaDay);
            if (calendar.getTimeInMillis()>=System.currentTimeMillis()) {
                ParseEvent event = new ParseEvent();
                event.setEvent(ParseEvent.BLOOD_EVENT);
                event.setUser(AppUser.getCurrentUser());
                event.setEventType(ParseEvent.BLOOD_EVENT_PLAZMA);
                event.setDate(calendar.getTime());
                event.setPlace(mParseEvent.getPlace());
                event.setRemind(2);
                setRemindDate(event);
                event.setStatus(ParseEvent.STATUS_NONE);
                events.add(event);
            }
        }
        if (fTromboDay>=0){
            calendar.setTime(mParseEvent.getDate());
            calendar.add(Calendar.DAY_OF_MONTH, fTromboDay);
            if (calendar.getTimeInMillis()>=System.currentTimeMillis()) {
                ParseEvent event = new ParseEvent();
                event.setUser(AppUser.getCurrentUser());
                event.setEvent(ParseEvent.BLOOD_EVENT);
                event.setEventType(ParseEvent.BLOOD_EVENT_TROMBO);
                event.setDate(calendar.getTime());
                event.setPlace(mParseEvent.getPlace());
                event.setRemind(2);
                setRemindDate(event);
                event.setStatus(ParseEvent.STATUS_NONE);
                events.add(event);
            }
        }
        return events;
    }

    private void setRemindDate(ParseEvent event) {
        int hourToRemind = event.getRemind();
        if (hourToRemind>0){
            Calendar calendar = GregorianCalendar.getInstance();
            calendar.setTime(event.getDate());
            calendar.add(Calendar.HOUR_OF_DAY,-hourToRemind);
            event.setDateRemind(calendar.getTime());
        }

    }

    private void saveFieldsToObject() {
        mParseEvent.setUser(AppUser.getCurrentUser());
        mParseEvent.setDate(new Date(mDate));
        mParseEvent.setEvent(mSpinnerEvent.getSelectedItemPosition());
        mParseEvent.removeDateRemind();
        if (mParseEvent.getEvent() == ParseEvent.BLOOD_EVENT||mParseEvent.getEvent() == ParseEvent.DENIED_EVENT) {
            int eventType = mSpinnerEventType.getSelectedItemPosition();
            if (mParseEvent.getEvent()==ParseEvent.DENIED_EVENT) {
                int days = mTemporaryContras.get(eventType).days;
                Calendar calendar = GregorianCalendar.getInstance();
                calendar.setTimeInMillis(mDate);
                calendar.add(Calendar.DAY_OF_YEAR,days);
                mParseEvent.setDeniedFinished(calendar.getTime());

                eventType = mTemporaryContras.get(eventType).id;
                mParseEvent.setStatus(ParseEvent.STATUS_DONE);
            }
            else if (mParseEvent.getEvent()==ParseEvent.BLOOD_EVENT) {
                int days = -1;
                if (mParseEvent.getEventType()==ParseEvent.BLOOD_EVENT_BLOOD)
                    days = 30;
                else if (mParseEvent.getEventType()==ParseEvent.BLOOD_EVENT_PLAZMA)
                    days = 14;
                else if (mParseEvent.getEventType()==ParseEvent.BLOOD_EVENT_TROMBO)
                    days = 14;
                Calendar calendar = GregorianCalendar.getInstance();
                calendar.setTimeInMillis(mDate);
                calendar.add(Calendar.DAY_OF_YEAR,days);
                mParseEvent.setDeniedFinished(calendar.getTime());
            }
            mParseEvent.setEventType(eventType);
            mParseEvent.setStatus(mStatus);
        } else {
            mParseEvent.setEventType(ParseEvent.EMPTY_EVENT);
        }
        if (mParseEvent.getEvent()==ParseEvent.ANALIZE_EVENT||mParseEvent.getEvent()==ParseEvent.BLOOD_EVENT){
            int remindPosition = mSpinnerRemind.getSelectedItemPosition();
            int hourToRemind = 0;
            switch (remindPosition){
                case 0:
                    hourToRemind = 1;
                    break;
                case 1:
                    hourToRemind = 4;
                    break;
                case 2:
                    hourToRemind = 24;
                    break;
            }
            if (hourToRemind>0){
                Calendar calendar = GregorianCalendar.getInstance();
                calendar.setTimeInMillis(mDate);
                calendar.add(Calendar.HOUR_OF_DAY,-hourToRemind);
                mParseEvent.setDateRemind(calendar.getTime());
            }
        }
        mParseEvent.setRemind(mSpinnerRemind.getSelectedItemPosition());
        mParseEvent.setPlace(mStationId);
        if (mListener!=null)
            mListener.getSettings().edit().putString(AppConstants.PREF_DEFAULT_STATION,mStationId).commit();
        mParseEvent.setNote(mEditNote.getText().toString());
    }

    private void sendBroadcast() {
        if (getActivity() != null) {
            Intent intent = new Intent(AppConstants.ACTION_EVENT_BROADCAST);
            intent.putExtra(AppConstants.ARG_FROM_DATE, mDate);
            intent.putExtra(AppConstants.ARG_NEW_DATE, mParseEvent.getDate().getTime());
            getActivity().sendBroadcast(intent);
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
        mPListener = null;
    }

    @Override
    public void onResume() {
        super.onResume();
        if (mListener != null)
            mListener.setTitle(R.string.title_event);

    }
    private boolean mDeniedMessageShown = false;
    private ParseQuery<ParseEvent> mDeniedCheckQuery = null;
    private void checkDateForDenied(){
        if (mDeniedMessageShown)
            return;
        if (mDeniedCheckQuery!=null)
            return;
        if (mFragmentState==FragmentState.EDIT&&mSpinnerEvent.getSelectedItemPosition()==ParseEvent.BLOOD_EVENT) {
            mDeniedCheckQuery = ParseEvent.getExistsDeniedEvents(new Date(mDate));
            mDeniedCheckQuery.getFirstInBackground(new GetCallback<ParseEvent>() {
                @Override
                public void done(ParseEvent parseEvent, ParseException e) {
                    mDeniedCheckQuery = null;
                    if (e==null&&parseEvent!=null){
                        showDeniedAlert(parseEvent);
                    }
                }
            });
        }
    }

    private void showDeniedAlert(ParseEvent parseEvent) {
        mDeniedMessageShown = true;
        Toast.makeText(getActivity(),R.string.toast_denied_alert,Toast.LENGTH_LONG).show();
    }
    private void showThanksToast(){
        Toast.makeText(getActivity(),R.string.toast_thanks,Toast.LENGTH_LONG).show();
    }

}
