package com.lexadev.donorinfo.ui;

import android.app.Activity;
import android.content.Context;
import android.content.DialogInterface;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;

import com.lexadev.donorinfo.AppConstants;
import com.lexadev.donorinfo.OnFragmentInteractionListener;
import com.lexadev.donorinfo.R;
import com.lexadev.donorinfo.helper.DbAdapter;
import com.lexadev.donorinfo.helper.FontsHelper;
import com.lexadev.donorinfo.helper.LoadingProgressDialog;
import com.lexadev.donorinfo.model.ParseRequest;
import com.lexadev.donorinfo.parse.AppUser;
import com.parse.FindCallback;
import com.parse.ParseException;
import com.parse.ParseQuery;

import java.text.DateFormat;
import java.util.Date;
import java.util.List;

public class RequestsFragment extends Fragment {
    public static final String TAG="RequestFragemnt";

    private static final String PROGRESS = "progress";
    private static final String LIST = "list";

    private FrameLayout mLayoutSelectRegions;
    private FrameLayout mLayoutNext;
    private FrameLayout mLayoutPrev;
    private TextView mTextViewRegion;
    private ListView mList;

    private boolean mIsAdmin = false;
    private OnFragmentInteractionListener mListener;
    private boolean mIsSelectRegions = false;
    private String mRegionId = null;
    private RequestsAdapter mAdapter=null;

    private int mProgress = 0;
    private int mPreviousProgress = -1;
    //private boolean mIsLoaded = false;

    public RequestsFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (savedInstanceState!=null)
            mProgress = savedInstanceState.getInt(PROGRESS);
        if (mListener!=null)
            mIsAdmin = AppUser.getCurrentUser().isAdmin()&&mListener.isUseAdmin();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        if (mListener!=null)
            mRegionId = mListener.getSelectedRegionId();
        mPreviousProgress = -1;
        View rootView = inflater.inflate(R.layout.fragment_requests, container, false);
        if (mListener!=null)
            mListener.setupUI(rootView);
        handleViews(rootView);
        if (savedInstanceState!=null)
            if (savedInstanceState.containsKey(LIST))
                mList.onRestoreInstanceState(savedInstanceState.getParcelable(LIST));
        fillViews();
        loadRequests();
        return rootView;
    }

    private void loadRequests() {
        final ParseQuery<ParseRequest> query = ParseRequest.getRequests(mRegionId,mIsAdmin);
        if (mListener!=null)
            mListener.startWaiting(R.string.progress_loading,new LoadingProgressDialog.OnCancelListener() {
                @Override
                public void onCancel(DialogInterface dialog) {

                    new AsyncTask<Void,Void,Void>(){
                        @Override
                        protected Void doInBackground(Void... voids) {
                            query.cancel();
                            return null;
                        }
                    };
                }
            });
        query.setSkip(mProgress);
        query.setLimit(AppConstants.REQUESTS_PER_PAGE);
        query.findInBackground(new FindCallback<ParseRequest>() {
            @Override
            public void done(List<ParseRequest> parseRequests, ParseException e) {
                if (mListener!=null)
                    mListener.stopWaiting();
                if (e==null){
                    if ((parseRequests==null || parseRequests.size()==0)&&mPreviousProgress>=0){
                        mProgress = mPreviousProgress;
                    }
                    else {
                        mAdapter = new RequestsAdapter(getActivity(), parseRequests, mIsAdmin);
                        mList.setAdapter(mAdapter);
                    }
                }
                else{
                    //TODO
                }

            }
        });
    }

    private static class RequestsAdapter extends ArrayAdapter<ParseRequest>{
        private LayoutInflater mInflater;
        private boolean mIsAdamin;
        public RequestsAdapter(Context context, List<ParseRequest> objects,boolean isAdmin) {
            super(context, 0, objects);
            mInflater = LayoutInflater.from(context);
            mIsAdamin = isAdmin;
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {
            ViewsHolder holder;
            if (convertView==null){
                convertView = mInflater.inflate(R.layout.layout_requests,parent,false);
                holder=new ViewsHolder(convertView);
            }
            else{
                holder = (ViewsHolder)convertView.getTag();
            }
            holder.build(position);
            return convertView;
        }

        private class ViewsHolder{
            private TextView status;
            private TextView date;
            private TextView needed;
            private ImageView image;
            private TextView blood;
            public ViewsHolder(View rootView){
                date = (TextView)rootView.findViewById(R.id.date);
                date.setTypeface(FontsHelper.getFont(FontsHelper.FontName.ROBOTO, FontsHelper.FontsStyle.BOLD));
                TextView titleNeeded = (TextView)rootView.findViewById(R.id.text_title_needed);
                titleNeeded.setTypeface(FontsHelper.getFont(FontsHelper.FontName.ROBOTO, FontsHelper.FontsStyle.MEDIUM));
                needed = (TextView)rootView.findViewById(R.id.text_needed);
                needed.setTypeface(FontsHelper.getFont(FontsHelper.FontName.ROBOTO, FontsHelper.FontsStyle.REGULAR));
                image = (ImageView)rootView.findViewById(R.id.image_needed);
                TextView titleBlood = (TextView)rootView.findViewById(R.id.text_title_blood);
                titleBlood.setTypeface(FontsHelper.getFont(FontsHelper.FontName.ROBOTO, FontsHelper.FontsStyle.MEDIUM));
                blood = (TextView)rootView.findViewById(R.id.text_blood);
                blood.setTypeface(FontsHelper.getFont(FontsHelper.FontName.ROBOTO, FontsHelper.FontsStyle.REGULAR));
                status = (TextView)rootView.findViewById(R.id.status);
                status.setTypeface(FontsHelper.getFont(FontsHelper.FontName.ROBOTO, FontsHelper.FontsStyle.REGULAR));
                rootView.setTag(this);
            }
            public void build(int position){
                ParseRequest request = getItem(position);
                DateFormat format = DateFormat.getDateInstance();
                Date createdAt = request.getCreatedAt();
                date.setText(format.format(createdAt));
                String neededStr = request.getNeeded();
                needed.setText(neededStr);
                blood.setText(request.getBlood());
                String[] neededArray = getContext().getResources().getStringArray(R.array.array_needed);
                if (neededStr.equals(neededArray[0])){
                    image.setImageResource(R.drawable.ic_red_blood);
                }else if (neededStr.equals(neededArray[1])){
                    image.setImageResource(R.drawable.ic_yellow_blood);
                }else if (neededStr.equals(neededArray[2])){
                    image.setImageResource(R.drawable.ic_green_blood);
                }
                if (mIsAdamin){
                    status.setVisibility(View.VISIBLE);
                    if (request.getStatus()== ParseRequest.Status.NEW){
                        status.setText(R.string.status_new);
                        status.setBackgroundResource(R.drawable.status_new);
                    }
                    else{
                        status.setText(R.string.status_approved);
                        status.setBackgroundResource(R.drawable.status_approved);

                    }
                }
                else{
                    status.setVisibility(View.GONE);
                }
            }
        }
    }
    private void fillViews() {
        String str = null;
        if (mRegionId == null){
            str = getString(R.string.item_all_regions);
        }
        else{
            str = DbAdapter.getInstance(getActivity()).getRegionName(mRegionId);
        }
        mTextViewRegion.setText(str);
    }

    private void handleViews(View rootView) {
        mLayoutSelectRegions = (FrameLayout)rootView.findViewById(R.id.select_region);
        mLayoutSelectRegions.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                selectRegions();
            }
        });
        mTextViewRegion = (TextView)rootView.findViewById(R.id.text_select_region);
        mLayoutPrev = (FrameLayout)rootView.findViewById(R.id.layout_prev);
        mLayoutPrev.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (mProgress>0)
                    mProgress-=AppConstants.REQUESTS_PER_PAGE;
                if (mProgress<0)
                    mProgress = 0;
                loadRequests();
            }
        });

        mLayoutNext = (FrameLayout)rootView.findViewById(R.id.layout_next);
        mLayoutNext.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                int items = mList.getCount();
                if (items>0) {
                    mPreviousProgress = mProgress;
                    mProgress += items;
                    loadRequests();
                }
            }
        });
        mList = (ListView)rootView.findViewById(R.id.list_requests);
        mList.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                ParseRequest request = (ParseRequest)mList.getItemAtPosition(position);
                if (mListener!=null)
                    mListener.viewRequest(request);
            }
        });
        if (mAdapter!=null)
            mList.setAdapter(mAdapter);
    }

    private void selectRegions(){
        if (mListener!=null)
            mListener.selectRegions(mRegionId);
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        try {
            mListener = (OnFragmentInteractionListener) activity;
        } catch (ClassCastException e) {
            throw new ClassCastException(activity.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    @Override
    public void onResume() {
        super.onResume();
        if (mListener!=null)
            mListener.setTitle(R.string.title_fragment_requests);

    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putInt(PROGRESS,mProgress);
        outState.putParcelable(LIST,mList.onSaveInstanceState());
    }
}
