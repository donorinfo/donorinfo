package com.lexadev.donorinfo.ui;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Context;
import android.graphics.Point;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.app.DialogFragment;
import android.support.v4.app.FragmentTransaction;
import android.view.Display;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.GoogleMap.OnMapLongClickListener;
import com.google.android.gms.maps.GoogleMap.OnMarkerDragListener;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.lexadev.donorinfo.OnFragmentInteractionListener;
import com.lexadev.donorinfo.R;
import com.lexadev.donorinfo.model.Station;
import com.lexadev.donorinfo.widgets.CustomSupportMapFragment;


public class StationMapFragment extends DialogFragment {
	@SuppressWarnings("unused")
	private OnFragmentInteractionListener mListener;
	private GoogleMap map = null;
	private Button buttonClose;
	private Marker marker;

	public static StationMapFragment newInstance(Station item) {
		StationMapFragment fragment = new StationMapFragment();
		fragment.initialize(item);
		return fragment;
	}

    private Station item;
	private LatLng location;

	public void initialize(Station item) {
		if (item != null) {
            this.item = item;
            location = new LatLng(item.getLatitude(),item.getLongitude());
		}
	}

	public StationMapFragment() {
		// Required empty public constructor
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		getDialog().getWindow().requestFeature(Window.FEATURE_NO_TITLE);
		// Inflate the layout for this fragment
		View rootView = inflater.inflate(R.layout.fragment_map, container,
				false);
		handleViews(rootView);
		return rootView;
	}

	@Override
	public void onStart() {	
		super.onStart();
		 // safety check
		  if (getDialog() == null) {
		    return;
		  }
		  Point size = getSize();
		  
		  int dialogWidth = size.x-50;
		  int dialogHeight = size.y-size.y/5;

		  getDialog().getWindow().setLayout(dialogWidth, dialogHeight);

	}

	@SuppressLint("NewApi")
	@SuppressWarnings("deprecation")
	protected Point getSize() {
		WindowManager wm = (WindowManager) getActivity().getSystemService(Context.WINDOW_SERVICE);
		Display display = wm.getDefaultDisplay();

		final Point point = new Point();

		if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB_MR2) {
			display.getSize(point);
		} else {
			point.x = display.getWidth();
			point.y = display.getHeight();
		}
		return point;
	}
	
	private void handleViews(View rootView) {
		buttonClose = (Button) rootView.findViewById(R.id.close);
		buttonClose.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				dismiss();
			}
		});

		Handler handler = new Handler();
		handler.post(new Runnable() {

			@Override
			public void run() {
				showMap();

			}
		});

	}

	private void showMap() {
		final CustomSupportMapFragment mapFragment = new CustomSupportMapFragment();
		mapFragment.setOnMapCreatedListener(new CustomSupportMapFragment.MapViewCreatedListener() {

			@Override
			public void onMapCreated() {
				map = mapFragment.getMap();
				if (map != null) {
					if (location != null) {
						addMarker();
					}
				}

			}
		});
		FragmentTransaction ft = getChildFragmentManager().beginTransaction();
		ft.replace(R.id.map_container, mapFragment);
		ft.commit();

	}

	private void addMarker() {
		if (location == null)
			return;
        String snippet = "";
        if (item.getAddress()!=null)
            snippet+=item.getAddress()+"\n";
        if (item.getPhone()!=null)
            snippet+=item.getPhone()+"\n";
        if (item.getDayWork()!=null)
            snippet+=item.getDayWork()+"\n";
        if (item.getTimeWork()!=null)
            snippet+=item.getTimeWork()+"\n";


        marker = map.addMarker(
                new MarkerOptions()
                    .position(location)
				    .draggable(false)
                    .icon(BitmapDescriptorFactory.fromResource(R.drawable.ic_location))
                    .title(item.getName())
                    .snippet(snippet)

        );
		map.setOnMarkerDragListener(new OnMarkerDragListener() {
			@Override
			public void onMarkerDragStart(Marker arg0) {
			}

			@Override
			public void onMarkerDragEnd(Marker arg0) {
				map.animateCamera(CameraUpdateFactory.newLatLng(arg0
						.getPosition()));
			}

			@Override
			public void onMarkerDrag(Marker arg0) {
			}
		});
		map.moveCamera(CameraUpdateFactory.newLatLngZoom(location, 15));
		map.setOnMapLongClickListener(new OnMapLongClickListener() {
			
			@Override
			public void onMapLongClick(LatLng arg0) {
				if (marker!=null){
					marker.setPosition(arg0);
				}				
			}
		});

	}

	@Override
	public void onAttach(Activity activity) {
		super.onAttach(activity);
		try {
			mListener = (OnFragmentInteractionListener) activity;
		} catch (ClassCastException e) {
			throw new ClassCastException(activity.toString()
					+ " must implement OnFragmentInteractionListener");
		}
	}

	@Override
	public void onDetach() {
		super.onDetach();
		mListener = null;
	}
}
