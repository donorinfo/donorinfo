package com.lexadev.donorinfo.ui;

import android.app.Activity;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.lexadev.donorinfo.OnFragmentInteractionListener;
import com.lexadev.donorinfo.R;
import com.lexadev.donorinfo.helper.FontsHelper;

public class MainFragment extends Fragment {

    private OnFragmentInteractionListener mListener;

    private LinearLayout mLayoutCalendar;
    private LinearLayout mLayoutProfile;
    private LinearLayout mLayoutInfo;
    private LinearLayout mLayoutStations;
    private LinearLayout mLayoutRequests;
    private LinearLayout mLayoutNeeded;

    private TextView mTextAbout;

    private int[] mTitles = new int[]{
        R.id.text_title_calendar,
            R.id.text_title_profile,
            R.id.text_title_info,
            R.id.text_title_station,
            R.id.text_title_request,
            R.id.text_title_needed,
            R.id.action_about_app
    };

    public MainFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_main, container, false);
        if (mListener!=null)
            mListener.setupUI(rootView);
        handleViews(rootView);
        return rootView;
    }

    private void handleViews(View rootView) {
        mLayoutCalendar = (LinearLayout) rootView.findViewById(R.id.action_calendar);
        mLayoutCalendar.setOnClickListener(new mOnButtonClickListener());
        mLayoutProfile = (LinearLayout) rootView.findViewById(R.id.action_profile);
        mLayoutProfile.setOnClickListener(new mOnButtonClickListener());
        mLayoutInfo = (LinearLayout) rootView.findViewById(R.id.action_info);
        mLayoutInfo.setOnClickListener(new mOnButtonClickListener());
        mLayoutStations = (LinearLayout) rootView.findViewById(R.id.action_station);
        mLayoutStations.setOnClickListener(new mOnButtonClickListener());
        mLayoutRequests = (LinearLayout) rootView.findViewById(R.id.action_request);
        mLayoutRequests.setOnClickListener(new mOnButtonClickListener());
        mLayoutNeeded = (LinearLayout) rootView.findViewById(R.id.action_needed);
        mLayoutNeeded.setOnClickListener(new mOnButtonClickListener());
        mTextAbout = (TextView) rootView.findViewById(R.id.action_about_app);
        mTextAbout.setOnClickListener(new mOnButtonClickListener());

        for (int i=0;i<mTitles.length;i++){
            TextView text = (TextView)rootView.findViewById(mTitles[i]);
            if (text!=null)
                text.setTypeface(FontsHelper.getFont(FontsHelper.FontName.COMFORTAA, FontsHelper.FontsStyle.REGULAR));
        }


    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        try {
            mListener = (OnFragmentInteractionListener) activity;
        } catch (ClassCastException e) {
            throw new ClassCastException(activity.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    private class mOnButtonClickListener implements View.OnClickListener {
        @Override
        public void onClick(View v) {
            if (mListener != null)
                mListener.toSelected(v.getId());
        }
    }

    @Override
    public void onResume() {
        super.onResume();
        if (mListener!=null)
            mListener.setTitle(R.string.app_name);
    }
}
