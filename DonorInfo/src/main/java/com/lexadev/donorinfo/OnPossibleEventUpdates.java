package com.lexadev.donorinfo;

import com.lexadev.donorinfo.model.ParseEvent;

/**
 * Created by Alexey Rogovoy (lexapublic@gmail.com) on 08.04.2015.
 */
public interface OnPossibleEventUpdates {
    public void updateEvent(int eventType,ParseEvent event);
    public ParseEvent getEvent(int eventType);
    public void invalidate();
}
