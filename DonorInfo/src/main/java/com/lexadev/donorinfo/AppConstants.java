package com.lexadev.donorinfo;

/**
 * Created by Alexey Rogovoy (lexapublic@gmail.com) on 12.02.2015.
 */
public class AppConstants {
    public static final boolean DEBUG = true;
    public static final boolean ACRA_ENABLE = true;
    public static final long SPLASH_TIME_OUT = 1500;
    public static final int MONTHS_COUNT = 60*12;
    public static final String PREF_EMAIL = "pref.email";
    public static final String ACTION_EVENT_BROADCAST = "com.lexadev.donorinfo.action.event";
    public static final String ARG_FROM_DATE = "from";
    public static final String ARG_NEW_DATE = "new";
    public static final String PARSE_TABLE_EVENT = "Event";
    public static final String PARSE_TABLE_STATIONS = "Stations";
    public static final String PREF_SELECTED_REGION = "pref.region";
    public static final String PARSE_TABLE_VERSIONS = "Versions";
    public static final String PARSE_TABLE_REGIONS = "Regions";
    public static final String PARSE_TABLE_CITIES = "Cities";
    public static final String PREF_DEFAULT_STATION = "pref_station_id";
    public static final String PARSE_TABLE_REQUEST = "Requests";
    public static final int REQUESTS_PER_PAGE = 5;
    public static final String PREF_SELECTED_REGION_REQUESTS = "pref.regions.requests";
    public static final String PREF_USER_ADMIN = "pref.use.admin";
    public static final String ACTION_REMIND = "com.lexadev.donorinfo.ACTION_REMIND";
    public static final String REQUESTS_CHANNEL = "requests";
    public static final int REGION_NEWS_PER_PAGE = 10;
    public static final int MAX_QUERY_LIMIT = 100;
}
