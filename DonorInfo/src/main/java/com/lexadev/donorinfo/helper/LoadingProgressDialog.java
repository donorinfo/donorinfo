package com.lexadev.donorinfo.helper;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;

import com.lexadev.donorinfo.R;


/**
 * The Class Loading Progress Dialog.
 */
public class LoadingProgressDialog {
	
	/**
	 * The listener interface for receiving onCancel events.
	 */
	public interface OnCancelListener{
		
		/**
		 * On cancel.
		 *
		 * @param dialog the dialog
		 */
		public void onCancel(DialogInterface dialog);
	}
	
	/** The on cancel listener. */
	private OnCancelListener mListener;
	
	/** The context. */
	private Context context;
	
	/**
	 * Instantiates a new loading progress dialog.
	 *
	 * @param context the context
	 */
	public LoadingProgressDialog(Context context){
		this.context = context;
	}
	/**
	 * Sets the on cancel listener.
	 *
	 * @param l the new on cancel listener
	 */
	public void setOnCancelListener(OnCancelListener l){
		mListener = l;
	}
	
	/** The progress dialog. */
	private ProgressDialog progressDialog;
	
	/**
	 * Show progress dialog.
	 */
	public ProgressDialog showProgressDialog(){
		return showProgressDialog(R.string.progress_waiting);
	}
	
	/**
	 * Show progress dialog.
	 *
	 * @param id the message string resource id
	 */
	public ProgressDialog showProgressDialog(int id){
		if (progressDialog!=null){
			if (progressDialog.isShowing()){
				return progressDialog;
			}
			hideProgressDialog();
		}		
		progressDialog = new ProgressDialog(context);
		if (id>0){
			progressDialog.setMessage(context.getResources().getString(id));
		}
		progressDialog.setIndeterminate(true);
		if (mListener==null)
			progressDialog.setCancelable(false);
		else
			progressDialog.setCancelable(true);
		progressDialog.setOnCancelListener(new DialogInterface.OnCancelListener() {
			
			@Override
			public void onCancel(DialogInterface dialog) {
				if (mListener!=null)
					mListener.onCancel(dialog);
			}
		});
		progressDialog.show();
		return progressDialog;
	}
	
	/**
	 * Hide progress dialog.
	 */
	public void hideProgressDialog(){
		if (progressDialog!=null){
			try{
				if (progressDialog.isShowing()){
					progressDialog.dismiss();				
				}
			}catch(Exception e){
				e.printStackTrace();
			}
		}
		progressDialog = null;
	}
    public boolean isShowing(){
        if (progressDialog!=null)
            return progressDialog.isShowing();
        return false;
    }
}
