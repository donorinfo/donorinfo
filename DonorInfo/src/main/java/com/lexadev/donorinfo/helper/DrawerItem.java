package com.lexadev.donorinfo.helper;

/**
 * Created by Alexey Rogovoy (lexapublic@gmail.com) on 12.02.2015.
 */
public class DrawerItem {
    private int mId;
    private int mIconId;
    private int mNameId;
    private boolean mEnableAlways = true;
    public DrawerItem(int id,int iconId,int nameId,boolean enableAlways){
        mId = id;
        mIconId = iconId;
        mNameId = nameId;
        mEnableAlways=enableAlways;
    }

    public int getNameId() {
        return mNameId;
    }

    public int getIconId() {
        return mIconId;
    }

    public int getId() {
        return mId;
    }
    public boolean isItemEnabled(boolean isUserDefined){
        if (mEnableAlways)
            return true;
        else
            return isUserDefined;
    }
}
