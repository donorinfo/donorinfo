package com.lexadev.donorinfo.helper;

/**
 * Created by Alexey Rogovoy (lexapublic@gmail.com) on 13.03.2015.
 */
public enum InfoFragments {
    DENIED,
    ABOUT,
    BEFORE,
    DENIED_FOREVER,
    DENIED_TEMPORARY
}
