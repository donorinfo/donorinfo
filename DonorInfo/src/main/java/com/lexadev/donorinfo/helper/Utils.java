package com.lexadev.donorinfo.helper;

import android.content.Context;

import com.lexadev.donorinfo.R;

import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;

/**
 * Created by Alexey Rogovoy (lexapublic@gmail.com) on 19.02.2015.
 */
public class Utils {
    public static String notNull(String str) {
        if (str==null)
            return "";
        return str;
    }
    public static Date getBeginDay(Date date){
        if (date==null)
            return null;
        Calendar calendar = GregorianCalendar.getInstance();
        calendar.setTime(date);
        calendar.set(Calendar.HOUR_OF_DAY,0);
        calendar.set(Calendar.MINUTE,0);
        calendar.set(Calendar.SECOND,0);
        calendar.set(Calendar.MILLISECOND,0);
        return calendar.getTime();
    }
    public static Date getEndDay(Date date){
        if (date==null)
            return null;
        Calendar calendar = GregorianCalendar.getInstance();
        calendar.setTime(date);
        calendar.set(Calendar.HOUR_OF_DAY,calendar.getActualMaximum(Calendar.HOUR_OF_DAY));
        calendar.set(Calendar.MINUTE,calendar.getActualMaximum(Calendar.MINUTE));
        calendar.set(Calendar.SECOND,calendar.getActualMaximum(Calendar.SECOND));
        calendar.set(Calendar.MILLISECOND,calendar.getActualMaximum(Calendar.MILLISECOND));
        return calendar.getTime();
    }
    public static long getBeginDay(long date){
        return getBeginDay(new Date(date)).getTime();
    }
    public static long getEndDay(long date){
        return getEndDay(new Date(date)).getTime();
    }
    public static String getDateString(Context context,long date) {
        if (context==null)
            return null;
        Calendar calendar = GregorianCalendar.getInstance();
        calendar.setTimeInMillis(date);
        String str = context.getResources().getString(R.string.format_date
                , calendar.get(Calendar.DAY_OF_MONTH)
                , calendar.get(Calendar.MONTH) + 1
                , calendar.get(Calendar.YEAR)
                , calendar.get(Calendar.HOUR_OF_DAY)
                , calendar.get(Calendar.MINUTE)
        );
        return str;
    }


}
