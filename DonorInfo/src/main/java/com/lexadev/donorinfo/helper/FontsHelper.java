package com.lexadev.donorinfo.helper;

import android.content.res.AssetManager;
import android.graphics.Typeface;

/**
 * Created by Alexey Rogovoy (lexapublic@gmail.com) on 12.02.2015.
 */
public class FontsHelper {
    public enum FontName{
        COMFORTAA,
        ROBOTO
    }
    public enum FontsStyle{
        REGULAR,
        BOLD,
        LIGHT,
        MEDIUM
    }
    private static FontsHelper instance = null;
    private FontsHelper(AssetManager asset){
        mFontComfortaaRegular = Typeface.createFromAsset(asset, "fonts/comfortaa_regular.ttf");
        mFontComfortaaBold = Typeface.createFromAsset(asset, "fonts/comfortaa_bold.ttf");
        mFontComfortaaLight = Typeface.createFromAsset(asset, "fonts/comfortaa_light.ttf");
        mFontRobotoRegular = Typeface.createFromAsset(asset, "fonts/roboto_regular.ttf");
        mFontRobotoBold = Typeface.createFromAsset(asset, "fonts/roboto_bold.ttf");
        mFontRobotoLight = Typeface.createFromAsset(asset, "fonts/roboto_light.ttf");
        mFontRobotoMedium = Typeface.createFromAsset(asset, "fonts/roboto_medium.ttf");

    }
    private Typeface mFontComfortaaRegular = null;
    private Typeface mFontComfortaaBold = null;
    private Typeface mFontComfortaaLight = null;
    private Typeface mFontRobotoRegular = null;
    private Typeface mFontRobotoBold = null;
    private Typeface mFontRobotoLight = null;
    private Typeface mFontRobotoMedium = null;

    public static FontsHelper createInstance(AssetManager asset){
        if (instance!=null)
            return instance;
        instance = new FontsHelper(asset);

        return instance;
    }
    public static Typeface getFont(FontName name,FontsStyle style){
        if (instance==null)
            throw new RuntimeException("You should create instance first");
        return instance.getFontLocal(name, style);
    }

    private Typeface getFontLocal(FontName name, FontsStyle style) {
        if (name==FontName.COMFORTAA){
            if (style==FontsStyle.REGULAR)
                return mFontComfortaaRegular;
            else if (style==FontsStyle.BOLD||style==FontsStyle.MEDIUM)
                return mFontComfortaaBold;
            else if (style==FontsStyle.LIGHT)
                return mFontComfortaaLight;
        } else if (name==FontName.ROBOTO){
            if (style==FontsStyle.REGULAR)
                return mFontRobotoRegular;
            else if (style==FontsStyle.BOLD)
                return mFontRobotoBold;
            else if (style==FontsStyle.LIGHT)
                return mFontRobotoLight;
            else if (style==FontsStyle.MEDIUM)
                return mFontRobotoMedium;

        }
        return Typeface.DEFAULT;
    }
}
