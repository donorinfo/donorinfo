package com.lexadev.donorinfo.helper;

/**
 * Created by Alexey Rogovoy (lexapublic@gmail.com) on 04.03.2015.
 */
public enum FragmentState {
    EDIT,
    VIEW
}
