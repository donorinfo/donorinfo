package com.lexadev.donorinfo.helper;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

import com.lexadev.donorinfo.AppConstants;
import com.lexadev.donorinfo.model.ParseCities;
import com.lexadev.donorinfo.model.ParseRegions;
import com.lexadev.donorinfo.model.ParseStation;
import com.lexadev.donorinfo.model.RegionObject;
import com.lexadev.donorinfo.model.RegionsSearch;
import com.lexadev.donorinfo.model.Station;
import com.lexadev.donorinfo.model.TableCities;
import com.lexadev.donorinfo.model.TableRegions;
import com.lexadev.donorinfo.model.TableStations;
import com.lexadev.donorinfo.model.TableVersions;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashSet;
import java.util.List;

public class DbAdapter {
    @SuppressWarnings("unused")
    private static final String TAG="DbAdapter";
    private DbHelper mDbHelper;

    private static DbAdapter mInstance = null;
    private static SQLiteDatabase mDbReadable = null;
    private static SQLiteDatabase mDbWritable = null;

    private synchronized  SQLiteDatabase getDbReadable(){
        if (mDbReadable==null)
            mDbReadable = mInstance.mDbHelper.getReadableDatabase();
        return mDbReadable;
    }
    private synchronized  SQLiteDatabase getDbWritable(){
        if (mDbWritable==null)
            mDbWritable = mInstance.mDbHelper.getWritableDatabase();
        return mDbWritable;
    }

    public static DbAdapter getInstance(Context context){
        if (mInstance==null) {
            mInstance = new DbAdapter(context);
            mInstance.open();
        }
        return mInstance;
    }
    private Context mCtx;
    public DbAdapter(Context ctx) {
        this.mCtx = ctx;
    }

    /**
     * Open database.
     *
     * @return the DB adapter
     * @throws SQLException the SQL exception
     */
    public DbAdapter open() throws SQLException {
        mDbHelper = new DbHelper(mCtx);
        return this;
    }

    /**
     * Close database.
     */
    public void close() {
        if (mDbReadable!=null) {
            mDbReadable.close();
            mDbReadable = null;
        }
        if (mDbWritable!=null) {
            mDbWritable.close();
            mDbWritable = null;
        }
        if (mDbHelper != null) {
            mDbHelper.close();
        }
        mInstance = null;
    }
    public boolean checkVersion(TableVersions.TABLES table,int remoteVersion){
        if (table==null){
            throw new IllegalArgumentException("There is no tables: "+table);
        }
        SQLiteDatabase db = getDbReadable();
        Cursor rows = db.query(TableVersions.TABLE_NAME, null,
                TableVersions.TABLE+"=?"
                ,new String[]{table.name()}
                , null, null, null);
        boolean ret = false;
        if (rows!=null){
            if (rows.getCount()>0){
                rows.moveToFirst();
                int ver = rows.getInt(rows.getColumnIndexOrThrow(TableVersions.VERSION));
                ret = ver==remoteVersion;
            }
            rows.close();
        }
        return ret;
    }
    public boolean updateTableRegions(List<ParseRegions> regions,int version){
        boolean ret = false;
        SQLiteDatabase db = getDbWritable();
        db.beginTransaction();
        try {
            db.delete(TableRegions.TABLE_NAME, null, null);
            ContentValues cv = new ContentValues();
            for (ParseRegions region:regions){
                cv.clear();
                cv.put(TableRegions.ID,region.getId());
                cv.put(TableRegions.NAME,region.getName());
                cv.put(TableRegions.SMALL_NAME,region.getName().toLowerCase());
                cv.put(TableRegions.HAS_STATIONS,0);
                if (AppConstants.DEBUG) Log.d(TAG,"Region update: "+cv.toString());
                db.insert(TableRegions.TABLE_NAME,null,cv);
            }
            cv.clear();
            cv.put(TableVersions.VERSION,version);
            int count = db.update(TableVersions.TABLE_NAME,cv,TableVersions.TABLE+"=?",new String[]{TableVersions.TABLES.REGIONS.name()});
            if (count==0){
                cv.put(TableVersions.TABLE, TableVersions.TABLES.REGIONS.name());
                db.insert(TableVersions.TABLE_NAME,null,cv);
            }
            db.setTransactionSuccessful();
            ret = true;
        }catch (Exception e){
            e.printStackTrace();
        }
        finally {
            db.endTransaction();
        }
        return ret;
    }
    public boolean updateTableCities(List<ParseCities> cities,int version){
        boolean ret = false;
        SQLiteDatabase db = getDbWritable();
        db.beginTransaction();
        try {
            db.delete(TableCities.TABLE_NAME, null, null);
            ContentValues cv = new ContentValues();
            for (ParseCities city:cities){
                cv.clear();
                cv.put(TableCities.ID, city.getId());
                cv.put(TableCities.REGION_ID,city.getRegionId());
                cv.put(TableCities.NAME,city.getName());
                cv.put(TableCities.SMALL_NAME,city.getName().toLowerCase());
                cv.put(TableCities.HAS_STATIONS,0);
                if (AppConstants.DEBUG) Log.d(TAG,"Cities update: "+cv.toString());
                db.insert(TableCities.TABLE_NAME,null,cv);
            }
            cv.clear();
            cv.put(TableVersions.VERSION,version);
            int count = db.update(TableVersions.TABLE_NAME,cv,TableVersions.TABLE+"=?",new String[]{TableVersions.TABLES.CITIES.name()});
            if (count==0){
                cv.put(TableVersions.TABLE, TableVersions.TABLES.CITIES.name());
                db.insert(TableVersions.TABLE_NAME,null,cv);
            }
            db.setTransactionSuccessful();
            ret = true;
        }catch (Exception e){
            e.printStackTrace();
        }
        finally {
            db.endTransaction();
        }
        return ret;
    }
    public boolean updateTableStations(List<ParseStation> stations,int version){
        boolean ret = false;
        SQLiteDatabase db = getDbWritable();
        HashSet<String> citiesWithStations = new HashSet<>();
        HashSet<String> regionsWithStations = new HashSet<>();
        db.beginTransaction();
        try {
            db.delete(TableStations.TABLE_NAME, null, null);
            ContentValues cv = new ContentValues();
            for (ParseStation station:stations){
                cv.clear();
                cv.put(TableStations.ID, station.getId());
                cv.put(TableStations.CITY_ID,station.getCityId());
                cv.put(TableStations.REGION_ID,station.getRegionId());
                cv.put(TableStations.NAME,station.getName());
                cv.put(TableStations.ORGANISATION_NAME,station.getOrganisationName());
                cv.put(TableStations.PHONE,station.getPhone());
                cv.put(TableStations.DAY_WORK,station.getDayWork());
                cv.put(TableStations.TIME_WORK,station.getTimeWork());
                cv.put(TableStations.ADDRESS,station.getAddress());
                cv.put(TableStations.LATITUDE,station.getLatitude());
                cv.put(TableStations.LONGITUDE,station.getLongitude());
                db.insert(TableStations.TABLE_NAME,null,cv);
                if (AppConstants.DEBUG) Log.d(TAG,"Station update: "+cv.toString());
                citiesWithStations.add(station.getCityId());
                regionsWithStations.add(station.getRegionId());
            }
            cv.clear();
            cv.put(TableVersions.VERSION,version);
            int count = db.update(TableVersions.TABLE_NAME,cv,TableVersions.TABLE+"=?",new String[]{TableVersions.TABLES.STATIONS.name()});
            if (count==0){
                cv.put(TableVersions.TABLE, TableVersions.TABLES.STATIONS.name());
                db.insert(TableVersions.TABLE_NAME,null,cv);
            }
            cv.clear();
            cv.put(TableCities.HAS_STATIONS, 0);
            db.update(TableCities.TABLE_NAME,cv,null,null);
            db.update(TableRegions.TABLE_NAME,cv,null,null);
            cv.clear();
            cv.put(TableCities.HAS_STATIONS, 1);
            for (String city:citiesWithStations){
                if (AppConstants.DEBUG) Log.d(TAG,"Has station cities update: "+cv.toString()+", city: "+city);
                db.update(TableCities.TABLE_NAME,cv,TableCities.ID+"=?",new String[]{city});
            }
            for (String region:regionsWithStations){
                if (AppConstants.DEBUG) Log.d(TAG,"Has station regions update: "+cv.toString()+", region: "+region);
                db.update(TableRegions.TABLE_NAME,cv,TableRegions.ID+"=?",new String[]{region});
            }

            db.setTransactionSuccessful();
            ret = true;
        }catch (Exception e){
            e.printStackTrace();
        }
        finally {
            db.endTransaction();
        }
        return ret;
    }

    public List<RegionObject> getRegions(){
        ArrayList<RegionObject> result = new ArrayList<>();
        SQLiteDatabase db = getDbReadable();
        Cursor cursor = null;
        cursor = db.rawQuery("SELECT * from "+TableRegions.TABLE_NAME,null);
        if (cursor!=null){
            if (cursor.moveToFirst()){
                do{
                    String regionName = cursor.getString(cursor.getColumnIndexOrThrow(TableRegions.NAME));
                    String regionId = cursor.getString(cursor.getColumnIndexOrThrow(TableRegions.ID));
                    result.add(new RegionObject(regionId,regionName));
                }while(cursor.moveToNext());
            }
            cursor.close();
        }
        return result;
    }
    public List<RegionObject> getCities(String regionId){
        ArrayList<RegionObject> result = new ArrayList<>();
        SQLiteDatabase db = getDbReadable();
        Cursor cursor = null;
        cursor = db.rawQuery("SELECT * from "+TableCities.TABLE_NAME+" where "+TableCities.REGION_ID+" =?",new String[]{regionId});
        if (cursor!=null){
            if (cursor.moveToFirst()){
                do{
                    String cityName = cursor.getString(cursor.getColumnIndexOrThrow(TableCities.NAME));
                    String cityId = cursor.getString(cursor.getColumnIndexOrThrow(TableCities.ID));
                    result.add(new RegionObject(cityId,cityName));
                }while(cursor.moveToNext());
            }
            cursor.close();
        }
        return result;
    }

    public List<RegionsSearch> getRegionsSearch(String search, boolean filterRegion){
        boolean isSearchExist = search!=null && search.length()>0;
        ArrayList<RegionsSearch> result = new ArrayList<>();
        SQLiteDatabase db = getDbReadable();
        Cursor cursor = null;
        if (isSearchExist){
            cursor = db.rawQuery("SELECT * from "+TableRegions.TABLE_NAME+" where "+TableRegions.SMALL_NAME +" LIKE ?"+
                    (filterRegion?" AND "+TableRegions.HAS_STATIONS+"=1":""),new String[]{"%"+search+"%"});
        }
        else{
            cursor = db.rawQuery("SELECT * from "+TableRegions.TABLE_NAME+(filterRegion?" where "+TableRegions.HAS_STATIONS+"=1":""),null);
        }
        if (cursor!=null){
            if (cursor.moveToFirst()){
                do{
                    String regionName = cursor.getString(cursor.getColumnIndexOrThrow(TableRegions.NAME));
                    String regionId = cursor.getString(cursor.getColumnIndexOrThrow(TableRegions.ID));
                    result.add(new RegionsSearch(regionName,regionId,null,null));
                }while(cursor.moveToNext());
            }
            cursor.close();
        }
        if (isSearchExist) {
            cursor = db.rawQuery("SELECT "
                    + TableCities.TABLE_NAME + "." + TableCities.NAME + ", "
                    + TableCities.TABLE_NAME + "." + TableCities.ID + ", "
                    + TableRegions.TABLE_NAME + "." + TableRegions.NAME + ", "
                    + TableRegions.TABLE_NAME + "." + TableRegions.ID
                    + " from "
                    + TableCities.TABLE_NAME + " ,"
                    + TableRegions.TABLE_NAME
                    + " where "
                    + TableCities.TABLE_NAME + "." + TableCities.SMALL_NAME + " LIKE ? AND "
                    + TableCities.TABLE_NAME + "." + TableCities.REGION_ID + "="
                    + TableRegions.TABLE_NAME + "." + TableRegions.ID+
                    (filterRegion?
                    " AND "
                    + TableCities.TABLE_NAME + "." + TableCities.HAS_STATIONS + "=1":"")
                    , new String[]{"%" + search + "%"});
        }
        else{
            cursor = db.rawQuery("SELECT "
                    +TableCities.TABLE_NAME+"."+TableCities.NAME+", "
                    +TableCities.TABLE_NAME+"."+TableCities.ID+", "
                    +TableRegions.TABLE_NAME+"."+TableRegions.NAME+", "
                    +TableRegions.TABLE_NAME+"."+TableRegions.ID
                    +" from "
                    +TableCities.TABLE_NAME+" ,"
                    +TableRegions.TABLE_NAME
                    +" where "
                    +TableCities.TABLE_NAME+"."+TableCities.REGION_ID+"="
                    +TableRegions.TABLE_NAME+"."+TableRegions.ID+
                    (filterRegion?
                    " AND "
                    + TableCities.TABLE_NAME + "." + TableCities.HAS_STATIONS + "=1":"")

                    ,null);
        }
        if (cursor!=null){
            if (cursor.moveToFirst()){
                do{
                    String regionName = cursor.getString(cursor.getColumnIndexOrThrow(TableRegions.NAME));
                    String regionId = cursor.getString(cursor.getColumnIndexOrThrow(TableRegions.ID));
                    String cityName = cursor.getString(cursor.getColumnIndexOrThrow(TableCities.NAME));
                    String cityId = cursor.getString(cursor.getColumnIndexOrThrow(TableCities.ID));
                    result.add(new RegionsSearch(regionName,regionId,cityName,cityId));
                }while(cursor.moveToNext());
            }
            cursor.close();
        }
        Collections.sort(result,new Comparator<RegionsSearch>() {
            @Override
            public int compare(RegionsSearch lhs, RegionsSearch rhs) {
                int regionsCompare = lhs.region.compareTo(rhs.region);
                if (regionsCompare==0) {
                    if (lhs.city == null) {
                        return -1;
                    }
                    else if (rhs.city==null){
                        return 1;
                    }
                    else return lhs.city.compareTo(rhs.city);
                }
                return regionsCompare;
            }
        });
        return result;
    }
    public List<Station> getStationList(String parentId){
        SQLiteDatabase db = getDbReadable();
        Cursor cursor = null;
        if (parentId==null){
            cursor = db.query(TableStations.TABLE_NAME,null,null,null,null,null,TableStations.NAME);
        }
        else if (parentId.length()<=2){
            cursor = db.query(TableStations.TABLE_NAME,null,TableStations.REGION_ID+"=?",new String[]{parentId},null,null,TableStations.NAME);
        }
        else{
            cursor = db.query(TableStations.TABLE_NAME,null,TableStations.CITY_ID+"=?",new String[]{parentId},null,null,TableStations.NAME);
        }
        List<Station> stations = new ArrayList<>();
        if(cursor!=null){
            if (cursor.moveToFirst()){
                do{
                    Station station = new Station();
                    station.setId(cursor.getString(cursor.getColumnIndexOrThrow(TableStations.ID)));
                    station.setName(cursor.getString(cursor.getColumnIndexOrThrow(TableStations.NAME)));
                    station.setNameOranisation(cursor.getString(cursor.getColumnIndexOrThrow(TableStations.ORGANISATION_NAME)));
                    station.setPhone(cursor.getString(cursor.getColumnIndexOrThrow(TableStations.PHONE)));
                    station.setDayWork(cursor.getString(cursor.getColumnIndexOrThrow(TableStations.DAY_WORK)));
                    station.setTimeWork(cursor.getString(cursor.getColumnIndexOrThrow(TableStations.TIME_WORK)));
                    station.setCityId(cursor.getString(cursor.getColumnIndexOrThrow(TableStations.CITY_ID)));
                    station.setRegionId(cursor.getString(cursor.getColumnIndexOrThrow(TableStations.REGION_ID)));
                    station.setAddress(cursor.getString(cursor.getColumnIndexOrThrow(TableStations.ADDRESS)));
                    if (!cursor.isNull(cursor.getColumnIndexOrThrow(TableStations.LATITUDE)))
                        station.setLatitude(cursor.getFloat(cursor.getColumnIndexOrThrow(TableStations.LATITUDE)));
                    if (!cursor.isNull(cursor.getColumnIndexOrThrow(TableStations.LONGITUDE)))
                        station.setLongitude(cursor.getFloat(cursor.getColumnIndexOrThrow(TableStations.LONGITUDE)));
                    stations.add(station);
                }while(cursor.moveToNext());
            }
            cursor.close();
        }
        return stations;
    }
    public Station getStation(String stationId){
        SQLiteDatabase db = getDbReadable();
        Cursor cursor = null;
        cursor = db.query(TableStations.TABLE_NAME,null,TableStations.ID+"=?",new String[]{stationId},null,null,null);
        Station station = null;
        if(cursor!=null){
            if (cursor.moveToFirst()){
                station = new Station();
                station.setId(cursor.getString(cursor.getColumnIndexOrThrow(TableStations.ID)));
                station.setName(cursor.getString(cursor.getColumnIndexOrThrow(TableStations.NAME)));
                station.setNameOranisation(cursor.getString(cursor.getColumnIndexOrThrow(TableStations.ORGANISATION_NAME)));
                station.setPhone(cursor.getString(cursor.getColumnIndexOrThrow(TableStations.PHONE)));
                station.setDayWork(cursor.getString(cursor.getColumnIndexOrThrow(TableStations.DAY_WORK)));
                station.setTimeWork(cursor.getString(cursor.getColumnIndexOrThrow(TableStations.TIME_WORK)));
                station.setCityId(cursor.getString(cursor.getColumnIndexOrThrow(TableStations.CITY_ID)));
                station.setRegionId(cursor.getString(cursor.getColumnIndexOrThrow(TableStations.REGION_ID)));
                station.setAddress(cursor.getString(cursor.getColumnIndexOrThrow(TableStations.ADDRESS)));
                if (!cursor.isNull(cursor.getColumnIndexOrThrow(TableStations.LATITUDE)))
                    station.setLatitude(cursor.getFloat(cursor.getColumnIndexOrThrow(TableStations.LATITUDE)));
                if (!cursor.isNull(cursor.getColumnIndexOrThrow(TableStations.LONGITUDE)))
                    station.setLongitude(cursor.getFloat(cursor.getColumnIndexOrThrow(TableStations.LONGITUDE)));
            }
            cursor.close();
        }
        return station;
    }

    public String getRegionName(String id) {
        SQLiteDatabase db = getDbReadable();
        Cursor cursor = null;
        if (id == null) {
            return null;
        } else if (id.length() <= 2) {
            cursor = db.query(TableRegions.TABLE_NAME, new String[]{TableRegions.NAME+" as NAME"}, TableRegions.ID + "=?", new String[]{id}, null, null, null);
        } else {
            cursor = db.query(TableCities.TABLE_NAME, new String[]{TableCities.NAME+" as NAME"}, TableCities.ID + "=?", new String[]{id}, null, null, null);
        }
        String ret = "";
        if (cursor!=null){
            if (cursor.moveToFirst())
                ret = cursor.getString(cursor.getColumnIndexOrThrow("NAME"));
            cursor.close();
        }
        return ret;
    }
    /**
     * The Class DBHelper.
     */
    private static class DbHelper extends SQLiteOpenHelper {

        // If you change the database schema, you must increment the database version.
        public static final int DATABASE_VERSION = 1;
        public static final String DATABASE_NAME = "donorinfo.db";


        /**
         * Instantiates a new dB helper.
         *
         * @param context the context
         */
        public DbHelper(Context context) {
            super(context, DATABASE_NAME, null, DATABASE_VERSION);
        }

        /* (non-Javadoc)
         * @see android.database.sqlite.SQLiteOpenHelper#onCreate(android.database.sqlite.SQLiteDatabase)
         */
        @Override
        public void onCreate(SQLiteDatabase db) {
            db.execSQL(TableRegions.CREATE_TABLE);
            db.execSQL(TableCities.CREATE_TABLE);
            db.execSQL(TableStations.CREATE_TABLE);
            db.execSQL(TableVersions.CREATE_TABLE);
        }

        /* (non-Javadoc)
         * @see android.database.sqlite.SQLiteOpenHelper#onUpgrade(android.database.sqlite.SQLiteDatabase, int, int)
         */
        @Override
        public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        }
    }
}