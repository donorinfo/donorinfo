package com.lexadev.donorinfo.helper;

import android.content.Context;
import android.content.pm.ApplicationInfo;

/**
 * Created by Alexey Rogovoy (lexapublic@gmail.com) on 06.03.2015.
 */
public class DebugDetect {
    private static boolean mIsChecked = false;
    private static boolean mIsDebug = false;
    public static boolean isDebug(Context context){
        if (mIsChecked)
            return mIsDebug;
        if (context==null)
            return mIsDebug;
        ApplicationInfo info = context.getApplicationInfo();
        if (info!=null){
            mIsChecked = true;
            if ((info.flags & ApplicationInfo.FLAG_DEBUGGABLE) !=0){
                mIsDebug = true;
            }
            else{
                mIsDebug = false;
            }
        }
        return mIsDebug;
    }
}
