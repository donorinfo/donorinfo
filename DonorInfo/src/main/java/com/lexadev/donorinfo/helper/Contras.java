package com.lexadev.donorinfo.helper;

import android.content.Context;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Alexey Rogovoy (lexapublic@gmail.com) on 03.03.2015.
 */
public class Contras {
    public Contras(Context context) {
        Category cat = new Category("1. Гемотрансмиссивные заболевания");
        mPermanent.add(cat);
        cat.add(new Contra(111,cat.name,"1.1. Инфекционные",
                "- СПИД, носительство ВИЧ-инфекции\n" +
                "- Сифилис, врожденный или приобретенный\n" +
                "- Вирусные гепатиты, положительный результат исследования на маркеры вирусных гепатитов (HB_sAg, анти-HCV антител)\n" +
                "- Туберкулез, все формы\n" +
                "- Бруцеллез\n" +
                "- Сыпной тиф\n" +
                "- Туляремия\n" +
                "- Лепра.\n"));
        cat.add(new Contra(112,cat.name,"1.2. Паразитарные",
                "- Эхинококкоз\n" +
                "- Токсоплазмоз\n" +
                "- Трипаносомоз\n" +
                "- Филяриатоз\n" +
                "- Ришта\n" +
                "- Лейшманиоз\n"));

        cat = new Category("2. Соматические заболевания");
        mPermanent.add(cat);
        cat.add(new Contra(1201,cat.name,"2.1. Злокачественные новообразования."));
        cat.add(new Contra(1202,cat.name,"2.2. Болезни крови."));
        cat.add(new Contra(1203,cat.name,"2.3. Органические заболевания ЦНС."));
        cat.add(new Contra(1204,cat.name,"2.4. Полное отсутствие слуха и речи."));
        cat.add(new Contra(1205,cat.name,"2.5. Психические заболевания."));
        cat.add(new Contra(1206,cat.name,"2.6. Наркомания, алкоголизм."));
        cat.add(new Contra(1207,cat.name,"2.7. Сердечно-сосудистые заболевания",
                "- гипертоническая болезнь II - III ст.\n" +
                        "- ишемическая болезнь сердца\n" +
                        "- атеросклероз, атеросклеротический кардиосклероз\n" +
                        "- облитерирующий эндоартериит, неспецифический аортоартериит, рецидивирующий тромбофлебит\n" +
                        "- эндокардит, миокардит\n" +
                        "- порок сердца.\n"));
        cat.add(new Contra(1208,cat.name,"2.8. Болезни органов дыхания",
                "- бронхиальная астма\n" +
                        "- бронхоэктатическая болезнь, эмфизема легких, обструктивный бронхит, диффузный пневмосклероз в стадии декомпенсации.\n"));
        cat.add(new Contra(1209,cat.name,"2.9. Болезни органов пищеварения",
                "- ахилический гастрит\n" +
                        "- язвенная болезнь желудка и двенадцатиперстной кишки.\n"));
        cat.add(new Contra(1210,cat.name,"2.10. Заболевания печени и желчных путей",
                "- хронические заболевания печени, в том числе токсической природы и неясной этиологии\n" +
                        "- калькулезный холецистит с повторяющимися приступами и явлениями холангита\n" +
                        "- цирроз печени.\n"));
        cat.add(new Contra(1211,cat.name,"2.11. Заболевания почек и мочевыводящих путей в стадии декомпенсации",
                "- диффузные и очаговые поражения почек\n" +
                        "- мочекаменная болезнь.\n"));
        cat.add(new Contra(1212,cat.name,"2.12. Диффузные заболевания соединительной ткани."));
        cat.add(new Contra(1213,cat.name,"2.13. Лучевая болезнь."));
        cat.add(new Contra(1213,cat.name,"2.14. Болезни эндокринной системы в случае выраженного нарушения функций и обмена веществ."));
        cat.add(new Contra(1215,cat.name,"2.15. Болезни ЛОР-органов",
                "- озена\n" +
                        "- прочие острые и хронические тяжелые гнойно-воспалительные заболевания.\n"));
        cat.add(new Contra(1216,cat.name,"2.16. Глазные болезни",
                "- остаточные явления увеита (ирит, иридоциклит, хориоретинит)\n" +
                        "- высокая миопия (6 Д и более)\n" +
                        "- трахома\n" +
                        "- полная слепота.\n"));
        cat.add(new Contra(1217,cat.name,"2.17. Кожные болезни",
                "- распространенные заболевания кожи воспалительного и инфекционного характера\n" +
                        "- генерализованный псориаз, эритродермия, экземы, пиодермия, сикоз, красная волчанка, пузырчатые дерматозы\n" +
                        "- грибковые поражения кожи (микроспория, трихофития, фавус, эпидермофития) и внутренних органов (глубокие микозы)\n" +
                        "- гнойничковые заболевания кожи (пиодермия, фурункулез, сикоз).\n"));
        cat.add(new Contra(1218,cat.name,"2.18. Остеомиелит острый и хронический."));
        cat.add(new Contra(1219,cat.name,"2.19. Оперативные вмешательства по поводу резекции органа (желудок, почка, желчный пузырь, селезенка, яичники, матка и пр.) и трансплантации органов и тканей."));

        cat = new Category("1. Факторы заражения гемотрансмиссивными заболеваниями");
        mTemporary.add(cat);
        cat.add(new Contra(211,cat.name,"1.1. Трансфузии крови, ее компонентов (исключение составляют ожоговые реконвалесценты и лица, иммунизированные к резус-фактору).","6 месяцев",180));
        cat.add(new Contra(212,cat.name,"1.2. Оперативные вмешательства, в т.ч. аборты (необходимо представление медицинской справки) (выписки из истории болезни) о характере и дате операции).","6 месяцев со дня оперативного вмешательства",180));
        cat.add(new Contra(213,cat.name,"1.3. Нанесение татуировки или лечение иглоукалыванием.","1 год с момента окончания процедур",365));
        cat.add(new Contra(214,cat.name,"1.4. Пребывание в загранкомандировках длительностью более 2 месяцев.","6 месяцев",180));
        cat.add(new Contra(215,cat.name,"1.5. Пребывание в эндемичных по малярии странах тропического и субтропического климата (Азия, Африка, Южная и Центральная Америка) более 3 месяцев.","3 года",1095));
        cat.add(new Contra(2161,cat.name,"1.6. Контакт с больными гепатитами: гепатит А","3 месяца",180));
        cat.add(new Contra(2162,cat.name,"1.6. Контакт с больными гепатитами: гепатиты В и С","1 год",365));
        cat = new Category("2. Перенесенные заболевания");
        mTemporary.add(cat);
        cat.add(new Contra(2211,cat.name,"2.1. Инфекционные заболевания: малярия в анамнезе при отсутствии симптомов и отрицательных результатов иммунологических тестов","3 года",1095));
        cat.add(new Contra(2212,cat.name,"2.1. Инфекционные заболевания: брюшной тиф после выздоровления и полного клинического обследования при отсутствии выраженных функциональных расстройств","1 год",365));
        cat.add(new Contra(2213,cat.name,"2.1. Инфекционные заболевания: ангина, грипп, ОРВИ","1 месяц",30));
        cat.add(new Contra(222,cat.name,"2.2. Прочие инфекционные заболевания, не указанные в разделе \"Абсолютные противопоказания\" и п.2.1 настоящего раздела.","6 месяцев",180));
        cat.add(new Contra(223,cat.name,"2.3. Экстракция зуба.","10 дней",10));
        cat.add(new Contra(224,cat.name,"2.4. Острые или хронические воспалительные процессы в стадии обострения независимо от локализации.","1 месяц после купирования острого периода",30));
        cat.add(new Contra(225,cat.name,"2.5. Вегето-сосудистая дистония.","1 месяц",30));
        cat.add(new Contra(226,cat.name,"2.6. Аллергические заболевания в стадии обострения.","2 месяца после купирования острого периода",60));
        cat = new Category("3. Период беременности и лактации");
        mTemporary.add(cat);
        cat.add(new Contra(23,cat.name,null,"1 год после родов, 3 месяца после окончания лактации",365));
        cat = new Category("4. Период менструации");
        mTemporary.add(cat);
        cat.add(new Contra(24,cat.name,null,"5 дней со дня окончания менструации",5));
        cat = new Category("5. Прививки");
        mTemporary.add(cat);
        cat.add(new Contra(251,cat.name,"- прививка убитыми вакцинами (гепатит В, столбняк, дифтерия, коклюш, паратиф, холера, грипп), анатоксинами","10 дней",10));
        cat.add(new Contra(252,cat.name,"- прививка живыми вакцинами (бруцеллез, чума, туляремия, вакцина БЦЖ, оспа, краснуха, полимиелит перорально), введение противостолбнячной сыворотки (при отсутствии выраженных воспалительных явления на месте инъекции)","1 год",365));
        cat.add(new Contra(253,cat.name,"- введение иммуноглобулина против гепатита В","1 год",365));
        cat.add(new Contra(254,cat.name,"- прививка вакциной против бешенства","2 недели",14));
        cat = new Category("6. Прием лекарственных препаратов:");
        mTemporary.add(cat);
        cat.add(new Contra(261,cat.name,"- антибиотики","2 недели после окончания приема",14));
        cat.add(new Contra(262,cat.name,"- анальгетики, салицилаты","3 дня после окончания приема",3));
        cat = new Category("7. Прием алкоголя.");
        mTemporary.add(cat);
        cat.add(new Contra(27,cat.name,null,"48 часов",2));
        cat = new Category("8. Изменения биохимических показателей крови");
        mTemporary.add(cat);
        cat.add(new Contra(281,cat.name,"- повышение активности аланин-аминотрансферазы (АЛТ) менее чем в 2 раза","3 месяца",90));
        cat.add(new Contra(282,cat.name,"- повторное повышение или увеличение АЛТ в 2 и более раз","отстранение от донорства и направление на обследование",-1));
        cat.add(new Contra(283,cat.name,"- диспротеинемия","1 месяц",30));
    }

    public enum Type{
        PERMANENT,
        TEMPORARY
    };
    private static Contras instance = null;
    public ArrayList<Category> mPermanent = new ArrayList<>();
    public ArrayList<Category> mTemporary = new ArrayList<>();
    public List<Category> getContras(Type type){
        switch (type){
            case PERMANENT:
                return mPermanent;
            case TEMPORARY:
                return mTemporary;
        }
        return null;
    }
    public class Category{
        public Category(String name){
            this.name = name;
        }
        public String name;
        public ArrayList<Contra> items = new ArrayList<>();
        public void add(Contra contra){
            items.add(contra);
        }
    }
    public class Contra{
        public Contra(int id,String category,String name,String list,String period,int days){
            this.id = id;
            this.category = category;
            this.name = name;
            this.period = period;
            this.days = days;
            this.list = list;
        }
        public Contra(int id,String category,String name,String period,int days){
            this.id = id;
            this.category = category;
            this.name = name;
            this.period = period;
            this.days = days;
            this.list = null;
        }

        public Contra(int id,String category,String name,String list){
            this(id,category,name,list,null,-1);
        }
        public Contra(int id,String category,String name){
            this(id,category,name,null);
        }
        public int id;
        public String category;
        public String name;
        public String list=null;
        public String period=null;
        public int days=-1;
    }
    public static Contras newInstance(Context context){
        if (instance==null){
            instance = new Contras(context);
        }
        return instance;
    }
    public List<Contra> getContrasList(Type type){
        ArrayList<Contra> items = new ArrayList<>();
        List<Category> categorized = null;
        switch (type){
            case PERMANENT:
                categorized = mPermanent;
                break;
            case TEMPORARY:
                categorized=mTemporary;
                break;
            default:
                return null;
        }
        for (Category category:categorized){
            for (Contra contra:category.items){
                items.add(contra);
            }
        }
        return items;
    }
}

