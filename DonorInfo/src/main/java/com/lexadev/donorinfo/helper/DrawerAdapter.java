package com.lexadev.donorinfo.helper;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.lexadev.donorinfo.R;
import com.lexadev.donorinfo.parse.AppUser;

import java.util.List;

/**
 * Created by Alexey Rogovoy (lexapublic@gmail.com) on 12.02.2015.
 */
public class DrawerAdapter extends ArrayAdapter<DrawerItem> {
    private static final String TAG = "DrawerAdapter";
    private List<DrawerItem> mItems;
    private int mResource;
    private LayoutInflater mInflater;
    public DrawerAdapter(Context context, int resource, List<DrawerItem> objects) {
        super(context, resource, objects);
        mItems = objects;
        mInflater = LayoutInflater.from(context);
        mResource = resource;
    }

    @Override
    public long getItemId(int position) {
        return getItem(position).getId();
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        ViewsHolder holder;
        if (convertView == null){
            convertView = mInflater.inflate(mResource,parent,false);
            holder = new ViewsHolder(convertView);
        }
        else{
            holder = (ViewsHolder)convertView.getTag();
        }
        holder.build(position);
        convertView.setEnabled(isEnabled(position));
        return convertView;
    }
    private class ViewsHolder{
        private ImageView icon;
        private TextView text;
        public ViewsHolder(View rootView){
            icon = (ImageView)rootView.findViewById(R.id.icon);
            text = (TextView)rootView.findViewById(R.id.text);
            text.setTypeface(FontsHelper.getFont(FontsHelper.FontName.ROBOTO, FontsHelper.FontsStyle.REGULAR));
            rootView.setTag(this);
        }
        public void build(int position){
            DrawerItem item = getItem(position);
            icon.setImageResource(item.getIconId());
            text.setText(item.getNameId());
        }
    }

    @Override
    public boolean areAllItemsEnabled() {
        return false;
    }

    @Override
    public boolean isEnabled(int position) {
        AppUser user = AppUser.getCurrentUser();
        DrawerItem item = getItem(position);
        return item.isItemEnabled(user!=null);
    }
}
