package com.lexadev.donorinfo;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;

public class BootCompletedReceiver extends BroadcastReceiver {
    public BootCompletedReceiver() {
    }

    @Override
    public void onReceive(Context context, Intent intent) {
        if (intent!=null && Intent.ACTION_BOOT_COMPLETED.equals(intent.getAction())){
            EventRemindService.startUpdateRemind(context);
            EventRemindService.startUpdateServiceRemind(context);
        }
    }
}
